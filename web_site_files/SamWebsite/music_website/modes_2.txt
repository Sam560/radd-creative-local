 
Cmaj7 : C,D,E,F,G,A,B,C
Dm7 : C,D,Eb,F,G,A,Bb,C
Em7 : C,Db,Eb,F,G,Ab,Bb,C
Fmaj7 : C,D,E,F#,G,A,B,C
G7 : C,D,E,F,G,A,Bb,C
Am7 : C,D,Eb,F,G,Ab,Bb,C
Bm7/b5 : C,d,Eb,F,Gb,Ab,Bb,C

Dbmaj7 : Db,Eb,F,Gb,Ab,Bb,C,Db
Ebm7 : Db,Eb,E,Gb,Ab,Bb,Cb,Db
Fm7 : Db,D,E,Gb,Ab,A,Cb,Db
Gbmaj7 : Db,Eb,F,F,Ab,Bb,C,Db
Ab7 : Db,Eb,F,Gb,Ab,Bb,Cb,Db
Bbm7 : Db,Eb,E,Gb,Ab,A,Cb,Db
Cm7b5 : Db,D,E,Gb,G,A,Cb,Db

Dmaj7 : D,E,F#,G,A,B,C#,D
Em7 : D,E,F,G,A,B,C,D
F#m7 :D,Eb,F,G,A,Bb,C,D
Gmaj7 : D,E,F#,G#,A,B,C#,D
A7 : D,E,F#,G,A,B,C,D
Bm7 : D,E,F,G,A,Bb,C,D
C#m7/b5 : D,Eb,F,G,Ab,Bb,C,D

Ebmaj7: Eb,F,G,Ab,Bb,C,D,Eb
Fm7 : Eb,F,Gb,Ab,Bb,C,Db,Eb
Gm7 : Eb,Fb,Gb,Ab,Bb,Cb,Db,Eb
Abmaj7 : Eb,F,G,A,Bb,C,D,Eb
Bb7 : Eb,F,G,Ab,Bb,C,Db,Eb
Cm7 : Eb,F,Gb,Ab,Bb,Cb,Db,Eb
Dm7/b5 : Eb,Fb,Gb,Ab,A,Cb,Db,Eb