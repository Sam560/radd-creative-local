C Modes: 
 Cmaj7 : C,D,E,F,G,A,B,C
Dm7 : C,D,Eb,F,G,A,Bb,C
Em7 : C,Db,Eb,F,G,Ab,Bb,C
Fmaj7 : C,D,E,F#,G,A,B,C
G7 : C,D,E,F,G,A,Bb,C
Am7 : C,D,Eb,F,G,Ab,Bb,C
Bm7/b5 : C,d,Eb,F,Gb,Ab,Bb,C

Db Modes: 
 Dbmaj7 : Db,Eb,F,Gb,Ab,Bb,C,Db
Ebm7 : Db,Eb,E,Gb,Ab,Bb,Cb,Db
Fm7 : Db,D,E,Gb,Ab,A,Cb,Db
Gbmaj7 : Db,Eb,F,F,Ab,Bb,C,Db
Ab7 : Db,Eb,F,Gb,Ab,Bb,Cb,Db
Bbm7 : Db,Eb,E,Gb,Ab,A,Cb,Db
Cm7b5 : Db,D,E,Gb,G,A,Cb,Db

D Modes: 
 Dmaj7 : D,E,F#,G,A,B,C#,D
Em7 : D,E,F,G,A,B,C,D
F#m7 :D,Eb,F,G,A,Bb,C,D
Gmaj7 : D,E,F#,G#,A,B,C#,D
A7 : D,E,F#,G,A,B,C,D
Bm7 : D,E,F,G,A,Bb,C,D
C#m7/b5 : D,Eb,F,G,Ab,Bb,C,D

Eb Modes: 
 Ebmaj7: Eb,F,G,Ab,Bb,C,D,Eb
Fm7 : Eb,F,Gb,Ab,Bb,C,Db,Eb
Gm7 : Eb,Fb,Gb,Ab,Bb,Cb,Db,Eb
Abmaj7 : Eb,F,G,A,Bb,C,D,Eb
Bb7 : Eb,F,G,Ab,Bb,C,Db,Eb
Cm7 : Eb,F,Gb,Ab,Bb,Cb,Db,Eb
Dm7/b5 : Eb,Fb,Gb,Ab,A,Cb,Db,Eb
E Modes: 
 Emaj7 : E,F#,G#,A,B,C#,D#,E
F#m7 : E,F#,G,A,B,C#,D,E
G#m7 : E,F,G,A,B,C,D,E
Amaj7 : E,F#,G#,A#,B,C#,D#,E
B7 : E,F#,G,A,B,C#,D,E
C#m7 : E,F#,G,A,B,C,D,E
D#m7/b5 : E,F,G,A,Bb,C,D,E
F Modes: 
 Fmaj7 : F,G,A,Bb,C,D,E,F
Gm7 : F,G,Ab,Bb,C,D,Eb,F
Am7 : F,Gb,Ab,Bb,C,Db,Eb,F
Bbmaj7 : F,G,A,B,C,D,E,F
C7 : F,G,A,Bb,C,D,Eb,F
Dm7 : F,G,Ab,Bb,C,Db,Eb,F
Em7/b5 : F,Gb,Ab,Bb,B,Db,Eb,F
F# Modes: 
 Fmaj7 : F#,G#,A#,B,C#,D#,E#,F#
G#m7 : F#,G#,A,B,C#,D#,E,F#
A#m7 : F#,G,A,B,C#,D,E,F#
Bmaj7 : F#,G#,A#,C,C#,D#,E#,F#
C#7 : F#,G#,A#,B,C#,D#,E,F#
D#m7 : F#,G#,A,B,C#,D,E,F#
E#m7/b5 : F#,G,A,B,C,D,E,F#
G Modes: 
 Gmaj7 : G,A,B,C,D,E,F#,G
Am7 : G,A,Bb,C,D,E,F,G
Bm7 : G,Ab,Bb,C,D,Eb,F,G
Cmaj7 : G,A,B,C#,D,E,F#,G
D7 : G,A,B,C,D,E,F,G
Em7 : G,A,Bb,C,D,Eb,F,G
F#m7/b5 : G,Ab,Bb,C,Db,Eb,F,G
Ab Modes: 
 Abmaj7 : Ab,Bb,C,Db,Eb,F,G,Ab
Bbm7 : Ab,Bb,Cb,Db,Eb,F,Gb,Ab
Cm7 : Ab,A,Cb,Db,Eb,E,Gb,Ab
Dbmaj7 : Ab,Bb,C,D,Eb,F,G,Ab
Eb7 : Ab,Bb,C,Db,Eb,F,Gb,Ab
Fm7 : Ab,Bb,Cb,Db,Eb,Fb,Gb,Ab
Gm7/b5 : Ab,A,Cb,Db,D,E,Gb,Ab
A Modes: 
 Amaj7 : A,B,C#,D,E,F#,G#,A
Bm7 : A,B,C,D,E,F#,G,A
C#m7 : A,Bb,C,D,E,F,G,A
Dmaj7 : A,B,C#,D#,E,F#,G#,A
E7 : A,B,C#,D,E,F#,G,A
F#m7 : A,B,C,D,E,F,G,A
G#m7/b5 : A,Bb,C,D,Eb,F,G,A
Bb Modes: 
 Bbmaj7 : Bb,C,D,Eb,F,G,A,Bb
Cm7 : Bb,C,Db,Eb,F,G,Ab,Bb
Dm7 : Bb,Cb,Db,Eb,F,Gb,Ab,Bb
Ebmaj7 : Bb,C,D,E,F,G,A,Bb
F7 : Bb,C,D,Eb,F,G,Ab,Bb
Gm7 : Bb,C,Db,Eb,F,Gb,Ab,Bb
Am7/b5 : Bb,Cb,Db,Eb,Fb,Gb,Ab,Bb
B Modes: 
 Bmaj7 : B,C#,D#,E,F#,G#,A#,B
C#m7 : B,C#,D,E,F#,G#,A,B
D#m7 : B,C,D,E,F#,G,A,B
Emaj7 : B,C#,D#,F,F#,G#,A#,B
F#7 : B,C#,D#,E,F#,G#,A,B
G#m7 : B,C#,D,E,F#,G,A,B
A#m7/b5 : B,C,D,E,F,G,A,B



