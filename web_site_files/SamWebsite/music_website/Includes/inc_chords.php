<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
<link rel="stylesheet" type="text/css" href="styles.css">
    <title>Guest Book Round </title>

</head>

<body>
<h1>Build Chords and Scales</h1>


<!--Numbers above fret board-->

<table border="0" align="center" >
    <tbody>
        <tr>
        <td width="38" valign="top" height="14" align="center">0</td>
        <td width="38" valign="top" height="14" align="center">1</td>
        <td width="38" valign="top" height="14" align="center">2</td>
        <td width="38" valign="top" height="14" align="center">3</td>
        <td width="38" valign="top" height="14" align="center">4</td>
        <td width="38" valign="top" height="14" align="center">5</td>
        <td width="38" valign="top" height="14" align="center">6</td>
        <td width="38" valign="top" height="14" align="center">7</td>
        <td width="38" valign="top" height="14" align="center">8</td>
        <td width="38" valign="top" height="14" align="center">9</td>
        <td width="38" valign="top" height="14" align="center">10</td>
        <td width="38" valign="top" height="14" align="center">11</td>
        <td width="38" valign="top" height="14" align="center">12</td>
        <td width="38" valign="top" height="14" align="center">13</td>
        <td width="38" valign="top" height="14" align="center">14</td>
        <td width="38" valign="top" height="14" align="center">15</td>
        </tr>   
    </tbody>
</table>
<!--End of Numbers table-->


<!--table for fret board -->
<table valign="top" id="tab" style="styles.css" width="681" height="88" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr><td>
<img src="../images/6th_string.jpg" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0">
<br> <!--Top row of frets-->
<img src="../images/5th_string.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0">
<br> <!--second row of frets-->
<img src="../images/4th_string.jpg" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0">
<br>    <!-- third row of frets-->
<img src="../images/3rd_string.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0">
<br>    <!-- forth row of frets-->
<img src="../images/2nd_string.jpg" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/inlay_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0">
<br>    <!--fifth row of frets-->
<img src="../images/top_string.jpg" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0"><img src="../images/empty_fret.png" width="42" height="14" border="0">
<br>    <!--end of fret board-->
</td></tr></tbody>
</table>
<!--end of fret board-->

<!--color coded table-->
<table valign="center" id="tab" style="styles.css" width="10" height="88" cellspacing="0" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
        <p>High E String <img src="../images/6th_string.jpg" width="42" height="14" border="0"></p>
        <p>High B String <img src="../images/5th_string.png" width="42" height="14" border="0"></p>
        <p> G String <img src="../images/4th_string.jpg" width="42" height="14" border="0"></p>
        <p> D String <img src="../images/3rd_string.png" width="42" height="14" border="0"></p>
        <p> A String <img src="../images/2nd_string.jpg" width="42" height="14" border="0"></p>
        <p> E String <img src="../images/top_string.jpg" width="42" height="14" border="0"></p>


    </tbody>
</table>
<!--color coded table-->

<!--Chords Table-->
<table>
    <tbody>
        <div class="chords table">
            <form name="chords" id="howHeard" action="/heard" method="post">
            <div class="one-third column">
             </div>
            <div class="two-thirds column" id="main">
                <feildset>
                    <legend>Normal Chords</legend>
                        <label for="Select Key">Key</lable>
                            <select id=inc_chords.php name="select_key">
                        <!-- Need to add php or javascript data -->
                    </select><br>
                    <legend>Scales</legend>
                        <label for="Select Scale">Scale</lable>
                            <select id=inc_chords.php name="select_scale">
                        <!-- need to add php or javascript data -->
                        </select><br>
                        <legend>Common Modes</legend>
                        <label for="Select Scale">Modes</lable>
                            <select id=inc_chords.php name="select_scale">
                        <!-- need to add php or javascript data -->
                        </select><br>
                    <input id="submit" type="submit" value="submit"/>

                </feildset>
            </div> <!--end of two thirds-->
        </div><!--container-->
    </tbody>
</table>

    <?php

    // check to see if user has entered any information
    if ( empty($_POST['Root Note']) || empty($_POST['Third Note']) || empty($_POST['Fifth Note']) ) {
        echo "Please enter a value into the text box";
    } else{              // if valid, identify values that were entered and print back
        !empty($_POST['Root Note']) || !empty($_POST['Third Note']) || !empty($_POST['Fifth Note']) 
            echo "The notes you selected print "; 
    }



    

    // once validated check the values to determine weather they are major or minor 
        //look for special symbols indicating sharp or flat 
        // look at sequence to determine if sqeuence will indicate major or minor
            // maybe create a button on the form to select actual mode? 
    // once determined which value is entered, show scale and associated with values. 

    // text boxs to enter information

    echo "works here";
    ?>
    <!-- <form method= "POST" action= "inc_chords.php">
        <p> Root Note:<input type= "text" name= "Root"/></p>
        <p>  Third Note:<input type= "text" name= "Third"/></p>
        <p> Fifth Note:<input type= "text" name= "Fifth"/></p>
        <p><input type= "submit" name= "submit" value= "Submit"/></p>
        <p><input type= "reset" name= "reset"   value= "Reset Entries" /></p> -->

</body>
</html>