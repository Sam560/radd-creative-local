<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>History of String Insturments</title>
</head>

 <!--top bar-->
 <div class="container" id="callus"> 
        <div class="eight columns">
          <div class="callus">
            <a title = "Check my Site " href="#">Check my Site</a>
            |
            <a title="Fort Collins Real Estate" href="#">Links to various artists</a>
         </div>
        </div>
     </div><!--end of top bar-->

        <!--Header and title-->
        <div class = "banner" id = "music_images" img = "rotban.img">
            <h1>History of String Instruments </h1>
            <h2>A Code Demonstration for PHP,Javascript,CSS</h2>
        </div>
        <!-- End of Header-->

        <!--Top menu buttons-->
        <div class = "menu" id = "music_buttons" img = "#">
            <p>Menu</p>
            <ul>
                  <button class = "btn"><i class = "homepage.php" img = "img\HomePage.img">Home Page</i></button>
                  <button class = "btn"><i class = "western_music_his.php">Music History</i></button>
                  <button class = "btn"><i class = "various_string_insturments.php">String Insturments</i></button>
                  <button class = "btn"><i class = "music_theory.php">Music Theory</i></button>
                  <button class = "btn"><i class = "scales.php">Scales</i></button>
                  <button class = "btn"><i class = "chords.php">Chords</i></button>
                  <button class = "btn"><i class = "composer_list.php">Composers</i></button>      
            </ul>
        </div>