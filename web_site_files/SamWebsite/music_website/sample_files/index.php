<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
<link rel="stylesheet" type="text/css" href="style.css">
    <title>History of String Instruments by Sam Schoenberg</title>
</head>
<body>
    <div id = "wrapper">
        <div id = "header">
            <?php include_once(__DIR__.'/Includes/inc_header.php')?>
        </div>

        <div id="txtnav">
             <p><?php include_once(__DIR__.'/Includes/inc_text_links.php') ?></p>
        </div>

        <div id="btnnav">
            <p><?php include_once(__DIR__.'/Includes/inc_button_nav.php') ?></p>
        </div>
   
    <div id="section">
<p>
    <?php
        if (isset($_GET['page'])) {
        switch ($_GET['page']) {
            case 'western_music_his':
            include('Includes/inc_muisc_his.php');
            break;
            case 'various_string_instruments':
            include('Includes/' . 'inc_string_instruments.php');
            break;
            case 'music_theory':
            include('Includes/' . 'inc_music_theory.php');
            break;
            case 'scales':
            include('Includes/' . 'inc_scales.php');
            break; 
            case 'chords':
            include('Includes/' . 'inc_chords.php');
            break;
            case 'composer_list':
            include('Includes/' . 'inc_composer_list.php');
            break;
            case 'home_page': // A value of 'home_page' means to display the default page
            default:
            include('Includes/inc_home.php');
            break;
            }
        } else { 
        // If no button has been selected, then display the default page
        include('include/inc_home.php');
        }
    ?>
</p>
</div>

  <div id="footer">
    <p><?php include_once(__DIR__.'/includes/inc_footer.php') ?></p>
  </div>
</div> <!-- wrapper -->

</body>
</html>