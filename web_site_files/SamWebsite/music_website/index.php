<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>\
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles.css">
    <title>History of String Instruments by Sam Schoenberg</title>
</head>
<body>
     <!--start of header-->
     <div id = "wrapper">
        <div id = "header">
            <?php include_once(__DIR__.'/Includes/inc_header.php')?>
        </div>

        <div id="txtnav">
             <p><?php include_once(__DIR__.'/Includes/inc_text_links.php') ?></p>
        </div>

        <div id="btnnav">
            <p><?php include_once(__DIR__.'/Includes/inc_button_nav.php') ?></p>
        </div>
   
    
    <!--end of header-->

    <!--start of nav bar-->
    <div id="section">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
        <a class="navbar-brand active" href="#">Sam Schoenberg</a>

        <div id="section">
            <p>
                <?php
                    if (isset($_GET['page'])) {
                    switch ($_GET['page']) {
                        case 'western_music_his':
                        include('Includes/inc_muisc_his.php');
                        break;
                        case 'various_string_instruments':
                        include('Includes/' . 'inc_string_instruments.php');
                        break;
                        case 'music_theory':
                        include('Includes/' . 'inc_music_theory.php');
                        break;
                        case 'scales':
                        include('Includes/' . 'inc_scales.php');
                        break; 
                        case 'chords':
                        include('Includes/' . 'inc_chords.php');
                        break;
                        case 'composer_list':
                        include('Includes/' . 'inc_composer_list.php');
                        break;
                        case 'home_page': // A value of 'home_page' means to display the default page
                        default:
                        include('Includes/inc_home.php');
                        break;
                        }
                    } else { 
                    // If no button has been selected, then display the default page
                    include('include/inc_home.php');
                    }
                ?>
        </p>

    </div>
</div> 
</nav>
    <!--end of nav bar-->
    <p>works here fucker</p>
   
</body>
</html>