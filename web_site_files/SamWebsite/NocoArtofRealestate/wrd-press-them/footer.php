<div class="section-two">
    <div class="w-container">
      <h1 class="comunities-heading">Communities </h1>
    </div>
    <div data-w-id="3e7d2c9d-a404-fd04-d2ac-b777d903fe8b" style="-webkit-transform:translate3d(0, 239PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 239PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 239PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 239PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="col-communites w-row">
      <div class="column-3 w-col w-col-3">
        <div data-w-id="b827760b-0e0d-8b3e-29b5-4235e2797c7f" class="communities-accrd-trigger"><img src="images/foco-2-banner.jpg" alt="fort-collins-pic" width="700" height="300"></div>
        <div data-w-id="9a72df7a-b6cb-d051-18d8-9401e09a33c1" style="height:0PX" class="comm-accrd-content">
          <h1 class="heading-10">Fort Collins </h1>
          <p class="paragraph-7">Award winning schools, a thriving arts scene, unique shops, restaurants and pristine outdoor amenities make Fort Collins one of the Top 100 Best Places to Live. High-tech companies like Hewlett Packard, Intel and National Semiconductor are able to draw talented and well educated workers thanks to the . Colorado State University anchors the city’s economy while also setting the cultural tone. A collection of breweries are among the many entertainment options if Fort Collins.</p>
        </div>
      </div>
      <div class="column-4 w-col w-col-3">
        <div data-w-id="9f874602-9fa3-1cf9-81ce-11f3236f8f4d" class="communities-accrd-trigger"><img src="images/loveland.jpg" alt="fort-collins-pic" width="700" height="300"></div>
        <div style="height:0PX" class="comm-accrd-content">
          <h1 class="heading-10">Loveland</h1>
          <p class="paragraph-7">What’s not to love about ? Sorry, couldn’t resist. But all kidding aside, this city - at the base of the Rocky Mountains - provides residents with quality health care, a strong economy and lots of things to do. Residents and visitors enjoy touring breweries, going on hikes, playing in the nearby lakes and checking out Loveland’s authentic arts community.</p>
        </div>
      </div>
      <div class="column-5 w-col w-col-3">
        <div data-w-id="60bf774b-5858-e6b3-e11f-c1367dd6e11c" class="communities-accrd-trigger"><img src="images/foco.jpg" alt="fort-collins-pic" width="700" height="300"></div>
        <div style="height:0PX" class="comm-accrd-content">
          <h1 class="heading-10">Wellington </h1>
          <p class="paragraph-7">Looking to move to Wellington , CO? We’ve got everything you want to know about the key factors that could make this the best place for you, including Wellington , CO real estate. Let’s start with the basics: Wellington , CO is located in Larimer County .What about cost of living in Wellington , CO? The median income in Wellington , CO is $76,917 and the median home value is $212,600, which should give you a pretty good idea of its affordability.</p>
        </div>
      </div>
      <div class="column-6 w-col w-col-3">
        <div data-w-id="627971a5-a5db-4d96-2449-b93be1731a22" class="communities-accrd-trigger"><img src="images/LivermoreCO.jpg" alt="fort-collins-pic" width="700" height="300" srcset="images/LivermoreCO-p-500.jpeg 500w, images/LivermoreCO-p-800.jpeg 800w, images/LivermoreCO.jpg 1000w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 88vw, (max-width: 991px) 22vw, 23vw"></div>
        <div style="height:0PX" class="comm-accrd-content">
          <h1 class="heading-10">Livermore</h1>
          <p class="paragraph-7">Living here is endless excitement and fun, and the people are great. There are so many things to do and people to meet, you’ll always be on the move. Residents are always hiking local trails or enjoying the serene beauty of the winter months skiing across a truly beautiful landscape. But the fun and excitement of living and working in Livermore only starts with the lifestyle families experience. There are also abundant job opportunities existing in the city, making it easier for new residents to find work, or a new career.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="section">
    <div class="columns-4 w-row">
      <div class="column-7 w-col w-col-3">
        <div>
          <h1 class="heading-14">Mark Keller </h1>
        </div>
        <div class="div-block-235">
          <h1 class="heading-237">Real Estate</h1>
        </div>
        <div class="div-block-3">
          <blockquote class="block-quote">1333 Sheilds Street<br>Fort Collins, Colorado <br>80521 <br>‍</blockquote>
        </div>
      </div>
      <div class="column-8 w-col w-col-3">
        <div class="div-block-4">
          <h4 class="heading-16">Telephone:</h4>
        </div>
        <div class="div-block-5"><img src="images/social-40-white.svg" width="38" alt="phone-pic" class="image-6"><a href="tel:9704209039" class="link-3">970 420 9039 </a></div>
      </div>
      <div class="column-9 w-col w-col-3">
        <ul class="unordered-list">
          <li class="list-item-2">Monday - Friday </li>
          <li class="list-item-3">8:00 am - 5:00 pm </li>
        </ul>
        <div class="div-block-7">
          <h4 class="heading-18">Hours of Operation:</h4>
        </div>
      </div>
      <div class="column-10 w-col w-col-3">
        <div class="div-block-8">
          <h4 class="heading-19"><strong class="bold-text">FOR EXCLUSIVE NEWS &amp; MARKET UPDATES SIGNUP TO OUR NEWSLETTER</strong></h4><img src="images/social-25-white.svg" width="36" alt="" class="image-7"></div>
        <div class="div-block-9">
          <div class="form-block w-form">
            <form id="email-form-3" name="email-form-3" data-name="Email Form 3"><label for="name" class="field-label">Name</label><input type="text" id="name" name="name" data-name="Name" maxlength="256" class="w-input"><label for="email" class="field-label-2">Email Address</label><input type="email" class="w-input" maxlength="256" name="email" data-name="Email" id="email" required=""><input type="submit" value="Submit" data-wait="Please wait..." class="w-button"></form>
            <div class="w-form-done">
              <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
              <div>Oops! Something went wrong while submitting the form.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="footer accent">
    <div class="w-container">
      <div class="w-row">
        <div class="column w-col w-col-3">
          <h1 class="footer-brand-text">Noco Art of <br>Real Estate</h1>
          <div class="text-block">Product of : </div><a href="#schoenbergwebtools.com" class="link-2">Schoenberg Web Tools LLC</a>
          <div><a href="#" class="social-icon-link w-inline-block"><img src="images/social-03.svg" width="20" alt="" class="image"></a><a href="#" class="social-icon-link w-inline-block"><img src="images/social-18.svg" width="20" alt="" class="image-2"></a><a href="#" class="social-icon-link w-inline-block"><img src="images/social-09.svg" width="20" alt="" class="image-3"></a><a href="#" class="social-icon-link w-inline-block"><img src="images/social-06.svg" width="20" alt="" class="image-4"></a></div>
        </div>
        <div class="column-2 w-col w-col-3">
          <h5 class="heading-3">PRODUCT</h5><a href="#" class="footer-link">Buying a Home</a><a href="#" class="footer-link">Selling A Home </a><a href="#" class="footer-link">Search Properties </a><a href="#" class="footer-link">Contact Me </a></div>
        <div class="w-col w-col-3">
          <h5 class="heading-4">SOCIAL</h5><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a></div>
        <div class="w-col w-col-3">
          <h5 class="heading-5">LEGAL</h5><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a><a href="#" class="footer-link">Footer Text Link</a></div>
      </div>
    </div>
  </div>
  <script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>