<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content=""/> 
    <!-- Meta Description -->

  
    <title>NoCo Art of Real Estate</title> <!-- Your Page Title -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


     <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/main-resource.css" />


    <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--end-fonts-->
  
    <link rel="icon" href="images/deck-installation-favicon.jpg">
    <!--Your Website Favicon-->
   
  </head>
  <body>
        <div class="container-fluid" id="banner">
            
            <a href="https://nocoartofrealestate.com"> <img src="images/" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
            <!-- YOUR WEBSITE LOGO-->
            

                <h2 align="center" class="tagline center-block">Creative Realestate for Northern Colorado</h2>

            <!--City Dropdown-->
            <div class="dropdown" id="city-selection" >

                    <button class="btn btn-default dropdown-toggle dropdown-button" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <p>CHOOSE CITY<span class="caret"></span></p></button>

                    <ul class="dropdown-menu">

                        <li><a href="https://yourwebsitename.com/">Fort Collins</a></li>

                        <li role="separator" class="divider"></li>

                        <li><a href="https://yourwebsitename.com/">Loveland </a></li>
                        
                        <li role="separator" class="divider"></li>

                        <li><a href="https://yourwebsitename.com/">Livermore</a></li>
                        
                        <li role="separator" class="divider"></li>

                        <li><a href="https://yourwebsitename.com/">Welington</a></li>
                        
                        <li role="separator" class="divider"></li>
                </ul>

            </div>  <!--- /#city-selection-->

        </div>  <!--- /#banner-->  

        <nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
        
        <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                    <span class="sr-only">Toggle navigation </span>
        
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
        
                </button>

        </div>


            <div class="collapse navbar-collapse">

                    <ul align="center" class="nav navbar-nav" >

                        <li> <a href="index.php"><span class="center-underline">Home</span></a></li>
            
                        <li> <a href="about.php"><span class="center-underline">About</span></a></li>

                        <li> <a href="resources.php"><span class="center-underline">Resources</span></a></li>

                        <li> <a href="#"><span class="center-underline">Listings</span></a></li>

                        <li> <a class="quote" href="contact.php"><span style="color:#774220">Contact</span></a></li>
                
                
                    </ul>

            </div><!--end navbar-collapse-->

        </nav><!--end nav-->

            <div class="resource-banner">
            
            <h2 align="center"> RESOURCES</h2>

            </div>  <!--- /.resource-banner-->

        <section class="wrapper style4 container">
            <div class="row gtr-150">
                <div class="col-4 col-12-narrower">
                    <!-- sidebar -->
                    <div class="sidebar">

                        <section>
                            <header>
                                <h3>Magna Feugiat</h3>
                            </header>
                                    <ul class="sidenav">
                            <li> <a href="#">Selling My House</a></li>     
                            <li> <a href="#">Why Choose Mark Keller</a></li>
                            <li> <a href="#">Selling your House FAQ</a></li>
                            <li> <a href="#">Listings</a></li>
                        </ul>
                            <footer>
                                <ul class="buttons">
                                    <li><a href="#" class="button small">Learn More</a></li>
                                </ul>
                            </footer>
                        </section>

                        <section>
                            <a href="#" class="image featured"><img src="images/house1-pic.jpg" alt="Home 1" /></a>
                            <a href="#" class="image featured"><img src="images/house1-pic.jpg" alt="Home 2" /></a>
                            <a href="#" class="image featured"><img src="images/house1-pic.jpg" alt="Home 3" /></a>
                            <a href="#" class="image featured"><img src="images/house1-pic.jpg" alt="Home 4" /></a>

                            <header>
                                <h3>Amet Lorem Tempus</h3>
                            </header>
                            <p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
                            <footer>
                                <ul class="buttons">
                                    <li><a href="#" class="button small">Learn More</a></li>
                                </ul>
                            </footer>
                        </section>
                    </div>
                    <!-- end of sidebar -->
                        </div>
                                <!-- left side content -->
                                <div class="col-8 col-12-narrower imp-narrower">
                                <!-- Content -->
                                <div class="content">
                                <section>
                                    <a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
                                    <header>
                                    <h3 align="center"> Why Choose Mark Keller as Your Agent?</h3>
                                    </header>
                                        <div class="col-4 col-12-narrower imp-narrower">
                                            <h2>Home Marketing Expertise </h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor sapien, commodo euismod mollis in, aliquet iaculis libero. Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. <br /> <br />Pellentesque vulputate sapien a sem imperdiet fringilla. Phasellus eget eros pulvinar, sagittis elit nec, malesuada enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                                            <ul type="circle">
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                            </ul>
                                        </div>
                                    <p>Vestibulum diam quam, mollis at consectetur non, malesuada quis augue. Morbi tincidunt pretium interdum. Morbi mattis elementum orci, nec dictum porta cursus justo. Quisque ultricies lorem in ligula condimentum, et egestas turpis sagittis. Cras ac nunc urna. Nullam eget lobortis purus. Phasellus vitae tortor non est placerat tristique. Sed id sem et massa ornare pellentesque. Maecenas pharetra porta accumsan. </p>
                                    <p>In vestibulum massa quis arcu lobortis tempus. Nam pretium arcu in odio vulputate luctus. Suspendisse euismod lorem eget lacinia fringilla. Sed sed felis justo. Nunc sodales elit in laoreet aliquam. Nam gravida, nisl sit amet iaculis porttitor, risus nisi rutrum metus, non hendrerit ipsum arcu tristique est.</p>
                                </section>
                                </div>

                                </div>

                            </div>
                            <!-- end of left side content -->

            </div>
                 



            <section class="wrapper style1 container special">
                <div class="row">
                    <div class="col-4 col-12-narrower">

                        <section>
                            <header>
                                <h3>This is Something</h3>
                            </header>
                            <p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
                            <footer>
                                <ul class="buttons">
                                    <li><a href="#" class="button small">Learn More</a></li>
                                </ul>
                            </footer>
                        </section>

                    </div>
                    <div class="col-4 col-12-narrower">

                        <section>
                            <header>
                                <h3>Also Something</h3>
                            </header>
                            <p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
                            <footer>
                                <ul class="buttons">
                                    <li><a href="#" class="button small">Learn More</a></li>
                                </ul>
                            </footer>
                        </section>

                    </div>
                    <div class="col-4 col-12-narrower">

                        <section>
                            <header>
                                <h3>Probably Something</h3>
                            </header>
                            <p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
                            <footer>
                                <ul class="buttons">
                                    <li><a href="#" class="button small">Learn More</a></li>
                                </ul>
                            </footer>
             </section>

        </section>
        <!-- end of wrapper section -->

  </body>
  </html>