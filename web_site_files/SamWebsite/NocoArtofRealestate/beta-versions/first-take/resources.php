
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content=""/> 
    <!-- Meta Description -->

  
    <title>NoCo Art of Real Estate</title> <!-- Your Page Title -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


     <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />


    <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--end-fonts-->
  
    <link rel="icon" href="images/deck-installation-favicon.jpg">
    <!--Your Website Favicon-->
   
  </head>

  <body>
    <div class="container-fluid" id="banner">
    
        <a href="https://nocoartofrealestate.com"> <img src="images/" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
        <!-- YOUR WEBSITE LOGO-->
        

            <h2 align="center" class="tagline center-block">Creative Realestate for Northern Colorado</h2>

        <!--City Dropdown-->
        <div class="dropdown" id="city-selection" >

                <button class="btn btn-default dropdown-toggle dropdown-button" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

            <p>CHOOSE CITY<span class="caret"></span></p></button>

                <ul class="dropdown-menu">

                    <li><a href="https://yourwebsitename.com/">Fort Collins</a></li>

                    <li role="separator" class="divider"></li>

                    <li><a href="https://yourwebsitename.com/">Loveland </a></li>
                    
                    <li role="separator" class="divider"></li>

                    <li><a href="https://yourwebsitename.com/">Livermore</a></li>
                    
                    <li role="separator" class="divider"></li>

                    <li><a href="https://yourwebsitename.com/">Welington</a></li>
                    
                    <li role="separator" class="divider"></li>
            </ul>

        </div>  <!--- /#city-selection-->

    </div>  <!--- /#banner-->  

     <nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
  
        <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

                    <span class="sr-only">Toggle navigation </span>
        
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
        
                </button>

        </div>


            <div class="collapse navbar-collapse">

                    <ul align="center" class="nav navbar-nav" >

                        <li> <a href="index.php"><span class="center-underline">Home</span></a></li>
            
                        <li> <a href="about.php"><span class="center-underline">About</span></a></li>

                        <li> <a href="resources.php"><span class="center-underline">Resources</span></a></li>

                        <li> <a href="#"><span class="center-underline">Listings</span></a></li>

                        <li> <a class="quote" href="contact.php"><span style="color:#774220">Contact</span></a></li>
                
                
                    </ul>

            </div><!--end navbar-collapse-->

        </nav><!--end nav-->





        

        <div id=" page-wrap" class="container">
            <div id="content" class="sidebar-left twelve columns">
                <article class="post" id="post-102">
                    <div class=entry>
                        <h1 align="center"> Why Choose Mark Keller as Your Agent?</h1>
                        <div class="break">
                            <hr>
                        </div>

                         <div class="one-half">
                        
                            <p></p>
                            <h2>Home Marketing Expertise </h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor sapien, commodo euismod mollis in, aliquet iaculis libero. Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. <br /> <br />Pellentesque vulputate sapien a sem imperdiet fringilla. Phasellus eget eros pulvinar, sagittis elit nec, malesuada enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            <ul type="circle">
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            </ul>
                        </div>
                        <div class="one-half">
                            <p></p>
                            <h2>Award Winning Service </h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor sapien, commodo euismod mollis in, aliquet iaculis libero. Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. <br /> <br />Pellentesque vulputate sapien a sem imperdiet fringilla. Phasellus eget eros pulvinar, sagittis elit nec, malesuada enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            <ul type="circle">
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                        <div class="one-half">
                           
                            <p></p>
                            <h2>Buying A Home </h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor sapien, commodo euismod mollis in, aliquet iaculis libero. Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. <br /> <br />Pellentesque vulputate sapien a sem imperdiet fringilla. Phasellus eget eros pulvinar, sagittis elit nec, malesuada enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                        </div>

                     

                         <div class="one-half">
                           
                            <p></p>
                            <h2>Selling A Home </h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor sapien, commodo euismod mollis in, aliquet iaculis libero. Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. <br /> <br />Pellentesque vulputate sapien a sem imperdiet fringilla. Phasellus eget eros pulvinar, sagittis elit nec, malesuada enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                        </div>


                        <div class="callout">
                            <div class="border clearfix">
                                <div class="callout-content">
                                    <h2 class="highlight">List your Home for Sale with Mark Keller</h2><br/>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                                <div class="callout-button">
                                    <a class="button normal alternative-1" href="#" target="_self"> Contact Mark</a>
                                </div>
                            </div>

                        </div>    



                    </div>
                </article>

            </div>
            <!-- end of content -->
            <div id="sidebar" class="four-columns">
                <ul class="sidenav">
                    <li> <a href="#">Selling My House</a></li>     
                    <li> <a href="#">Why Choose Mark Keller</a></li>
                    <li> <a href="#">Selling your House FAQ</a></li>
                    <li> <a href="#">Listings</a></li>
                </ul>

                <div id="portfolio-thumbs" class="widget widget-portfolio">
                    <h3 class="title">
                        <span>Current Homes for Sale</span>
                    </h3>
                    <div class="recent-works-items clearfix">
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins1 ">
                                <img width="60"height="60" src="#someAjax" alt="House1" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins2 ">
                                <img width="60"height="60" src="#someAjax" alt="House2" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins1 ">
                                <img width="60"height="60" src="#someAjax" alt="House3" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins1 ">
                                <img width="60"height="60" src="#someAjax" alt="House4" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins1 ">
                                <img width="60"height="60" src="#someAjax" alt="House5" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                        <div class="portfolio-widget-item">
                            <a href="#" class="portfolio-widget" title="Fort Collins1 ">
                                <img width="60"height="60" src="#someAjax" alt="House6" size="(max-width:60px) 100vw, 60px" >
                            </a>
                        </div>
                    </div>
                     <!-- end of recent-works-item -->
                </div>
                <!-- end of portfolio-thumb -->
            </div>
            <!-- end of sidebar -->
        </div>
        <!-- end of page wrap -->

  </body>