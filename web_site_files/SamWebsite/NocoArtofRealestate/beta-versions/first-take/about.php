<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <meta name="description" content=""/> 
    <!-- Meta Description -->

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>About Mark Keller</title> <!-- Your Page Title -->

      <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


      <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />


      <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

      <!--Your Website Favicon-->
    <link rel="icon" href="images/deck-installation-favicon.jpg">
   
  </head>

  <body>

  <div class="container-fluid" id="banner">
  
  <a href="https://nocoartofrealestate.com"> <img src="images/" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
  <!-- YOUR WEBSITE LOGO-->
  

  <h2 align="center" class="tagline center-block">Creative Realestate for Northern Colorado</h2>

    <!--City Dropdown-->
    <div class="dropdown" id="city-selection" >

        <button class="btn btn-default dropdown-toggle dropdown-button" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

            <p>CHOOSE CITY<span class="caret"></span></p></button>

            <ul class="dropdown-menu">

            <li><a href="https://yourwebsitename.com/">Fort Collins</a></li>

            <li role="separator" class="divider"></li>

            <li><a href="https://yourwebsitename.com/">Loveland </a></li>
            
            <li role="separator" class="divider"></li>

            <li><a href="https://yourwebsitename.com/">Livermore</a></li>
            
            <li role="separator" class="divider"></li>

            <li><a href="https://yourwebsitename.com/">Welington</a></li>
            
            <li role="separator" class="divider"></li>
            </ul>

    </div>  <!--- /#city-selection-->

</div>  <!--- /#banner-->  
    
  
  
<nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
  
      
  
  <div class="navbar-header">

         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

             <span class="sr-only">Toggle navigation </span>
 
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
 
        </button>

 </div>


 <div class="collapse navbar-collapse">

          <ul align="center" class="nav navbar-nav" >

             <li> <a href="index.php"><span class="center-underline">Home</span></a></li>
 
             <li> <a href="about.php"><span class="center-underline">About</span></a></li>

             <li> <a href="resources.php"><span class="center-underline">Resources</span></a></li>

             <li> <a href="#"><span class="center-underline">Listings</span></a></li>

             <li> <a class="quote" href="contact.php"><span style="color:#774220">Contact</span></a></li>
     
     
         </ul>

  </div><!--end navbar-collapse-->

</nav><!--end nav-->
  


  <div class="about-banner">
  
      <h2 align="center"> ABOUT MARK KELLER</h2>

  </div>  <!--- /.about-banner-->


  <div class="about">
  
    <div class="row">
      
      <div class="col-md-6 col-md-offset-3" align="center">


<p> Mark has lived in Northern Colorado since 1972. He was rated #5 Broker in the Rocky Mountain Region. As a Special Ed teacher for 27 yrs he is an expert at working w/people in difficult and emotional situations, AKA purchasing and selling real estate. His open and cheerful attitude are infectious to everyone he is in contact with. If you have a referral and want to know your client will be taken care of, Mark is the agent to call.</p><br /><br />

<p align="center"><img src="images/mark-keller-pic.jpg" alt="mark-keller-pic" class="img-responsive" /></p><br /><br />

<p>Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue. Cras sit amet ultricies eros, vel blandit libero. Suspendisse vitae velit magna. Praesent semper odio magna.</p>

<ul type="circle">
    <li>Sed volutpat dapibus nunc et ultrices. Vestibulum at porttitor augue.</li>

    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li> 

    <li>Pellentesque vulputate sapien a sem imperdiet fringilla.</li>
</ul>

        </div> <!--- /.col-md-->

    </div> <!--- /.row-->

  </div> <!--- /.about-->


<a href="https://yourwebsitename.com/get-a-quote.php"><button type="submit" id="get-quote" class="btn btn-lg center-block">GET A QUOTE</button></a><br /> <br /> <br />


<div class="social-links" align="center">

    <!--- Your Social Media Accounts go here-->

    <p>CONNECT WITH US</p>

    <!--- INSTAGRAM -->
    <a href="https://www.instagram.com/YOUR-INSTGRAM-ACCOUNT" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24">
    <path fill="#774220" d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />

    </svg></a>


      <!--- FACEBOOK -->
    <a href="https://www.facebook.com/YOUR-FACEBOOK-ACCOUNT" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24"><path fill="#774220" d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" /></svg></a><br /> <br />  

  </div> <br /> 



<footer>

    <div align="center" id="footer">

      <a href="https://yourwebsitename.com/composite-decking.php">COMPOSITE DECKING</a> 

      <a href="https://yourwebsitename.com/hardwood-decking.php">HARDWOOD DECKING</a> <a href="https://yourwebsitename.com/blog.html">BLOG</a> 

      <a href="https://yourwebsitename.com/get-a-quote.php">GET A QUOTE</a>

    </div><!--end of footer-->


    <div align="center" class="footerlinks">

      <a href="https://yourwebsitename.com/about.html">About</a> 

      <a href="https://yourwebsitename.com/privacy-policy.html" rel="nofollow">Privacy Policy</a> 

      <a href="https://yourwebsitename.com/terms-and-conditions.html" rel="nofollow">Terms &amp; Conditions</a>

      <a href="https://yourwebsitename.com/contact.php" rel="nofollow">Contact</a>

     
      <p align="center" class="text-muted copyright">&copy; yourwebsitename.com</p>

    </div><!--end footerlinks-->

  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
  </script> 


  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js">
  </script> 
   
   <!--FIXED NAVBAR SCRIPT-->
  <script>
  $('#topnavbar').affix({
     offset: {
         top: $('#banner').height()
     }   
  }); 
  </script>


  </body>

</html>