<!--Form submit php control--->
<?php

 if ($_POST["submit"]) {

      
      
     if (!$_POST['name']) {

       $error="<br />Please enter your name";

     }
      
     if (!$_POST['email']) {

       $error.="<br />Please enter your email address";

     }
     
      
      if (!$_POST['phone']) {
     
      $error.="<br /> Please enter your phone number";
     
     }

       if (!$_POST['city-name']) {
     
      $error.="<br /> Please select your city";
     
     }
      
     if ($_POST['email']!="" AND !filter_var($_POST['email'],
FILTER_VALIDATE_EMAIL)) {
      
     $error.="<br />Please enter a valid email address";

     }
     
      
     if ($error) {

 $result='<div class="alert alert-danger"><strong>There were error(s)
in your form:</strong>'.$error.'</div>';

     } else {

       /* THE EMAIL WHERE YOU WANT TO RECIEVE THE CONTACT MESSAGES */
  if (mail("info@focoroofers.com", "Rent Website", 


"Name: ".$_POST['name']."
Email: ".$_POST['email']."
Phone: ".$_POST['phone']." 
City: ".$_POST['city-name'])) {
$result='<div class="alert alert-success"> <strong> Thank
you!</strong> We\'ll get back to you shortly.</div>';
} else {
$result='<div class="alert alert-danger">Sorry, there was
an error sending your message. Please try again later.</div>';
}
}
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="Fort Collins Roofing quote: Get roofing prices in Colorado for all your roofing and construction needs. Call or email us today and tell us more about your needs."/> 
    <!-- Meta Description -->

     <meta name="keywords" content="Residential Roofing, Roof Replacement, Shingle Roof Contractor, Commercial Roofing, Leak Repair, Roof Repair, Metal Roof Contractor, 
     New Construction Roofing, Roof Materials, 24 Hour/Emergency Service"/> 
    <!-- Meta Description -->
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WP8GWMW');</script>
    <!-- End Google Tag Manager -->

  
  <title>Contact Me for A Quote</title> <!-- Your Page Title -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


     <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />


    <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--end-fonts-->
  
    <link rel="icon" href="images/deck-installation-favicon.jpg">
    <!--Your Website Favicon-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127473022-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127473022-1');
        </script>
   
  </head>

  <body>
  
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WP8GWMW"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
  <div class="container-fluid" id="banner">
  
        <a href="https://yourwebsitename.com"> <img src="images/hammer_wrench-step-0.jpg" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
        <!-- YOUR WEBSITE LOGO-->
        

        <h2 align="center" class="tagline center-block">Roofer Pricing Options</h2>

    
  
  <!-- Begining of Nav bar -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
  
      
  
           <div class="navbar-header">
      
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      
                      <span class="sr-only">Toggle navigation </span>
          
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
          
                 </button>
      
          </div>

    
    <div class="collapse navbar-collapse">
      
                   <ul align="center" class="nav navbar-nav" >
                      <li> <a href="index.php"><span class="center-underline">HOME</span></a></li>
          
                      <li> <a href="hail-dmg.php"><span class="center-underline">ROOFING PROCESS</span></a></li>

                      <li> <a href="material-pric-des.php"><span class="center-underline">MATERIAL AND PRICING </span></a></li>

                      <li> <a href="get-quote.php"><span class="center-underline">GET QUOTE</span></a></li>
              
              
                  </ul>

    </div><!--end navbar-collapse-->

</nav><!--end nav-->



   <!--GET A QUOTE FORM-->
   <div class="get-quote-form">

      <div class="row">

        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

          <form method="post">
            <p align="center">Interested in Getting A Quote ?<br>
            <span style="font-weight: bold; font-size: 50px; position:relative; top:5px; line-height: 50px;">Contact Sam Schoenberg</span><br>
            <br>
            <span style="font-weight: bold; font-size: 50px; position:relative; top:5px; line-height: 50px;">Phone: 970 420 9039</span><br>
            <br>
            <div class="form-group center-block">
              <div class="icon-addon addon-lg">
                <input class="form-control" name="name" placeholder="Name" type="text" value="<?php echo $_POST['name']; ?>"> 
                <label class="glyphicon glyphicon glyphicon-user" for="name" title="name"></label>
              </div>
            </div>

            <div class="form-group center-block">
              <div class="icon-addon addon-lg">
                <input class="form-control" name="email" placeholder="Email" type="email" value="<?php echo $_POST['email']; ?>"> 
                <label class="glyphicon glyphicon-envelope" for="email" rel="tooltip" title="email"></label>
              </div>
            </div>

            <div class="form-group center-block">
              <div class="icon-addon addon-lg">
                <input class="form-control" name="phone" placeholder="Phone" type="tel" value="<?php echo $_POST['phone']; ?>"> 
                <label class="glyphicon glyphicon-earphone" for="email" rel="tooltip" title="email"></label>
              </div>
            </div>

            <div class="form-group center-block">
            <select align="center" name="city-name" class="form-control citynames center-block" value="<?php echo $_POST['city-name']; ?>" >
              <option disabled selected><strong>Choose your city</strong></option>

                <option>Fort Collins</option>
                <option>Loveland</option>
                <option>Livermore</option>
                <option>Wellington</option>

                </select> 
            
                <p align="center"> <input type="submit" name="submit" class="btn btn-default btn-lg quote-submit center-block" value="GET A QUOTE" ></input/> </p>

            </form>

          </div>


          <div class="col-md-6 col-md-offset-3">

            <!--Form Success Message will display here-->
            <p align="center" id="result"> <strong> <?php echo $result; ?> </strong></p>

          </div>
           <!--end of Form Success/ Failure  -->

      </div><!--end row-->

</div><!--end get-quote-form-->




<br /> <br /> <br />


<div class="social-links" align="center">


<p>CONNECT WITH US</p>

<a href="https://www.linkedin.com/in/sam-schoenberg-61000596/"><svg  width="35" height="35" viewBox="0 0 24 24">
          <path fill="#454867" d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z"/></svg></a>

<a href="https://www.facebook.com/sam.schoenberg.5" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24"><path fill="#774220" d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
</svg></a>
 
 <br /> <br />  

</div> <br /> 





<footer>

    <div align="center" id="footer">

      <a href="hail-dmg.php">ROOFING PROCESS</a>  

      <a href=material-pric-des.php>MATERIAL AND PRICING</a>

      <a href="get-quote.php">GET A QUOTE</a>

    </div><!--end of footer-->



    <div align="center" class="footerlinks">

    <a href="404-error-page.php">About</a> 

    <a href="privacy-policy.php" rel="nofollow">Privacy Policy</a> 

    <a href="404-error-page.php" rel="nofollow">Terms &amp; Conditions</a>

    <a href="get-quote.php" rel="nofollow">Contact</a>

     
      <p align="center" class="text-muted copyright">&copy; schoenbergwebtools.com</p>

    </div><!--end footerlinks-->

  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
  </script> 
  <!-- Include all compiled plugins (below), or include individual files as needed -->

   
   <!--FIXED NAVBAR SCRIPT-->
  <script src="js/bootstrap.min.js">
  </script> 
  <script>
  $('#topnavbar').affix({
     offset: {
         top: $('#banner').height()
     }   
  }); 
  </script>

 

  </body>
  </html>