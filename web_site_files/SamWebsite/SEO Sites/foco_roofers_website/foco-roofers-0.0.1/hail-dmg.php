<!--Form submit php control--->
<?php

 if ($_POST["submit"]) {

      
      
     if (!$_POST['name']) {

       $error="<br />Please enter your name";

     }
      
     if (!$_POST['email']) {

       $error.="<br />Please enter your email address";

     }
     
      
      if (!$_POST['phone']) {
     
      $error.="<br /> Please enter your phone number";
     
     }

       if (!$_POST['city-name']) {
     
      $error.="<br /> Please select your city";
     
     }
      
     if ($_POST['email']!="" AND !filter_var($_POST['email'],
FILTER_VALIDATE_EMAIL)) {
      
     $error.="<br />Please enter a valid email address";

     }
     
      
     if ($error) {

 $result='<div class="alert alert-danger"><strong>There were error(s)
in your form:</strong>'.$error.'</div>';

     } else {

       /* THE EMAIL WHERE YOU WANT TO RECIEVE THE CONTACT MESSAGES */
  if (mail("info@focoroofers.com", "Rent Website", 


"Name: ".$_POST['name']."
Email: ".$_POST['email']."
Phone: ".$_POST['phone']." 
City: ".$_POST['city-name'])) {
$result='<div class="alert alert-success"> <strong> Thank
you!</strong> We\'ll get back to you shortly.</div>';
} else {
$result='<div class="alert alert-danger">Sorry, there was
an error sending your message. Please try again later.</div>';
}
}
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

     <meta name="description" content="Fort Collins Roofing quote: Get roofing prices in Colorado for all your roofing and construction needs. Call or email us today and tell us more about your needs."/> 
    <!-- Meta Description -->

     <meta name="keywords" content="Residential Roofing, Roof Replacement, Shingle Roof Contractor, Commercial Roofing, Leak Repair, Roof Repair, Metal Roof Contractor, 
     New Construction Roofing, Roof Materials, 24 Hour/Emergency Service"/> 
    <!-- Meta Description -->

    <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-WP8GWMW');</script>
    <!-- End Google Tag Manager -->

  
    <title>Hail Damage 2019</title> <!-- Your Page Title -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


     <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />


    <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--end-fonts-->
  
    <link rel="icon" href="images/deck-installation-favicon.jpg">
    <!--Your Website Favicon-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127473022-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127473022-1');
        </script>
   
  </head>

  <body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WP8GWMW"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
  <a href="focoroofers.com"> <img src="" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
        <!-- YOUR WEBSITE LOGO--> 
        <br />
        <br />
  <div class="container-fluid" id="banner">

        <h2 align="center" class="tagline center-block">Roofer Pricing Options</h2>

    
  
  
  <nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
  
      
  
           <div class="navbar-header">
      
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      
                      <span class="sr-only">Toggle navigation </span>
          
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
          
                 </button>
      
          </div>

     <div class="collapse navbar-collapse">
      
                <ul align="center" class="nav navbar-nav" >
                    <li> <a href="index.php"><span class="center-underline">HOME</span></a></li>
          
                      <li> <a href="hail-dmg.php"><span class="center-underline">ROOFING PROCESS</span></a></li>

                      <li> <a href="material-pric-des.php"><span class="center-underline">MATERIAL AND PRICING </span></a></li>

                      <li> <a href="get-quote.php"><span class="center-underline">GET QUOTE</span></a></li>
              
              
                  </ul>

</div><!--end navbar-collapse-->

  </nav><!--end nav-->


<!--- /Form success/failure message -->
<div class="display-banner">

<div class="quote-main">

  <a href="get-quote.php"><button type="submit" id="free-quote" class="btn btn-lg center-block">GET YOUR FREE QUOTE</button></a><br />


      <div class="row">

            <div class="col-md-6 col-md-offset-3">

          
                            <!--Form Success Message will display here-->
                    <p align="center" id="result"> <strong> <?php echo $result; ?> </strong></p>

              </div>

      </div> <!--- /Form success/failure message -->





<div class="container">

<!-- DESCRIPTION 1 -->
<div class="description">

<div class="row"> <br />

  <div class="col-md-8 col-md-offset-2" align="center">

      <h2 align="center">All About Roof Replacement and Repair</h2>

      <p  class="descrip" align="left"> 
        Is your roof in need of repair? Does it have a leak, or broken <a href=-.php-> shingles </a>? Whether it’s a damage
        issue or a simple update of the look and style, there are a plethora of options available to help.
        And if you are confused, or unsure of where to begin, there’s no need to worry; here is all the
        information you need to get started on a great roofing renovation project for your home or
        business.</p> <br />

       <h3 align="left"> Residential Roofing vs. Commercial Roofing</h3>

       <p class="descrip" align="left"> Roofing services stretch from residential to commercial areas, and while the two may seem
        similar on the surface, there are important distinctions to be made both in cost and design. For
        instance, commercial roofs often require far more complex and layered designs than residential
        roofs, driving up the expense during a commercial project, due to the need to hire top-quality
        professional roofing contractors. Additionally, the design for commercial roofs are usually far
        different than residential; commercial roofs tend to be flatter, far larger, and frequently have to be
        built around air flow systems, piping, and chimney smoke stakes.

        One similarity between the two roof types is with the range of materials that can be used,
        including shingles (asphalt, slate, ceramic, and tile), wood, metal, and more. Both residential and
        commercial designs can use any of the <a href=-.php->roof materials listed previously</a>, although the choice of
        material often depends on what the contractor thinks best.</p><br />
        
        <h3 align="left"> Different Types of Roof Renovations</h3>

        <p class="descrip" align="left">Depending on your project need, there are several different ways to fix your roof or update its
        style. If you only require <a href="hail-dmg.php">fixing a leak</a>, there are a variety of top-notch contractors offering leak
        repair in almost every area in the country. If your requirements are a bit heavier, and you need a
        full-on <a href="https://www.usa.gov/repairing-home">roof repair, then looking at the different roofing contractors</a> in your area should bring upgood results. Finally, if you’re looking for a full-on renovation project, a company specializing in
        roof replacement will most likely be your best bet for a top-quality product.</p><br />
       
        <h3 align="left">Kinds of Contractors</h3>

        <p class="descrip" align="left">Depending on the project, there are a variety of contractors that can help, many of which
        specialize in working with certain materials. There are shingle roof contractors, who are
        professionals at repairing and installing shingle roofs. If your needed design or endeavor is more
        commercial-oriented, you may want to hire a metal roof contractor, top-notch installers of metals
        roofs. However, <a href="https://www.expertise.com/co/fort-collins/roofing">do not forget that new construction roofing is not something you should take
        lightly</a>; make sure to compare several different contractors in price and reputation. Some
        companies may even offer 24 hour emergency service as part of their plan. </p> <br />

    

  </div>  <!--- /.col-md-->

</div>  <!--- /.row-->

</div>  <!--- /.description1-->
<br /> <br /> <br /> 

<!-- DESCRIPTION 2 -->
<div class="description">

    <div class="row"> <br />

      <div class="col-md-8 col-md-offset-2" align="center">

      <h2 align="center" class="description">Hail Damage and Roof Repair</h2><br />

          <p align="center" class="description"> Hail is a type of precipitation that occurs when air updrafts (upward air currents) in thunderstorms carry raindrops into the upper level clouds where temperatures are below freezing. A small particle of ice forms around some type of nucleus. It could be a tiny ice crystal, spec of dust, frozen raindrop or something else.

The frozen ice particle then drops within the <a href="https://en.wikipedia.org/wiki/Hail">cloud back below freezing temperatures</a>, where it picks up more moisture from rain drops and water vapor. Eventually it gets caught in another updraft and freezes again.

The stronger the updrafts, the longer a hailstone circulates up and down… growing larger and larger until gravity no longer allows it to remain lofted.

At that point, the hail falls toward the ground, where it can inflict damage on people or property.</p><br /> 

          <h2 align="center" class="description">How to check for hail damage</h2><br />
          <p align="left" class="description">Check the ridge cap of the roof for dents. This area of the roof will receive the most damage from hail since it is flat and will take a direct hit in a storm. Look at the shingles. Check the whole shingle, as well as the edges, for signs of damage.</p><br />
          <h2 align="left" class="description">3 different types of Hail Damage.</h2><br /> 
          <li align="left" class="description" > Check for missing pieces in the asphalt of the roof. Look for areas on the shingle that have exposed, black substrate. This means the granules of the asphalt have been compromised or knocked off by something.</li><br/>
          <li align="left" class="description"> Search for bruising in the shingles. Dents from hail are not always completely visible. Run your hand over the shingle to feel for small dimples in the surface. Press 1 of the dimples to see if it has any give. If it does, this is a sign that the shingle has begun some deterioration.</li><br />
          <li align="left" class="description"> Look for cracking in the shingle. Large hail can make a circular crack if the hail hits it hard enough.</li><br />


      </div>  <!--- /.col-md-->

  </div>  <!--- /.row-->

</div>  <!--- /.description 2-->



</div> <!--end of container-->

<br /><br /><br />


<br /> <br /> <br />

<div class="social-links" align="center">

<p>CONNECT WITH US</p>

<a href="https://www.linkedin.com/in/sam-schoenberg-61000596/"><svg  width="35" height="35" viewBox="0 0 24 24">
          <path fill="#454867" d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z"/></svg></a>

<a href="https://www.facebook.com/sam.schoenberg.5" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24"><path fill="#774220" d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
</svg></a>
 
 <br /> <br />  

</div> <br /> 





<footer>

      <div align="center" id="footer">

        <a href="hail-dmg.php">ROOFING PROCESS</a> 

        <a href=material-pric-des.php>MATERIAL AND PRICING</a>

        <a href="get-quote.php">GET A QUOTE</a>

    </div><!--end of footer-->


    <div align="center" class="footerlinks">

      <a href="404-error-page.php">About</a> 

      <a href="privacy-policy.php" rel="nofollow">Privacy Policy</a> 

      <a href="404-error-page.php" rel="nofollow">Terms &amp; Conditions</a>

      <a href="get-quote.php" rel="nofollow">Contact</a>
     
      <p align="center" class="text-muted copyright">&copy; schoenbergwebtools.com</p>

    </div><!--end footerlinks-->

  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
  </script> 
  <!-- Include all compiled plugins (below), or include individual files as needed -->

   
   <!--FIXED NAVBAR SCRIPT-->
  <script src="js/bootstrap.min.js">
  </script> 
  <script>
  $('#topnavbar').affix({
     offset: {
         top: $('#banner').height()
     }   
  }); 
  </script>


  </body>
  </html>