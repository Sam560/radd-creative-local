<?php
/**
 * Created by PhpStorm.
 * User: sam6061
 * Date: 3/1/19
 * Time: 8:39 AM
 */
?>
<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Fri Mar 01 2019 15:17:31 GMT+0000 (UTC)  -->
<html data-wf-page="5c780bb1cf9614c2638c4cff" data-wf-site="5c780bb0cf9614f1a18c4cfc">
<head>
    <meta charset="utf-8">
    <title>Schoenberg Web Tools</title>
    <meta content="Bringing Dreams to the Web" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/schoenbergwebtools2-0.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Droid Serif:400,400italic,700,700italic","Indie Flower:regular","Gloria Hallelujah:regular","Satisfy:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
<section data-ix="page-loader" class="loading-section">
    <div class="loader-holder"><img src="images/bold-gif-preloader.gif" width="160" alt="" class="loading-image"></div>
</section>
<!--nav-bar-->
<header class="fixed-header">
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="hero-section-slide-up" class="navbar w-nav">
        <div class="wide-container nav-container w-container"><a href="index.php" class="brand w-nav-brand w--current"><img src="images/youtube_profile_image.png" width="53" alt=""></a>
            <div class="menu-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
            <nav role="navigation" class="nav-menu w-clearfix w-nav-menu"><a href="projects.html" class="nav-link w-nav-link">Portfolio</a><a href="404-error-page.php" class="nav-link w-nav-link">About</a><a href="services.html" class="nav-link w-nav-link">services</a><a href="work-with-us.html" class="nav-link button-link fixed-button-link w-nav-link">Work with Us</a></nav>
        </div>
    </div>
</header>

<section id="Home" data-ix="hero-section-slide-up" class="section hero-section">
    <h1 class="hero-h1"><span class="sub-h1">Bringing Dreams TO the Web </span><br>SChoenberg web tools</h1>
</section>
<div class="site-wrapper">
    <section id="Work" class="section work-section">
        <div class="wid-noco-contain work-container w-container">
            <h2 class="section-heading center">What we are about</h2>
            <div class="noco-work-item">
                <div class="section-heading featured">Current Projects and Info<br>‍</div>
                <a href="/work-item" class="work-link featured-link w-inline-block">
                    <div> </div>
                    <div class="text-block">Building Real Solutions For Real People </div>
                </a><img src="images/mountains.jpeg" data-ix="featured-image-scroll" width="1132" height="668" alt="" class="featured-noco-item"></div>
            <div class="w-row">
                <div class="w-col w-col-7 w-col-stack">
                    <div class="w-row">
                        <div class="w-col w-col-5">
                            <div data-ix="show-work-link-on-hover" class="noco-work-item short short-image-1"></div>
                        </div>
                        <div class="w-col w-col-7">
                            <div data-ix="show-work-link-on-hover" class="noco-work-item short right short-right-image-1">
                                <a href="services.html" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                                    <div class="text-block-3">SEO </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div data-ix="show-work-link-on-hover" class="noco-work-item tall tall-image-1">
                        <a href="/work-item" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                            <div class="text-block-2">Foco Roofers</div>
                        </a>
                    </div>
                    <div class="w-row">
                        <div class="w-col w-col-5">
                            <div data-ix="show-work-link-on-hover" class="noco-work-item short short-image-2"></div>
                        </div>
                        <div class="w-col w-col-7">
                            <div data-ix="show-work-link-on-hover" class="noco-work-item short right">
                                <a href="/work-item" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                                    <div class="text-block-7">Noco Art Of Real Estate</div>
                                </a><img src="images/nocoartofrealestate-p-500_1.jpeg" width="334" data-ix="short-wide-image-scroll" alt="" class="short-wide-image"></div>
                        </div>
                    </div>
                    <div data-ix="show-work-link-on-hover" class="noco-work-item tall tall-image-3">
                        <a href="services.html" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                            <div class="text-block-5">Social Media Integration </div>
                        </a>
                    </div>
                </div>
                <div class="w-col w-col-5 w-col-stack">
                    <div data-ix="show-work-link-on-hover" class="noco-work-item tall tall-image-2">
                        <a href="services.html" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                            <div class="text-block-4">Web Design </div>
                        </a>
                    </div>
                    <div data-ix="show-work-link-on-hover" class="noco-work-item tall tal-image-3">
                        <a href="services.html" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                            <div class="text-block-6">Custom Applications </div>
                        </a>
                    </div>
                    <div data-ix="show-work-link-on-hover" class="noco-work-item wide wide-image-1">
                        <a href="individual-projects/books-of-discovery-project.html" data-ix="opacity-0-on-load" class="work-link w-inline-block">
                            <div class="text-block-8">Books Of Discovery </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="About" class="section about-section">
        <div class="wid-noco-contain w-container"><img src="images/youtube_profile_image.png" width="125" alt="" class="elk">
            <h3 class="section-heading about-heading">A little about me </h3>
            <p class="meet-heading">I have been a developer for three years with a range of experience in building websites and web applications. I pride myself in my ability to listen to peoples needs and not assume that cool gimmicks or quick fix are what clients desire. I am very active person and enjoy a wide variety of activities and exploring other parts of Colorado. I look forward to connecting with you about your project, and helping guided you to your most authentic expression of your web presence. </p>
            <div class="w-row">
                <div class="w-col w-col-3">
                    <div data-ix="dial" class="dial">
                        <div data-ix="color-dial" class="dial color-dial">
                            <div class="dial inner-dial">
                                <div class="dial cover">50%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-col w-col-3">
                    <div data-ix="dial" class="dial">
                        <div data-ix="color-dial-backwards" class="dial color-dial _30">
                            <div class="dial inner-dial">
                                <div class="dial cover">30%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-col w-col-3">
                    <div data-ix="dial" class="dial">
                        <div data-ix="color-dial" class="dial color-dial _15">
                            <div class="dial inner-dial">
                                <div class="dial cover">15%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-col w-col-3">
                    <div data-ix="dial" class="dial">
                        <div data-ix="color-dial-backwards" class="dial color-dial">
                            <div class="dial inner-dial">
                                <div class="dial cover">5%</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="meet-section-backdrop">
            <div class="w-container">
                <div data-ix="profile-row" class="profiles-row w-row">
                    <div class="w-col w-col-4">
                        <div class="profile-holder _2"><img src="images/profile-pic-2.jpg" width="243" height="189" srcset="images/profile-pic-2-p-500.jpeg 500w, images/profile-pic-2-p-800.jpeg 800w, images/profile-pic-2-p-1080.jpeg 1080w, images/profile-pic-2.jpg 1536w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 243px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
                    </div>
                    <div class="w-col w-col-4">
                        <div class="profile-holder _2"><img src="images/sam-photo.jpg" width="186" alt="" class="avatar">
                            <div class="avatar-heading">Sam Schoenberg</div>
                            <div>CEO/ Lead Designer </div>
                        </div>
                    </div>
                    <div class="w-col w-col-4">
                        <div class="profile-holder _2"><img src="images/profile-pic-3.jpg" width="226" height="189" srcset="images/profile-pic-3-p-500.jpeg 500w, images/profile-pic-3.jpg 960w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 226px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
                    </div>
                </div>
            </div>
            <div class="w-row">
                <div class="w-col w-col-6">
                    <div class="section testimonials-section">
                        <div class="wid-noco-contain w-container">
                            <div data-animation="slide" data-duration="2000" data-infinite="1" data-delay="4000" data-autoplay="1" data-easing="ease-out-expo" class="testimonials-slider w-slider">
                                <div class="w-slider-mask">
                                    <div class="slide w-slide"><img src="images/testimonial-1.png" width="65" alt="" class="avatar testimonials">
                                        <div>“ Schoenberg Web tools is like a <span class="testimonial-highlight">fresh breath of air</span> ”</div>
                                    </div>
                                    <div class="slide w-slide"><img src="images/testimonial-2.png" width="75" alt="" class="avatar testimonials">
                                        <div>“ schoenberg web tools Gave Our Team a <span class="testimonial-highlight">New Perspective</span> ”</div>
                                    </div>
                                    <div class="slide w-slide"><img src="images/testimonial-3.png" width="75" alt="" class="avatar testimonials">
                                        <div>“ Finally, a designer that listens, and totally <span class="testimonial-highlight">gets my needs</span> ”</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-col w-col-6">
                    <div id="Contact" class="section contact-section">
                        <div class="wid-noco-contain w-container">
                            <h4 class="section-heading contact-heading">Work With Us</h4>
                            <div class="w-form">
                                <form id="email-form" name="email-form" data-name="Email Form"><a href="#" class="form-link w-inline-block"><label for="name" class="label">Name</label><input type="text" id="name" name="name" data-name="Name" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Email-2" class="label">Email</label><input type="email" id="Email-2" name="Email" data-name="Email" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Message" class="message-label">Message</label><textarea id="Message" name="Message" maxlength="5000" data-name="Message" class="text-field text-area w-input"></textarea></a><input type="submit" value="Send" data-wait="Please wait..." class="submit-button w-button"></form>
                                <div class="success w-form-done">
                                    <div>Thanks! Your Email is on its way to us.</div>
                                </div>
                                <div class="error w-form-fail">
                                    <div>Oops! Something went wrong while submitting the form :(</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section contact-and-footer">
                <section class="section footer-section">
                    <div class="wid-noco-contain w-container"><img src="images/youtube_profile_image.png" width="174" alt="" class="footer-logo"></div><a href="#" class="w-inline-block"><img src="images/facebook3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/twitter3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/rocket_1rocket.png" width="25" alt="" class="social-icon"></a>
                    <div class="copyright">©2017 schoenberg web tools</div><a href="index.html" class="footer-link w--current">Home</a><a href="404-error-page.php" class="footer-link">About</a><a href="#Work" class="footer-link">Portfolio</a><a href="#Work" class="footer-link">SERVICES</a><a href="work-with-us.html" class="footer-link">Contact</a></section>
            </div>
        </div>
    </section>
</div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="js/webflow.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
