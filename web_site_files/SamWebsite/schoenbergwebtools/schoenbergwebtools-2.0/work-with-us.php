<?php
/**
 * Created by PhpStorm.
 * User: sam6061
 * Date: 3/1/19
 * Time: 8:45 AM
 */
?>
<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Fri Mar 01 2019 15:17:31 GMT+0000 (UTC)  -->
<html data-wf-page="5c78c1474948aa253583ad5a" data-wf-site="5c780bb0cf9614f1a18c4cfc">
<head>
    <meta charset="utf-8">
    <title>work-with-us</title>
    <meta content="work-with-us" property="og:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/schoenbergwebtools2-0.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Droid Serif:400,400italic,700,700italic","Indie Flower:regular","Gloria Hallelujah:regular","Satisfy:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
<header class="fixed-header">
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="hero-section-slide-up" class="navbar w-nav">
        <div class="wide-container nav-container w-container"><a href="index.html" class="brand w-nav-brand"><img src="images/youtube_profile_image.png" width="53" alt=""></a>
            <div class="menu-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
            <nav role="navigation" class="nav-menu w-clearfix w-nav-menu"><a href="projects.html" class="nav-link w-nav-link">Portfolio</a><a href="#" class="nav-link w-nav-link">About</a><a href="services.html" class="nav-link w-nav-link">services</a><a href="work-with-us.html" class="nav-link button-link fixed-button-link w-nav-link w--current">Work with Us</a></nav>
        </div>
    </div>
</header>
<div class="section-2">
    <div class="container">
        <div class="w-layout-grid contact-form-grid">
            <div id="w-node-d42ac54db6bc-3583ad5a" class="contact-form-wrap">
                <div class="contact-form-heading-wrap">
                    <h2 class="contact-heading">Contact us</h2>
                    <div class="paragraph-light">If you are curious about renting this site or have other questions about how to help your online presence. Feel free to send me a link and I will answer your inquires. </div>
                </div>
                <div class="contact-form w-form">
                    <form data-name="Get In Touch Form" name="wf-form-Get-In-Touch-Form" id="wf-form-Get-In-Touch-Form" class="get-in-touch-form"><label for="name-2">Name</label><input type="text" class="text-field-2 cc-contact-field w-input" maxlength="256" name="Name-2" data-name="Name 2" placeholder="Enter your name" id="Name-2"><label for="Email-3">Email Address</label><input type="email" class="text-field-2 cc-contact-field w-input" maxlength="256" name="Email-3" data-name="Email 3" placeholder="Enter your email" id="Email-3" required=""><label for="Message-2">Message</label><textarea id="Message-2" name="Message-2" placeholder="Hi there, I’m reaching out because I think we can collaborate…" maxlength="5000" data-name="Message 2" class="text-field-2 cc-textarea cc-contact-field w-input"></textarea><input type="submit" value="Submit" data-wait="Please wait..." class="button-2 w-button"></form>
                    <div class="status-message cc-success-message w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="status-message cc-error-message w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
            </div>
            <div id="w-node-d42ac54db6d4-3583ad5a">
                <div class="details-wrap">
                    <div class="label-2">Our offices </div>
                    <div class="paragraph-light">Schoenberg Web Tools </div>
                </div>
                <div class="details-wrap">
                    <div class="label-2">OUR OFFICE</div>
                    <div class="paragraph-light">1300 West Magnolia Street</div>
                </div>
                <div class="details-wrap">
                    <div class="label-2">WORKING HOURS</div>
                    <div class="paragraph-light">8AM - 3PM, Mon to Fri</div>
                </div>
                <div class="details-wrap">
                    <div class="label-2">CONTACT</div><a href="mailto:contact@business.com?subject=You&#x27;ve%20got%20mail!" class="contact-email-link">schoenbergwebtools@gmail.com</a>
                    <div class="paragraph-light">(970)420-9039</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="meet-section-backdrop">
    <div class="w-container">
        <div data-ix="profile-row" class="profiles-row w-row">
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/profile-pic-2.jpg" width="243" height="189" srcset="images/profile-pic-2-p-500.jpeg 500w, images/profile-pic-2-p-800.jpeg 800w, images/profile-pic-2-p-1080.jpeg 1080w, images/profile-pic-2.jpg 1536w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 243px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
            </div>
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/sam-photo.jpg" width="186" alt="" class="avatar">
                    <div class="avatar-heading">Sam Schoenberg</div>
                    <div>CEO/ Lead Designer </div>
                </div>
            </div>
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/profile-pic-3.jpg" width="226" height="189" srcset="images/profile-pic-3-p-500.jpeg 500w, images/profile-pic-3.jpg 960w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 226px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
            </div>
        </div>
    </div>
    <div class="w-row">
        <div class="w-col w-col-6">
            <div class="section testimonials-section">
                <div class="wid-noco-contain w-container">
                    <div data-animation="slide" data-duration="2000" data-infinite="1" data-delay="4000" data-autoplay="1" data-easing="ease-out-expo" class="testimonials-slider w-slider">
                        <div class="w-slider-mask">
                            <div class="slide w-slide"><img src="images/testimonial-1.png" width="65" alt="" class="avatar testimonials">
                                <div>“ Schoenberg Web tools is like a <span class="testimonial-highlight">fresh breath of air</span> ”</div>
                            </div>
                            <div class="slide w-slide"><img src="images/testimonial-2.png" width="75" alt="" class="avatar testimonials">
                                <div>“ schoenberg web tools Gave Our Team a <span class="testimonial-highlight">New Perspective</span> ”</div>
                            </div>
                            <div class="slide w-slide"><img src="images/testimonial-3.png" width="75" alt="" class="avatar testimonials">
                                <div>“ Finally, a designer that listens, and totally <span class="testimonial-highlight">gets my needs</span> ”</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-col w-col-6">
            <div id="Contact" class="section contact-section">
                <div class="wid-noco-contain w-container">
                    <h4 class="section-heading contact-heading">Work With Us</h4>
                    <div class="w-form">
                        <form id="email-form" name="email-form" data-name="Email Form"><a href="#" class="form-link w-inline-block"><label for="name" class="label">Name</label><input type="text" id="name" name="name" data-name="Name" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Email-2" class="label">Email</label><input type="email" id="Email-2" name="Email" data-name="Email" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Message" class="message-label">Message</label><textarea id="Message" name="Message" maxlength="5000" data-name="Message" class="text-field text-area w-input"></textarea></a><input type="submit" value="Send" data-wait="Please wait..." class="submit-button w-button"></form>
                        <div class="success w-form-done">
                            <div>Thanks! Your Email is on its way to us.</div>
                        </div>
                        <div class="error w-form-fail">
                            <div>Oops! Something went wrong while submitting the form :(</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section contact-and-footer">
        <section class="section footer-section">
            <div class="wid-noco-contain w-container"><img src="images/youtube_profile_image.png" width="174" alt="" class="footer-logo"></div><a href="#" class="w-inline-block"><img src="images/facebook3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/twitter3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/rocket_1rocket.png" width="25" alt="" class="social-icon"></a>
            <div class="copyright">©2017 schoenberg web tools</div><a href="index.html" class="footer-link">Home</a><a href="404-error-page.php" class="footer-link">About</a><a href='projects.html' class="footer-link">Portfolio</a><a href="services.html" class="footer-link">SERVICES</a><a href="work-with-us.html" class="footer-link w--current">Contact</a></section>
    </div>
</div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="js/webflow.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
