<?php
/**
 * Created by PhpStorm.
 * User: sam6061
 * Date: 3/1/19
 * Time: 8:43 AM
 */
?>
<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Fri Mar 01 2019 15:17:31 GMT+0000 (UTC)  -->
<html data-wf-page="5c7824a74f58af3fca8a8ec9" data-wf-site="5c780bb0cf9614f1a18c4cfc">
<head>
    <meta charset="utf-8">
    <title>portfolio</title>
    <meta content="portfolio" property="og:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/schoenbergwebtools2-0.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic","Droid Serif:400,400italic,700,700italic","Indie Flower:regular","Gloria Hallelujah:regular","Satisfy:regular"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
<header class="fixed-header">
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="hero-section-slide-up" class="navbar w-nav">
        <div class="wide-container nav-container w-container"><a href="index.html" class="brand w-nav-brand"><img src="images/youtube_profile_image.png" width="53" alt=""></a>
            <div class="menu-button w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
            <nav role="navigation" class="nav-menu w-clearfix w-nav-menu"><a href="projects.html" class="nav-link w-nav-link w--current">Portfolio</a><a href="#" class="nav-link w-nav-link">About</a><a href="services.html" class="nav-link w-nav-link">services</a><a href="work-with-us.html" class="nav-link button-link fixed-button-link w-nav-link">Work with Us</a></nav>
        </div>
    </div>
</header>
<div class="men-four">
    <div class="div-block-25">
        <h2 data-w-id="d252851a-09f0-a5c8-3bba-5a4456062d9e" style="-webkit-transform:translate3d(0, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 80PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heading-title">Some Of My Work </h2>
        <div data-w-id="d252851a-09f0-a5c8-3bba-5a4456062da0" style="-webkit-transform:translate3d(0, 218PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 218PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 218PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 218PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="accrd-work-wrapper">
            <div id="w-node-5a4456062da1-ca8a8ec9" class="accrd-work-item">
                <div data-w-id="d252851a-09f0-a5c8-3bba-5a4456062da2" class="accrd-work-trigger"><img src="https://uploads-ssl.webflow.com/5c2fde37b3bf5fb7ac10b89e/5c776037f98fe92decc1fb9e_foco-roofers.png" width="596" alt="Foco-roofers-pic" class="image-26"></div>
                <div style="height:0PX" class="accrd-work-content"><a href="individual-projects/noco-art-of-realsestate.html" class="link-15">FoCo Roofers</a>
                    <p class="paragraph-36">Rental Site For Local Contractors Looking for a Rank and Rent Site. Currently Ranked #15 on Google for keyword &quot;Fort Collins Roofing&quot;. </p>
                </div>
            </div>
            <div id="w-node-5a4456062da9-ca8a8ec9" class="accrd-work-item">
                <div data-w-id="d252851a-09f0-a5c8-3bba-5a4456062daa" class="accrd-work-trigger"><img src="images/bod-front-page.jpeg" width="596" srcset="images/bod-front-page-p-500.jpeg 500w, images/bod-front-page-p-1080.jpeg 1080w, images/bod-front-page.jpeg 1366w" sizes="(max-width: 479px) 100vw, (max-width: 991px) 596px, 28vw" alt="" class="image-26"></div>
                <div style="height:0PX" class="accrd-work-content"><a href="individual-projects/books-of-discovery-project.html" class="link-15">Books Or Discovery</a>
                    <p class="paragraph-36">Book Store and Publisher of Manual Therapist Materials, Currently Inovield in SEO development with Schoenberg Web Tools </p>
                </div>
            </div>
            <div id="w-node-5a4456062db1-ca8a8ec9" class="accrd-work-item">
                <div data-w-id="d252851a-09f0-a5c8-3bba-5a4456062db2" class="accrd-work-trigger"><img src="images/nocoartofrealestate.jpg" width="596" srcset="images/nocoartofrealestate-p-500.jpeg 500w, images/nocoartofrealestate-p-1080.jpeg 1080w, images/nocoartofrealestate.jpg 1299w" sizes="(max-width: 479px) 100vw, 596px" alt="" class="image-27"></div>
                <div style="height:0PX" class="accrd-work-content"><a href="individual-projects/foco-roofers.html" class="link-16">NoCo Art Of Real Estate</a>
                    <p class="paragraph-36">Primer Real estate site for Mark Keller, Local Real estate Agent For Remax Advanced in Fort Collins . </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="meet-section-backdrop">
    <div class="w-container">
        <div data-ix="profile-row" class="profiles-row w-row">
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/profile-pic-2.jpg" width="243" height="189" srcset="images/profile-pic-2-p-500.jpeg 500w, images/profile-pic-2-p-800.jpeg 800w, images/profile-pic-2-p-1080.jpeg 1080w, images/profile-pic-2.jpg 1536w" sizes="(max-width: 479px) 75vw, (max-width: 767px) 243px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
            </div>
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/sam-photo.jpg" width="186" alt="" class="avatar">
                    <div class="avatar-heading">Sam Schoenberg</div>
                    <div class="text-block-14">CEO/ Lead Designer </div>
                </div>
            </div>
            <div class="w-col w-col-4">
                <div class="profile-holder _2"><img src="images/profile-pic-3.jpg" width="226" height="189" srcset="images/profile-pic-3-p-500.jpeg 500w, images/profile-pic-3.jpg 960w" sizes="(max-width: 479px) 75vw, (max-width: 767px) 226px, (max-width: 991px) 177.8541717529297px, 185px" alt="" class="avatar"></div>
            </div>
        </div>
    </div>
    <div class="w-row">
        <div class="w-col w-col-6">
            <div class="section testimonials-section">
                <div class="wid-noco-contain w-container">
                    <div data-animation="slide" data-duration="2000" data-infinite="1" data-delay="4000" data-autoplay="1" data-easing="ease-out-expo" class="testimonials-slider w-slider">
                        <div class="w-slider-mask">
                            <div class="slide w-slide"><img src="images/testimonial-1.png" width="65" alt="" class="avatar testimonials">
                                <div>&quot;Schoenberg Web Tools is like a <span class="testimonial-highlight">fresh breath of air</span> ”</div>
                            </div>
                            <div class="slide w-slide"><img src="images/testimonial-2.png" width="75" alt="" class="avatar testimonials">
                                <div>“ Schoenberg web tools Gave Our Team a <span class="testimonial-highlight">New Perspective</span> ”</div>
                            </div>
                            <div class="slide w-slide"><img src="images/testimonial-3.png" width="75" alt="" class="avatar testimonials">
                                <div>“ Finally, a Designer that listens, and totally <span class="testimonial-highlight">gets My needs</span> ”</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-col w-col-6">
            <div id="Contact" class="section contact-section">
                <div class="wid-noco-contain w-container">
                    <h4 class="section-heading contact-heading">Work With Us</h4>
                    <div class="w-form">
                        <form id="email-form" name="email-form" data-name="Email Form"><a href="#" class="form-link w-inline-block"><label for="name" class="label">Name</label><input type="text" id="name" name="name" data-name="Name" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Email" class="label">Email</label><input type="email" id="Email" name="Email" data-name="Email" maxlength="256" class="text-field w-input"></a><a href="#" class="form-link w-inline-block"><label for="Message" class="message-label">Message</label><textarea id="Message" name="Message" maxlength="5000" data-name="Message" class="text-field text-area w-input"></textarea></a><input type="submit" value="Send" data-wait="Please wait..." class="submit-button w-button"></form>
                        <div class="success w-form-done">
                            <div>Thanks! Your Email is on its way to us.</div>
                        </div>
                        <div class="error w-form-fail">
                            <div>Oops! Something went wrong while submitting the form :(</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section contact-and-footer">
        <section class="section footer-section">
            <div class="wid-noco-contain w-container"><img src="images/youtube_profile_image.png" width="174" alt="" class="footer-logo"></div><a href="#" class="w-inline-block"><img src="images/facebook3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/twitter3.svg" width="25" alt="" class="social-icon"></a><a href="#" class="w-inline-block"><img src="images/pinterest.svg" width="25" alt="" class="social-icon"></a>
            <div class="copyright">©2017 schoenberg web tools</div><a href="index.html" class="footer-link">Home</a><a href="#" class="footer-link">About</a><a href="projects.html" class="footer-link w--current">Portfolio</a><a href="services.html" class="footer-link">SERVICES</a><a href="work-with-us.html" class="footer-link">Contact</a></section>
    </div>
</div>
<script src="https://d1tdp7z6w94jbb.cloudfront.net/js/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="js/webflow.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
