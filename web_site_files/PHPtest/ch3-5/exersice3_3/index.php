<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Compare Strings</title>

</head>
<body>
<h1>Validate Local Address</h1>
    <?php
    //decalre array that contains emails. 

    $email = array(
        "jsmith123@example.org",
        "john.smith.mail@example.org",
        "john.smith@example.org",
        "john.smith@example",
        "jsmith123@mail.example.org"
    );

    //foreach loop to processe the elemnts of array

     foreach ($email as $emailAddress){
         echo "The email address &ldquo;" . $emailAddress .
            "&rdquo; ";
        if(preg_match("/^(([A-Za-z]+\d+)|<br />" .
                "([A-Za-z]+\.[A-Za-z]+))" .
                "@((mail\.)?)example\.org$/i",
                $emailAddress)==1)
             echo " is a valid email address.";
        else
             echo " is not a valid e-mail address.";

     }
    
    ?>
</body>
</html>
