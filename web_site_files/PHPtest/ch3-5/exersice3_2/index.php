<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Compare Strings</title>

</head>
<html>
<body>
    <h1>Compare Strings</h1><hr />

    <?php 
        //script section, declare and intialize two string var
        $firstString = "Geek2Geek";
        $secondString = "Geezer2Geek";

        // evaulate the script to determine if they match or not
        if (!empty($firstString) && !empty($secondString)){
            if ($firstString == $secondString)
                echo "<p>Both Strings are the same.</p>";
            else{
                echo "<p>Both strings have".similar_text($firstString,
                $secondString) ."Characters in common.<br />";

                echo "<p>You must change " .
                levenshtein($firstString,$secondString) . 
                "Characters to make the strings the same. <br />";
            
            
            }
        
        }
        //else statment executes if first or second string are empty
        else 
            echo "<p> Either the \$firstString variable or
            the \$secondString variable does not contain
            a value so the two strings cannot be compared.</p>";
    ?>

</body>
</html>