<!DOCTYPE HTML PUBLIC"-/W3C/DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <title>College Interns</title>
</head>
<body>
    <h1>College Internship</h1>
    <h2>Intern Registration</h2>
    <?php
        // vars
        $errors = 0;
        $email = "";
        // validate the email address entry 
        if( empty($_POST['email']) ) {
            ++$errors;
            echo "<p>You need to enter an e-mail address.</p>\n";
        } 
         else {
            $email = stripslashes($_POST['email']);
            if(preg_match("/^[\w-]+(\.[\w-]+)*@" . "[\w-]+(\.[\w-]+)*(\.[a-zA-Z]{2,})$/i", $email) == 0 ) {     // updated email preg_match
                ++$errors;
                echo "<p>You need to enter a valid" . " e-mail address.</p>\n";
                $email = "";
        }
        
    }
        // // validate the password entry
        if( empty($_POST['password']) )  {
            +$errors;
            echo "<p>You need to enter a valid password.</p>\n";
            $password = "";
        } else 
            $password = stripslashes($_POST['passwod']);
        if( empty($_POST['password2']) ) {
            ++$errors;
            echo "<p>You need to re-enter you a password to confirm</p>\n";
            $password2 = "";
        } else 
            $password2 = stripslashes($_POST['password2']);
        if((!(empty($password))) && (!(empty($password2)))) {       // checking that the passwords match and are correct length
            if(strlen($password) < 6) {
                ++$errors; 
                echo "<p>The password is too short.</p>\n";
                $password = "";
                $password2 = "";
            }
            if($password <> $password2) {
                ++$errors;
                echo "<p>The passwords do not match</p>\n";
                $password = "";
                $password2 = "";
            }
        }

            //connect to the database to provide information     
       // $DBConnect = FALSE;
        if($errors == 0) {
            $DBConnect = @mysqli_connect("localhost","root","root");
            if($DBConnect === FALSE) {
                echo "<p>Unable to connect to the database server " . " Error Code " . mysqli_errno() . ":" . mysqli_error() .  "</p>\n";
                ++$errors;
            } else {
                $DBName = "internships";
                $result = @mysqli_select_db($DBName,$DBConnect);
                if($result === FALSE) {
                    echo "<p>Uable to select the database." . "Error code " . mysqli_errno($DBConnect) . ":" . mysqli_error($DBConnect) . "</p>\n";
                    ++$errors;
                }
            }
        }

        $TableName = "interns";
        if($errors == 0) {
            $SQLString = "SELECT count(*) FROM $TableName " . " where email='$email'";
            $QueryResult = @mysqli_query($SQLString,$DBConnect);
            if($QueryResult !== FALSE) {
                $Row = mysqli_fetch_row($QueryResult);
                if($Row[0]>0) {
                    echo "<p>The email address entered(" . htmlentities($email) . ") is already registered.</p>\n";
                    ++$errors;
                }
            }
        }

        if($errors > 0) {       //message indicating that a error has occured
            echo "<p>Please use your browser's BACK button to return " . "to the form and fix the errors indicated.</p>\n";
        }
        
        // add the information to the database 
        if ($errors == 0) {
            $first = stripslashes($_POST['first']);
            $last = stripslashes($_POST['last']);
            $SQLstring = "INSERT INTO $TableName " . " (first, last, email, password_md5) " . " VALUES( '$first', '$last', '$email', " .
                      " '" . md5($password) . "')";
            $QueryResult = @mysqli_query($SQLstring, $DBConnect);
            if ($QueryResult === FALSE) {
                 echo "<p>Unable to save your registration " .
                      " information. Error code " .
                      mysqli_errno($DBConnect) . ": " . 
                      mysqli_error($DBConnect) . "</p>\n";
                 ++$errors;
            }
            else {
                 $InternID = mysqli_insert_id($DBConnect);
            }
            mysqli_close($DBConnect);
       }
       if ($errors == 0) {
            $InternName = $first . " " . $last;
            echo "<p>Thank you, $InternName. ";
            echo "Your new Intern ID is <strong>" .
                 $InternID . "</strong>.</p>\n";
       }
      
        
    ?>
</body>
</html>