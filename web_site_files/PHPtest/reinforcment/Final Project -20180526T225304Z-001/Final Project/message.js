(function () {
  var message = $('message');
  var messageCounter = $('message-counter');

  message.on('focus', updateCounter);
  message.on('keyup', updateCounter);

  // when we leave the textarea, we hide the counter unless there are too
  // many characters
  message.on('blur', function () {
    if (messageCounter.text() >= 0) {
      messageCounter.addClass('hide');
    }
  });


  function updateCounter(e) {
    var count = 100 - message.val().length;
    var status = '';
    if (count < 0) {
      status = 'error';
    } else if (count <= 15) {
      status = 'warn';
    } else {
      status = 'good';
    }

    // remove previous classes
    messageCounter.removeClass('error warn good hide');
    // add new class
    messageCounter.addClass(status);
    messageCounter.text(count);
  }

}());