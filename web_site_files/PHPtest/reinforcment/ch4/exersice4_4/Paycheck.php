<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Calculate Paycheck</title>
</head>
<body>
        <?php
          
            function displayError($fieldName) {
                echo "Error!\"$fieldName\"is required to calculate your pay<br />\n";

            }
            function validateInput($data,$fieldName) {
                global $errorCount;
                if( empty($data) ) {
                    displayError($fieldName);
                    ++$errorCount; 
                    $retval = "";
                } else 
                return($data);
                
            } 
            $errorCount =0;

           

            //redisplay of the form to enter correct information  
        ?>
        <h1> Your Pay Calculated</h1>
        <form action = "Paycheck.php" method ="post">

            Hours worked: <input type = "text" name = "HoursWorked" /><br /><br/>
            Hourly Wage: <input type = "text"  name = "HourlyWage" /> <br /><br/>

            <input type ="reset" value = "Clear Form"/>
                <input type = "submit" name="Submit" value = "Send Form"/>
        </form>
    <?php
      
     if($errorCount>0) {
        echo "Please enter the correct information.<br />\n";
        redisplayForm($hoursWorked,$wages);
    }

    $hoursWorked = $_POST['HoursWorked'];       // learned somthing, need to have post not get. 
    $wages = $_POST['HourlyWage'];

    if ( is_numeric($hoursWorked) && is_numeric($wages) ) {
        if ($hoursWorked <= 40) {      // if equal to 40 hrs 
       $payCheck = ($hoursWorked * $wages);
           echo "<h1>Your Wages Earned</h1><p>You worked <strong>$hoursWorked</strong> hours at a rate of <strong>$$wages</strong> an hour.
           </p> <h3>Your pay total is:  <strong>$$payCheck</strong></h3><br />
           <p><a href='Paycheck.html'> Calculate another pay</a></p>";
   }
      if ($hoursWorked > 40) {          // if greater than 40 hrs, time and half
       $payCheck = ($hoursWorked * $wages) + (($hoursWorked - 40) * $wages * 1.5);
           echo "<h1>Your Wages Earned</h1><p>You worked <strong>$hoursWorked</strong> hours at a rate of <strong>$$wages</strong> an hour.
          </p> <h3>Your pay total is:  <strong>$$payCheck</strong></h3><br />
           <p><a href='Paycheck.html'> Calculate another pay</a></p>";
       }
       
   } else{              // if vaules are invalided prompt user. 
      echo  " <p>Only standard numbers.</p>
      <p><a href='Paycheck.html'> Click here to re-enter the information</a></p>";
     
  }
 
    ?>

</body>
</html>