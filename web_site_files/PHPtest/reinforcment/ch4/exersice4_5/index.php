<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Guest Book</title>
</head>
<body>
    <?php

        // need to validate your inputs first and formost ,agnostic function
    function validateInput($data,$feildname) {
        global $errorCount;

        if( strlen($data)<=0 ) {
            echo "\"$feildname\" is a required field.<br />\n";
            ++$errorCount;
            $retval= "";
        }
        else {
            $retval = trim($data);
            $retval = stripslashes($retval);

            if( is_numeric($retval) ) {
                if($retval <=0) {
                    echo "\"$feildname\"Is not a valid number.<br />\n";
                    ++$errorCount;
                    $retval= "";
                 }

            }
            else {
                // process data 
                echo "\"$feildname\"Must be greater than zero.<br />\n";
                    ++$errorCount;
                    $retval= "";
            }
        } 
        return $retval;
    }
    

    // sticky form xhtml to present data entered
    // you could put "Train A Speed" in a variable
    function displayForm($SpeedA,$SpeedB,$Distance) {
        ?>

        <h2 style = "text-align:center">Train Distance Form</h2>
        <form name = "Train problem" action ="index.php" method = "post";>   
            <p>Train A speed: <input type = "text" name = "SpeedA" value ="<?php echo $SpeedA; ?>" /></p>
            <p>Train B speed: <input type = "text" name = "SpeedB" value ="<?php echo $SpeedB; ?>" /></p>
            <p>Train Distance Traveled: <input type = "text" name = "Distance" value ="<?php echo $Distance; ?>" /></p>
            <p><input type="reset" value="Clear Form" />&nbsp; &nbsp;<input type="submit" name="Submit" value="Send Form" /></p>
       </form>
       <?php
        }

        $errorCount = 0;
        $ShowForm = TRUE;
        $SpeedA = "";
        $SpeedB = "";
        $Distance = "";
// intalize form entry   data
if ( isset($_POST['Submit']) ) {
    $SpeedA = validateInput($_POST['SpeedA'],"Speed of Train A.");
    $SpeedB = validateInput($_POST['SpeedB'],"Speed of Train B.");
    $Distance = validateInput($_POST['Distance'],"Distance between Trains.");
    
    if($errorCount == 0) {
        $ShowForm = FALSE; 
    }
    else {
        $ShowForm = TRUE;
    }

} // working to this point 

if($ShowForm == TRUE) {          // took away the absolute equals 
    if($errorCount > 0) {
        echo "<p>Please reenter your values.</p>";
    }
    
    displayForm($SpeedA,$SpeedB,$Distance);
}

else {

         // following equations needed to solve the problem
            $DistanceA = (($SpeedA / $SpeedB) * $Distance) /
                (1 +($SpeedA/ $SpeedB));

           $DistanceB = $Distance - $DistanceA;
            $TimeA = $DistanceA / $SpeedA;
            $TimeB = $DistanceB / $SpeedB;


    echo "Train A Travled $DistanceA miles in $TimeA hours at $SpeedA mph <br />\n";
    echo "Train B Travled $DistanceA miles in $TimeB hours at $SpeedB mph <br />\n";
    echo "The sume of the two distances is " .($DistanceA + $DistanceB). " miles<br />\n";
    
    
}

    ?>
</body>
</html>