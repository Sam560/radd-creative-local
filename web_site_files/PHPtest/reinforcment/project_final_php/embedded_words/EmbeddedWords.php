<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Embedded Words</title>

</head>
<body>
    <h1>Embedded Words</h1>
    <?php
        // creating a script that validates the zodiac names made using letters of each phrase. 

        // create arrays for phrases and signs 
        $Phrases = array("Your chinses zodiac sign tells a lot about your personality.", "Embed PHP scripts within an XHTML document.");
        $SignNames = array("Rat","Ox","Tiger","Rabbit","Dragon","Snake","Horse","Goat","Monkey","Rooster","Dog","Pig");

        // function to check values and create new array
            function BuildLetterCounts($text){
                $text = strtoupper($text); // covert to upper case
                $letter_counts = count_chars($text);   // count the number of ASCII char
                return $letter_counts; // retun new array 
            }  

            // function to process new array
                function AContainsB($A, $B){
                    $retval = TRUE;     //set default value to true 
                    $first_letter_index = ord('A');
                    $last_letter_index = ord ('Z'); // ord is used to get ASCII char values of first and last letters

                for ($letter_index = $first_letter_index; $letter_index<=$last_letter_index; ++$letter_index){  // loop to check the counts for each uppercase letter 
                    if($A[$letter_index] < $B[$letter_index]){

                    }
                }
                return $retval; 
            }

            //loop to step through each of the phrases
            foreach($Phrases as $Phrase) {
                $PhraseArray = BuildLetterCounts($Phrase);
                $GoodWords = array();   // char that match 
                $BadWords = array();    // char that don't match

            // loop to step through each of the sign names
                foreach($SignNames as $Word) {
                    $WordArray = BuildLetterCounts($Word);
                    if( AContainsB($PhraseArray, $WordArray) ) 
                        $GoodWords[] = $Word;
                    else
                        $BadWords[] = $Word;
                }
                // display the array and words that can and cannot be made from the phrase. 
                    echo "<p>The following words can be made from the letters in the phrase &quot; $Phrase&quot;:";
                    foreach($GoodWords as $Word)
                        echo "$Word";
                        echo "</p>\n";
                        
                    echo "<p> The following words can not be made from the letters in the phrase &quot;$Phrase&quot;:";
                    foreach($BadWords as $Word)
                        echo "$Word";
                    echo "</p>\n";
                    echo "<hr />\n";
            }
    ?>
</body>
</html>