<!DOCTYPE HTML PUBLIC"-/W3C/DTD XHTML 1.0 Strict// EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <title>BirthDay Switch</title>
</head>
<body>
    <?php
        // required
    // form to handle user entering date of birth 
    // "You were born in the sign of the[image]."create display using saved images
    //set a counter for the number of times a year sh ows up, and store that data in a "statistics " subdirectory
    //make sure to set the write and read permissions for statitics directory
    // prompt user with how many times a year was entered "You are visitor[count] to enter[year]"


    // error handling
   //check to see if any value has been entered
   function displayerror($fieldname,$data){
    global $errorCount; 
    if( empty($fieldname) ) {
        echo "<p>The feild $fieldname is required.</p>\n";
        ++$errorCount; 
        $retval ="";
    }else{ // process clean data
        $data = trim($data);
        $data = stripslashes($data);
       $pattern = "[0-9]{4}";
       if(preg_match($data,$pattern)){
           $retval = $data; 
        } else{ // if error has occured with pattern, prompt user
            echo"<p> You need to make sure to enter a vaild birth year</p>\n";
            ++$errorCount; 
            $retval; 
        }
    }            
    return($retval);
}
 // check that the value entered is valid 
 date_default_timezone_set('America/Denver');
 $currentyear = date(Y);
 $AnimalSigns = array(
     "rat"=> array("Start Date"=> "1900","End Date"=> "2020"),
     "ox"=> array("Start Date"=> "1901","End Date"=> "2021"),
    "tiger"=> array("Start Date"=> "1902","End Date"=> "2022"),
    "rabbit"=> array("Start Date"=> "1900","End Date"=> "2020"),
    "dragon"=> array("Start Date"=> "1904","End Date"=> "2024"),
    "snake"=> array("Start Date"=> "1905","End Date"=> "2025"),
    "horse"=> array("Start Date"=> "1906","End Date"=> "2026"),
    "goat"=> array("Start Date"=> "1907","End Date"=> "2027"),
    "monkey"=> array("Start Date"=> "1908","End Date"=> "2028"),
    "rooster"=> array("Start Date"=> "1908","End Date"=> "2028"),
    "dog"=> array("Start Date"=> "1910","End Date"=> "2030"),
    "pig"=> array("Start Date"=> "1911","End Date"=> "2031"),
);


 if (isset($_POST['birthyear'])) {
    $year = $_POST['birthyear'];
    $sign = findsign($year);
    $count = getstats($year);
            echo "You were born under the sign of the " . $sign;
            echo "<br/ >";
            echo "<img src='images/" . $sign .".png'>";
            echo "<br/ >";
            echo "You are visitor ".$count." to enter ".$year;
 } 
    ?>
    <form method="POST" >
            <p>What year were you born? <input type="number" name="birthyear" min="<?php echo $currentyear-100 ?>" max="<?php echo $currentyear ?>" required autofocus></p>
            <input type="submit" name="submit">
    </form>

    <?php
    function findsign($zyear) {
        // create zodiac signs array
        $signs = array( "monkey", "rooster", "dog", "pig", "rat", "ox", "tiger", "rabbit", "dragon", "snake", "horse", "sheep");
        $i = $zyear % 12;
        switch ($i) {
          case 0:
            $sign = $signs[0];
            break;
          case 1:
            $sign = $signs[1];
            break;
          case 2:
            $sign = $signs[2];
            break;
          case 3:
            $sign = $signs[3];
            break;
          case 4:
            $sign = $signs[4];
            break;
          case 5:
            $sign = $signs[5];
            break;
          case 6:
            $sign = $signs[6];
            break;
          case 7:
            $sign = $signs[7];
            break;
          case 8:
            $sign = $signs[8];
            break;
          case 9:
            $sign = $signs[9];
            break;
          case 10:
            $sign = $signs[10];
            break;
          case 11:
            $sign = $signs[11];
            break;
        }
        return $sign;
      }
      function getstats($syear) {
        $name = "statistics/$syear.txt";
        if (file_exists($name)) {
          $file = fopen($name, "r");
          $counter = fgets($file);
          fclose($file);
        } else {
          $file = fopen($name, "w");
          $counter = 0;
          fclose($file);
        }
        // add 1 to counter
        $counter += 1;
        file_put_contents($name, $counter);
        return $counter;
      }
    
    

      echo "works here";
    ?>
    
</body>

</html>