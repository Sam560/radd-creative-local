

<?php
    // array containing animals images 
    $Animals = array("rat", "ox", "tiger", "rabbit", "dragon", "snake", "horse", "sheep", "monkey", "rooster", "dog", "pig");

    //set timezone to the current year
    date_default_timezone_set('America/Denver');

    //intialize variable to count columns 
    $col = 1; 

    echo '<table>';
    echo '<tr>';
    //header row for images
    for($Animals = 0; $Animals < count($animal); ++$Animals ){
        echo '<td><img src ="img/'. $Animals[$animal]. '.jpg"></td>';
    }
    echo '</tr>';
    echo '<tr>';
    for ($year = 1912; $year <= $date("Y"); ++$year){
        echo '<td>'. $year . '</td>';
        if($col % 12 === 0) {
            echo '</tr>';
            echo '<tr>';
        }
        $col++;
    }
    echo '</tr>';
    echo '</table>';
?>