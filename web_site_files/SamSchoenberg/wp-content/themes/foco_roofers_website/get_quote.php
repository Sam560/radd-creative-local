<?php

if ($_POST["submit"]) {

      
      
     if (!$_POST['name']) {

       $error="<br />Please enter your name";

     }
      
     if (!$_POST['email']) {

       $error.="<br />Please enter your email address";

     }
     
      
      if (!$_POST['phone']) {
     
      $error.="<br /> Please enter your phone number";
     
     }

       if (!$_POST['city-name']) {
     
      $error.="<br /> Please select your city";
     
     }
      
     if ($_POST['email']!="" AND !filter_var($_POST['email'],
FILTER_VALIDATE_EMAIL)) {
      
     $error.="<br />Please enter a valid email address";

     }
     
      
     if ($error) {

 $result='<div class="alert alert-danger"><strong>There were error(s)
in your form:</strong>'.$error.'</div>';

     } else {

       /* THE EMAIL WHERE YOU WANT TO RECIEVE THE CONTACT MESSAGES */
  if (mail("schoenbergwebtools.com", "Message from Deck Installation", 


"Name: ".$_POST['name']."
Email: ".$_POST['email']."
Phone: ".$_POST['phone']." 
City: ".$_POST['city-name'])) {
$result='<div class="alert alert-success"> <strong> Thank
you!</strong> We\'ll get back to you shortly.</div>';
} else {
$result='<div class="alert alert-danger">Sorry, there was
an error sending your message. Please try again later.</div>';
}
}
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content=""/> 
    <!-- Meta Description -->

  
  <title>Foco Roofers</title> <!-- Your Page Title -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


     <!--CSS STYLESHEETS<-->
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />


    <!--GOOGLE FONT  STYLE<-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">

    <!--end-fonts-->
  
    <link rel="icon" href="images/deck-installation-favicon.jpg">
    <!--Your Website Favicon-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127473022-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127473022-1');
        </script>
   
  </head>

  <body>
  <div class="container-fluid" id="banner">
  
        <a href="https://yourwebsitename.com"> <img src="images/hammer_wrench-step-0.jpg" align="center" class="img-responsive logo center-block" alt="deck-installers-logo"> </a>
        <!-- YOUR WEBSITE LOGO-->
        

        <h2 align="center" class="tagline center-block">Roofer Pricing Options</h2>

    
  
  <!-- Begining of Nav bar -->
  <nav class="navbar navbar-default navbar-static-top" role="navigation" id="topnavbar">
  
      
  
           <div class="navbar-header">
      
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      
                      <span class="sr-only">Toggle navigation </span>
          
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
          
                 </button>
      
          </div>

    
    <div class="collapse navbar-collapse">
      
            <ul align="center" class="nav navbar-nav" >
                    <li> <a href="index.php"><span class="center-underline">HOME</span></a></li>
                    
                    <li> <a href="hail_dmg.php"><span class="center-underline">HAIL DAMAGE REPIAR</span></a></li>

                    <li> <a href="material_pric_des.php"><span class="center-underline">MATERIAL AND PRICING </span></a></li>

                    <li> <a href="get_quote.php"><span class="center-underline">GET QUOTE</span></a></li>
                
        
                </ul>

    </div><!--end navbar-collapse-->

</nav><!--end nav-->



   <!--GET A QUOTE FORM-->
   <div class="get-quote-form">

<div class="row">

  <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

    <form method="post">
      <p align="center">SEE HOW MUCH<br>
      <span style="font-weight: bold; font-size: 50px; position:relative; top:5px; line-height: 50px;">You Can Save</span><br>
      <br>

      <div class="form-group center-block">
        <div class="icon-addon addon-lg">
          <input class="form-control" name="name" placeholder="Name" type="text" value="<?php echo $_POST['name']; ?>"> 
          <label class="glyphicon glyphicon glyphicon-user" for="name" title="name"></label>
        </div>
      </div>

      <div class="form-group center-block">
        <div class="icon-addon addon-lg">
          <input class="form-control" name="email" placeholder="Email" type="email" value="<?php echo $_POST['email']; ?>"> 
          <label class="glyphicon glyphicon-envelope" for="email" rel="tooltip" title="email"></label>
        </div>
      </div>

      <div class="form-group center-block">
        <div class="icon-addon addon-lg">
          <input class="form-control" name="phone" placeholder="Phone" type="tel" value="<?php echo $_POST['phone']; ?>"> 
          <label class="glyphicon glyphicon-earphone" for="email" rel="tooltip" title="email"></label>
        </div>
      </div>
      

      <p align="center"> <input type="submit" name="submit" class="btn btn-default btn-lg quote-submit center-block" value="GET A QUOTE" ></input/> </p>

      </form>

    </div>

</div><!--end row-->

</div><!--end get-quote-form-->




<br /> <br /> <br />

<div class="social-links" align="center">

<p>CONNECT WITH US</p>

<a href="https://www.instagram.com/YOUR-INSTGRAM-ACCOUNT" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24">
    <path fill="#774220" d="M7.8,2H16.2C19.4,2 22,4.6 22,7.8V16.2A5.8,5.8 0 0,1 16.2,22H7.8C4.6,22 2,19.4 2,16.2V7.8A5.8,5.8 0 0,1 7.8,2M7.6,4A3.6,3.6 0 0,0 4,7.6V16.4C4,18.39 5.61,20 7.6,20H16.4A3.6,3.6 0 0,0 20,16.4V7.6C20,5.61 18.39,4 16.4,4H7.6M17.25,5.5A1.25,1.25 0 0,1 18.5,6.75A1.25,1.25 0 0,1 17.25,8A1.25,1.25 0 0,1 16,6.75A1.25,1.25 0 0,1 17.25,5.5M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9Z" />
</svg></a>

<a href="https://www.facebook.com/" target="_blank"><svg style="width:30px;height:30px" viewBox="0 0 24 24"><path fill="#774220" d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
</svg></a>
 
 <br /> <br />  

</div> <br /> 




<footer>

    <div align="center" id="footer">

      <a href="https://yourwebsitename.com/composite-decking.php">HAIL DAMAGE REPAIR</a> 

      <a href="https://yourwebsitename.com/hardwood-decking.php">MATERIAL AND PRICING</a>

      <a href="https://yourwebsitename.com/get-a-quote.php">GET A QUOTE</a>

    </div><!--end of footer-->


    <div align="center" class="footerlinks">

      <a href="https://yourwebsitename.com/about.html">About</a> 

      <a href="https://yourwebsitename.com/privacy-policy.html" rel="nofollow">Privacy Policy</a> 

      <a href="https://yourwebsitename.com/terms-and-conditions.html" rel="nofollow">Terms &amp; Conditions</a>

      <a href="https://yourwebsitename.com/contact.php" rel="nofollow">Contact</a>

     
      <p align="center" class="text-muted copyright">&copy; schoenbergwebtools.com</p>

    </div><!--end footerlinks-->

  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
  </script> 
  <!-- Include all compiled plugins (below), or include individual files as needed -->

   
   <!--FIXED NAVBAR SCRIPT-->
  <script src="js/bootstrap.min.js">
  </script> 
  <script>
  $('#topnavbar').affix({
     offset: {
         top: $('#banner').height()
     }   
  }); 
  </script>

 

  </body>
  </html>