    // array containig menu titles 
var menu = {
    "menu":[
        {"name": "Pending-Applications", "link":"#", "submenu": [
            {"name": "In-Progress", "link" :"incl_php/pending_apps/inc_inprog.php", "submenu": null},
            {"name": "Completed", "link":"#", "submenu": null},
            {"name": "Waitlisted", "link":"#", "submenu": null},
            {"name": "Held", "link":"#", "submenu": null},
            {"name": "Declined", "link":"#", "submenu": null}
        ]},
        {"name": "Roster by Clusters", "link":"#", "submenu": [
            {"name": "All Clusters", "link":"#", "submenu": null},
            {"name": "General", "link":"#", "submenu": null},
            {"name": "Returners", "link":"#", "submenu": null},
            {"name": "Science Outreach", "link":"#", "submenu": null},
            {"name": "Sustainability", "link":"#", "submenu": null},
            {"name": "Science Outreach", "link":"#", "submenu": null},
        ]},       
        {"name": "Tools", "link":"#", "submenu": [
            {"name": "Applications", "link":"#", "submenu": null},
            {"name": "Message Defaults", "link":"#", "submenu": null},
            {"name": "Data Utilites", "link":"#", "submenu": null},
            {"name": "Query Builder", "link":"#", "submenu": null},
            {"name": "Statistics", "link":"#", "submenu": null}
        ]},
        ]
    };

    function parseMenu(element, menu) {
        for (var i=0; i<menu.length; i++) {
            var nestedli = document.createElement('li');
            nestedli.setAttribute('style', 'display:none;');
            nestedli.setAttribute('onmouseenter', "showNodes(this)");
            nestedli.setAttribute('onmouseleave', "hideNodes(this)");
      var link = document.createElement('a');
      link.setAttribute('href', menu[i].link);
      link.appendChild(document.createTextNode(menu[i].name));
            nestedli.appendChild(link);
            if (menu[i].submenu!=null) {
                var subul = document.createElement('ul');
                nestedli.appendChild(subul);
                parseMenu(subul, menu[i].submenu);
            }
            element.appendChild(nestedli);
    
        }
    }