<cfmodule template="#application.urlpath_security#" reqApplicant="true">

<!--- If user should not have access to this page --->
<cfif session.app_status NEQ "Completed">
	<!--- Log action --->
	<cfmodule template="#application.urlpath_log#" action="Completed Page: User who should not have been on this page (their application was not completed) went to this page." impacted_user="#session.username#">

	<!--- Kick user out --->
	<cflocation url="#application.urlpath#?status=noAuth" addtoken="false">

</cfif>

<!--- End user's session --->
<cfset structClear(session)>


<cfoutput>
<h2>Application Submitted</h2>

<p style="margin-top: 10px;">Thanks for your interest in the College of Natural Sciences Learning Community. We will email you within the next few weeks with more information about your application status and any next steps we need from you (interviews, additional questions, etc.).</p>

<p>Please note that this application does not replace your CSU Housing Application which is required for all students who live on campus. It can be found here: <a href="https://housing.colostate.edu/halls/apply/" style="text-decoration: underline;">https://housing.colostate.edu/halls/apply/</a>.</p>

<p><span style="font-weight: bold;">Note:</span> You can always <a href="#application.urlpath#" style="text-decoration: underline;">return to your application</a> to check the status and to review the answers you provided.</p>

</cfoutput>

<cfinclude template="#application.urlpath#mods/questions.cfm">