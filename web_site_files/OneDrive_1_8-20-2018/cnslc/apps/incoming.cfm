<cfmodule template="#application.urlpath_security#">

<cfquery name="getMajors">
    SELECT major_full
    FROM ref_majors
    ORDER BY major_full ASC
</cfquery>

<cfoutput>

<form method="post" action="" name="appForm" id="appForm" class="firstCol200strict firstColRight">

<!--- BASIC INFO --->
<div class="formRowGroup">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-5">
            <label for="first_name">First Name</label>
            <input name="first_name" id="first_name" class="form-control form-control-sm" type="text" size="25" value="#htmlEditFormat(getInfo.first_name)#" required>
        </div>
        <div class="col-1 hidden-xs"></div>
        <div class="col-5">
            <label for="last_name">Last Name</label>
            <input name="last_name" id="last_name" class="form-control form-control-sm" type="text" size="25" value="#htmlEditFormat(getInfo.last_name)#" required>
        </div>
        <div class="col-1 hidden-xs"></div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-7">
            <label for="email">Preferred Email</label>
            <input name="email" id="email" type="email" class="form-control form-control-sm" size="50" value="#getInfo.email#">
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col">
            <label for="phone1">Contact Phone Number<span class="optNote">(Optional)</span></label>
            <div class="form-inline phoneInputs">
                <input name="phone" id="phone1" class="form-control form-control-sm autoNext phone phone3 opt" type="text" maxlength="3" value="#htmlEditFormat(left(getInfo.phone, 3))#" placeholder="xxx">
                <input name="phone" id="phone2" class="form-control form-control-sm autoNext phone phone3 opt" type="text" maxlength="3" value="#htmlEditFormat(mid(getInfo.phone, 4, 3))#" placeholder="xxx">
                <input name="phone" id="phone3" class="form-control form-control-sm phone phone4 opt" type="text" maxlength="4" value="#htmlEditFormat(right(getInfo.phone, 4))#" placeholder="xxxx">
                
            </div>
        </div>
    </div>
    <div class="row" style="">
        <div class="col-5">
            <label for="said_major">Currently Declared Major</label>
            <select name="said_major" id="said_major" class="form-control form-control-sm">
                <option value="" disabled selected>Select Major</option>
                <cfloop query="getMajors">
                    <option value="#getMajors.major_full#"<cfif getInfo.said_major EQ getMajors.major_full> selected</cfif>>#getMajors.major_full#</option>
                </cfloop>
            </select>
        </div>
    </div>
</div>
<!--- END BASIC INFO --->

<!--- BEGIN QUESTIONS --->
<div class="formRowGroup">
    <div class="row">
        <div class="col">
            <label for="why_major">What specifically in your past led you to choose your major? How do you envision your major impacting your future goals?</label>
            <textarea name="why_major" id="why_major" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.why_major)#</textarea>
        </div>
    </div>
</div>
<div class="formRowGroup">
    <div class="row">
        <div class="col">
            <label for="shaped_you">What is something someone may not know by looking at you that has helped shape who you are?</label>
            <textarea name="shaped_you" id="shaped_you" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.shaped_you)#</textarea>
        </div>
    </div>
</div>
<div class="formRowGroup">
    <div class="row">
        <div class="col">
            <label for="meaningful_exp">Think about your life so far. Describe one meaningful experience that has influenced the way you see the world.</label>
            <textarea name="meaningful_exp" id="meaningful_exp" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.meaningful_exp)#</textarea>
        </div>
    </div>
</div>
<div class="formRowGroup">
    <div class="row">
        <div class="col">
            <label for="something_unique">Describe something unique about yourself in 50 words or more.</label>
            <textarea name="something_unique" id="something_unique" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.something_unique)#</textarea>
        </div>
    </div>
</div>
<div class="formRowGroup lastFormRowGroup">
    <div class="row">
        <div class="col" style="vertical-align: top;">
            <label for="interests">The following additional opportunities will be available to CNSLC members. Please indicate which of the following you are interested in learning more about.</label>
            <fieldset id="interests" style="margin-top: 10px;">
                <legend style="width: auto; margin-bottom: 0; font-size: 1em;">Additional Opportunities</legend>
                <div class="formCol">

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-1">
                            <input type="checkbox" name="int_sos" id="int_sos" value="1" data-extradiv="int_sos_extraDiv" style="margin-top: 5px; float: right;"<cfif getInfo.int_sos EQ 1> checked</cfif>>
                        </div>
                        <div class="col-11">
                            <label for="int_sos">
                                <p style="font-weight: bold; margin: 0; padding: 0;">Living on a floor focused on outreach to K-12 students called the Science Outreach Scholars</p>
                                <p style="padding-top: 2px; padding-bottom: 0;">Are you interested in giving back to local kids from underrepresented backgrounds and culturally diverse communities? Do you want to learn how to make a difference in the world through science and technology? This floor is an opportunity to live alongside other students with the same interests, and support each other in engaging in K-12 science outreach.</p>
                            </label>

                            <div class="row">
                                <div id="int_sos_extraDiv" class="col" style="margin-top: 10px;">
                                    <label for="int_sos_extra" style="margin-left: 10px;">Describe how a college student mentor could have impacted you when you were in elementary school. What would you have liked for them to have shared with you?</label>
                                    <textarea name="int_sos_extra" id="int_sos_extra" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more..." style="margin-left: 12px;">#htmlEditFormat(getInfo.int_sos_extra)#</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="margin: 20px; border-bottom: 1px solid ##ededed;"></div>

                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-1">
                            <input type="checkbox" name="int_sustainability" id="int_sustainability" value="1" data-extradiv="int_sustainability_extraDiv" style="margin-top: 5px; float: right;"<cfif getInfo.int_sustainability EQ 1> checked</cfif>>
                        </div>
                        <div class="col-11">
                            <label for="int_sustainability">
                                <p style="font-weight: bold; margin: 0; padding: 0;">Living on a Sustainability floor</p>
                                <p style="padding-top: 2px; padding-bottom: 0;">Are you interested in exploring how the fundamental sciences connect to sustainability? Would you like to get to compost, garden, and explore issues of sustainability in your first year at CSU?</p>
                            </label>
                            
                            <div class="row">
                                <div id="int_sustainability_extraDiv" class="col" style="margin-top: 10px;">
                                    <label for="int_sustainability_extra" style="margin-left: 10px;">What has been your experience with sustainability in your family/culture/community and how does it impact your desire to live on the floor?</label>
                                    <textarea name="int_sustainability_extra" id="int_sustainability_extra" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more..." style="margin-left: 12px;">#htmlEditFormat(getInfo.int_sustainability_extra)#</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <cfif getInfo.pi_gender EQ "F">
                    <div style="margin: 20px; border-bottom: 1px solid ##ededed;"></div>

                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-1">
                            <input type="checkbox" name="int_wins" id="int_wins" value="1" data-extradiv="int_wins_extraDiv" style="margin-top: 5px; float: right;"<cfif getInfo.int_wins EQ 1> checked</cfif>>
                        </div>
                        <div class="col-11">
                            <label for="int_wins">
                                <p style="font-weight: bold; margin: 0; padding: 0;">Living on a Women in Science floor</p>
                            </label>

                            <div class="row">
                                <div id="int_wins_extraDiv" class="col" style="margin-top: 10px;">
                                    <label for="int_wins_extra" style="margin-left: 10px;">Why would it be important to have a Women in Sciences floor?</label>
                                    <textarea name="int_wins_extra" id="int_wins_extra" class="form-control form-control-sm"  placeholder="Please answer the question using around 50 words or more..." style="margin-left: 12px;">#htmlEditFormat(getInfo.int_wins_extra)#</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    </cfif>
                </div>
            </fieldset>
        </div>

    </div>
</div>

</form>

</cfoutput>