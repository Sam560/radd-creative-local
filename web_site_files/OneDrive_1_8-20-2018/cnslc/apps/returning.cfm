<cfmodule template="#application.urlpath_security#">

<cfquery name="getHalls">
	SELECT hall_name
	FROM ref_res_halls
	ORDER BY hall_name ASC
</cfquery>

<cfoutput>

<form method="post" action="" name="appForm" id="appForm" class="firstCol200strict firstColRight">

<!--- BASIC INFO --->
<div class="formRowGroup">
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-5">
            <label for="first_name">First Name</label>
            <input name="first_name" id="first_name" class="form-control form-control-sm" type="text" size="25" value="#htmlEditFormat(getInfo.first_name)#" required>
        </div>
        <div class="col-1 hidden-xs"></div>
        <div class="col-5">
            <label for="last_name">Last Name</label>
            <input name="last_name" id="last_name" class="form-control form-control-sm" type="text" size="25" value="#htmlEditFormat(getInfo.last_name)#" required>
        </div>
        <div class="col-1 hidden-xs"></div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-7">
            <label for="email">Preferred Email</label>
            <input name="email" id="email" type="email" class="form-control form-control-sm" size="50" value="#getInfo.email#">
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col">
            <label for="phone1">Contact Phone Number<span class="optNote">(Optional)</span></label>
            <div class="form-inline phoneInputs">
                <input name="phone" id="phone1" class="form-control form-control-sm autoNext phone phone3 opt" type="text" maxlength="3" value="#htmlEditFormat(left(getInfo.phone, 3))#" placeholder="xxx">
                <input name="phone" id="phone2" class="form-control form-control-sm autoNext phone phone3 opt" type="text" maxlength="3" value="#htmlEditFormat(mid(getInfo.phone, 4, 3))#" placeholder="xxx">
                <input name="phone" id="phone3" class="form-control form-control-sm phone phone4 opt" type="text" maxlength="4" value="#htmlEditFormat(right(getInfo.phone, 4))#" placeholder="xxxx">
                
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-5">
			<label for="res_hall">Current Residence Hall:</label>
			<select name="res_hall" id="res_hall" class="form-control form-control-sm">
          	 	<option value="" disabled selected>Select Residence Hall</option>
                <cfloop query="getHalls">
                    <option value="#getHalls.hall_name#"<cfif hall_name EQ getInfo.res_hall> selected</cfif>>#getHalls.hall_name#</option>
                </cfloop>
                <option value="offCampus">Off-Campus</option>
            </select>
		</div>
	</div>
</div>
<!--- END BASIC INFO --->

<!--- BEGIN QUESTIONS --->
<div class="formRowGroup">
	<div class="row">
		<div class="col">
			<label for="why_cnslc">Briefly explain why you are interested in joining the CNSLC.</label>
			<textarea name="why_cnslc" id="why_cnslc" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.why_cnslc)#</textarea>
		</div>
	</div>
</div>
<div class="formRowGroup">
	<div class="row">
		<div class="col">
			<label for="academic_exp">Describe <span style="font-weight: bold;">one academic</span> experience that you had during your freshman year in college that you felt was particularly influential.</label>
			<textarea name="academic_exp" id="academic_exp" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.academic_exp)#</textarea>
		</div>
	</div>
</div>
<div class="formRowGroup">
	<div class="row">
		<div class="col">
			<label for="one_moment">Describe <span style="font-weight: bold;">one moment</span> during your time at Colorado State University where you were assured that being a natural science major was something for you.</label>
			<textarea name="one_moment" id="one_moment" class="form-control form-control-sm" placeholder="Please answer the question using around 50 words or more...">#htmlEditFormat(getInfo.one_moment)#</textarea>
		</div>
	</div>
</div>
<div class="formRowGroup">
	<div class="row">
		<div class="col">
			<label for="reference">Please provide the name of a reference who knows a little bit about you as a CSU student. References can be RAs, RDs, club advisors, learning community coordinators, or faculty.</label>
			<fieldset id="reference" style="margin: 5px 0 0 0; padding: 0 10px 10px 10px;">
        		<legend style="width: auto; font-size: 1em; margin-bottom: 0;">Reference Information</legend>

        		<div class="row">
					<div class="col-5">
						<label for="ref_fName">First Name:</label>
						<input name="ref_fName" id="ref_fName" class="form-control form-control-sm" type="text" value="#htmlEditFormat(getInfo.ref_fName)#" placeholder="Reference's first name">
					</div>

					<div col="col-1"></div>

					<div class="col-5">
						<label for="ref_lName">Last Name:</label>
						<input name="ref_lName" id="ref_lName" class="form-control form-control-sm" type="text" value="#htmlEditFormat(getInfo.ref_lName)#" placeholder="Reference's last name">
					</div>
        		</div>
        		<!---
        		<div class="row">
					<div class="col">
						<label for="ref_email">Contact Email:</label>
						<input name="ref_email" id="ref_email" size="50" value="#htmlEditFormat(getInfo.ref_email)#">
					</div>
        		</div>
	        	--->
        		<div class="row">
        			<div class="col-7">
        				<label for="ref_title">Job Title:</label>
        				<input name="ref_title" id="ref_title" class="form-control form-control-sm" type="text" size="50" value="#htmlEditFormat(getInfo.ref_title)#" placeholder="Reference's job title">
        			</div>
        		</div>
          	</fieldset>
		</div>
	</div>
</div>
<div class="formRowGroup">
	<div class="formRow">
		<div class="formCol">
			<label for="liked_programs">Are there any programs that were hosted throughout your time in the learning community that you'd like to see return next year?</label>
		</div>
		<div class="formCol" style="vertical-align: top;">
			<textarea name="liked_programs" id="liked_programs" class="form-control form-control-sm" placeholder="Please list requested programs...">#htmlEditFormat(getInfo.liked_programs)#</textarea>
		</div>
	</div>
</div>
<div class="formRowGroup lastFormRowGroup">
	<div class="row">
		<div class="col" style="vertical-align: top;">
			<label for="interests">The additional opportunities listed on the right will be available to CNSLC members. Please indicate which of them you are interested in learning more about.</label>

			<fieldset id="interests" style="">
	            <legend style="width: auto; font-size: 1em;">Additional Opportunities</legend>
	            <div class="col">

	                <div class="row">
	                    <div class="col-1">
	                        <input type="checkbox" name="int_pal" id="int_pal" value="1" style="margin-top: 7px; float: right;"<cfif getInfo.int_pal EQ 1> checked</cfif>>
	                    </div>
	                    <div class="col">
	                        <label for="int_pal"><p style="font-size: 1em; font-weight: bold; margin: 0; padding: 0;">Serving as a peer academic leader (PAL) in the community</p></label>
	        				<p style="font-size: .85em; padding-top: 2px; padding-bottom: 0;">(<a href="mailto:#application.palEmail#" target="_blank">Email #application.palContact# for more information</a>)</p>
	                    </div>
	                </div>

	                <div style="margin: 20px; border-bottom: 1px solid ##ededed;"></div>

					<div class="row">
						<div class="col-1">
	                        <input type="checkbox" name="int_returners" id="int_returners" value="1" style="margin-top: 7px; float: right;"<cfif getInfo.int_returners EQ 1> checked</cfif>>
	                    </div>
	                	<div class="col" style="width: 93%;">
	                        <label for="int_returners">
	                            <p style="font-size: 1em; font-weight: bold; margin: 0; padding: 0;">Returners</p>
	                            <p style="padding-top: 2px; padding-bottom: 0;">Are you interested in living on a floor with other students who are returning to LiveOn in the residence halls?</p>
	                        </label>
	                    </div>
					</div>

					<cfif getInfo.pi_gender EQ "F">
	                <div style="margin: 20px; border-bottom: 1px solid ##ededed;"></div>

	                <div class="row">
						<div class="col-1">
	                        <input type="checkbox" name="int_wins" id="int_wins" value="1" style="margin-top: 7px; float: right;"<cfif getInfo.int_wins EQ 1> checked</cfif>>
	                    </div>
	                	<div class="col">
	                        <label for="int_wins">
	                            <p style="font-size: 1em; font-weight: bold; margin: 0; padding: 0;">Women in Natural Sciences</p>
	                            <p style="padding-top: 2px; padding-bottom: 0;">Are you interested in living on a floor with other students who identify as women in the sciences?</p>
	                        </label>
	                    </div>
					</div>
					</cfif>

				</div>
			</fieldset>
		</div>
	</div>
</div>

</form>

</cfoutput>