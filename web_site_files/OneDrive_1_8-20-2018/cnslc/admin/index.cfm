<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfparam name="url.filter" default="In-Progress">
<cfparam name="url.filtertype" default="app_status">

<!---<link rel="stylesheet" href="/std_script/theme.blue.css">--->

<cfoutput>

<cfinclude template="mods/_pageHeader.cfm">
<cfmodule template="mods/menu.cfm">

<div id="rightSide">
	<h2>
		<cfswitch expression="#url.filter#">
			<cfdefaultcase>
				In-Progress Applications
			</cfdefaultcase>
			<cfcase value="In-Progress">
				In-Progress Applications
			</cfcase>
			<cfcase value="Completed">
				Completed Applications
			</cfcase>
			<cfcase value="Waitlisted">
				Waitlisted Applications
			</cfcase>
			<cfcase value="Held">
				Held Applications
			</cfcase>
			<cfcase value="Declined">
				Declined Applications
			</cfcase>
			<cfcase value="Accepted">
				All Accepted Applications
			</cfcase>
			<cfcase value="General">
				General Cluster Applications
			</cfcase>
			<cfcase value="Returners">
				Returners Cluster Applications
			</cfcase>
			<cfcase value="SOS">
				Science Outreach Scholars Cluster Applications
			</cfcase>
			<cfcase value="Sustainability">
				Sustainability Cluster Applications
			</cfcase>
			<cfcase value="WINS">
				WINS Cluster Applications
			</cfcase>
		</cfswitch>
	</h2>

	<!--- BEGIN MAIN QUERY --->
	<cfset filterTypeValid = false>
	<cfif (url.filtertype EQ "app_status") OR (url.filtertype EQ "cluster")>
		<cfset filterTypeValid = true>
	</cfif>

	<cfif filterTypeValid>
		<!--- Set a url filter var to the database name when they differ (space " " issues) --->
		<cfset dbFilter = url.filter>
		<cfif url.filter EQ "SOS">
			<cfset dbFilter = "Science Outreach">
		</cfif>

		<cfquery name="getApplications" datasource="cnslc_maria">
			SELECT 
				id,
				csuid,
				CONCAT(last_name, ', ', first_name) AS full_name,
				pi_gender,
				app_status,
				said_major,
				ods_major,
				app_type,
				confirmation_decision,
				cluster,
				start_dt,
				complete_dt
			FROM application_form
			WHERE #url.filtertype# = <cfqueryparam value="#dbFilter#" cfsqltype="cf_sql_varchar">
			<!--- "Declined" view should include both apps we declined and apps that students declined after we made them na offer --->
			<cfif dbFilter EQ "Declined"> OR confirmation_decision = 'Declined'</cfif>
			<!--- If this is a "roster by cluster" view, make sure to exclude those apps who delcined our offer (needed b/c their cluster remains intact)--->
			<cfif (url.filtertype EQ "cluster")>AND confirmation_decision <> 'Declined'</cfif>
			<cfif (dbFilter EQ "Accepted")>OR app_status = 'Held'</cfif>
			<!--- Add sort only for waitlist view --->
			<cfif url.filter EQ "Waitlisted">ORDER BY latest_decision_dt ASC
			<cfelse>ORDER BY complete_dt</cfif>
		</cfquery>

	<!--- Invalid URL param --->
	<cfelse>
		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin: Invalid filtertype provided in URL (#url.filtertype#)">

		<!--- Kick user out --->
		<cflocation url="#application.urlpath#?status=invalidData" addtoken="false">
	</cfif>
	<!--- END MAIN QUERY --->

	<!--- BEGIN MAIN OUTPUT TABLE --->
	<cfset wlPos = 1>
	<div class="rowCountDiv">Displaying <span class="liveRowCount"></span> of <span class="intialRowCount">#getApplications.recordCount#</span></div>
	<div style="clear: both;"></div>
	<table id="mainTable" class="tablesorter tsArrows tsArrowsBothFade" style="table-layout: fixed;">
		<thead>
			<tr>
				<cfif url.filter EQ "Waitlisted">
					<th class="filter-false" style="width: 30px;">##</th>
				</cfif>
				<cfif (url.filtertype EQ "cluster") OR (url.filter EQ "Accepted")>
					<th class="filter-select" style="width: 20px;"></th>
				</cfif>
				<th>Name</th>
				<th style="width: 120px;">CSU ID</th>
				<cfif url.filter EQ "Accepted"><th class="filter-select">Cluster</th></cfif>
				<th>Major (ODS)</th>
				<th class="filter-select" style="width: 70px;">Gender</th>
				<th class="filter-select" style="width: 110px;">App Type</th>
				<th style="width: 120px;">
					<cfif url.filter EQ "In-Progress">
						Start Date
					<cfelse>
						Completed Date
					</cfif>
				</th>
			</tr>
		</thead>
		<tbody>
			<cfloop query="getApplications">
				<tr>
					<cfif url.filter EQ "Waitlisted">
						<td>
							#wlPos#
							<cfset wlPos++>
						</td>
					</cfif>
					<cfif (url.filtertype EQ "cluster") OR (url.filter EQ "Accepted")>
						<td>
							<cfif app_status EQ "Held">
								<i class="far fa-pause-circle fa-lg" aria-hidden="true" style="color: ##555;"></i><span style="display: none;">H</span>
							<cfelseif app_status EQ "Accepted">
								<i class="far fa-check fa-lg" aria-hidden="true" style="color: ##555;"></i><span style="display: none;">A</span>
							</cfif>
						</td>
					</cfif>
					<td>
						<a href="viewApp.cfm?csuid=#htmlEditFormat(csuid)#" style="text-decoration: underline;">#htmlEditFormat(full_name)#</a>
						<cfif confirmation_decision EQ "Confirmed">
							<i class="far fa-handshake" aria-hidden="true" data-toggle="tooltip" title="Confirmed" style="color: ##777; margin-left: 5px; font-size: .8em;"></i>
						<cfelseif confirmation_decision EQ "Declined">
							<i class="far fa-handshake" aria-hidden="true" data-toggle="tooltip" title="Declined" style="color: ##b00000; margin-left: 5px; font-size: .8em;"></i>
						</cfif>
					</td>
					<td>#htmlEditFormat(csuid)#</td>
					<cfif url.filter EQ "Accepted">
						<td>
							#cluster#
						</td>
					</cfif>
					<td>#htmlEditFormat(ods_major)#</td>
					<td>#htmlEditFormat(pi_gender)#</td>
					<td>#htmlEditFormat(app_type)#</td>
					<td>
						<cfif url.filter EQ "In-Progress">
							#dateFormat(start_dt, "mm/dd/yyyy")#
						<cfelse>
							#dateFormat(complete_dt, "mm/dd/yyyy")#
						</cfif>
					</td>
				</tr>
			</cfloop>
		</tbody>
	</table>
	<div class="rowCountDiv">Displaying <span class="liveRowCount"></span> of <span class="intialRowCount">#getApplications.recordCount#</span></div>
	<div style="clear: both;"></div>
	<!--- END MAIN OUTPUT TABLE --->
	
</div>

</cfoutput>

<div style="clear: both;"></div>
<cfinclude template="#application.urlpath#mods/tech_issues.cfm">

<script>
$(function(){
	// Highlight current menu item
	$('[data-menuID=<cfoutput>#url.filter#</cfoutput>]').addClass('current');

	// Table sorter
	var mTable = $('#mainTable');

	mTable.tablesorter({
		widgets: ['zebra', 'filter', 'saveSort'],
		widgetOptions: {
			filter_columnFilters: true,
			filter_saveFilters: false,
			filter_reset: 'button.resetFilter',
			filter_functions: {
			}
		}
	});
	mTable.bind('filterInit filterEnd pagerComplete', function (event, data) {
	    $('.liveRowCount').html( data.filteredRows );
	});

	// Tooltip
	$('[data-toggle="tooltip"]').tooltip();
});
</script>