<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfparam name="url.csuid" default="0">
<cfif url.csuid EQ ''>
	<cfset url.csuid = 0>
</cfif>

<!--- BEGIN MAIN QUERY --->
<cfquery name="getInfo">
	SELECT 
		*,
		CONCAT(last_name, ', ', first_name) AS full_name
	FROM application_form
	WHERE csuid = <cfqueryparam value="#url.csuid#" cfsqltype="cf_sql_varchar" maxlength="20">
</cfquery>
<!--- END MAIN QUERY --->
<!--- Get Waitlist Number if Needed --->
<cfset wlPos = 0>
<cfif getInfo.app_status EQ "Waitlisted">
	<cfquery name="getWaitlist">
		SELECT id, csuid, latest_decision_dt
		FROM application_form
		WHERE app_status = 'Waitlisted'
		ORDER BY latest_decision_dt ASC
	</cfquery>

	<cfset tempPos = 1>
	<cfloop query="getWaitlist">
		<cfif csuid EQ #url.csuid#>
			<cfset wlPos = tempPos>
		<cfelse>
			<cfset tempPos++>
		</cfif>
	</cfloop>
</cfif>
<!--- Get Clusters --->
<cfquery name="getClusters">
	SELECT short_name
	FROM ref_clusters
	<cfif getInfo.app_type EQ "Returning">
	WHERE returner = 1
	<cfelse>
	WHERE incoming = 1
	</cfif>
	AND
	<cfif getInfo.pi_gender EQ "F">
	female = 1
	<cfelse>
	male = 1
	</cfif>
	ORDER BY full_name
</cfquery>

<!--- Check if ODS major is actually one of ours --->
<cfquery name="checkMajor">
	SELECT id
	FROM ref_majors
	WHERE major_full = <cfqueryparam value="#getInfo.ods_major#" cfsqltype="cf_sql_varchar">
</cfquery>
<cfset validMajor = true>
<cfif checkMajor.recordCount LT 1>
	<cfset validMajor = false>
</cfif>
<!--- Check if majors match --->
<cfset majorsMatch = true>
<cfif (getInfo.app_type EQ "Incoming") AND (getInfo.ods_major NEQ getInfo.said_major)>
	<cfset majorsMatch = false>
</cfif>


<cfoutput>

<cfinclude template="mods/_pageHeader.cfm">
<cfmodule template="mods/menu.cfm">

<div id="rightSide" style="width: 990px;">
	<h2>View Application</h2>

	<cfif url.csuid EQ 0>
		<h3>No record found.</h3>
	<cfelse>

	<div id="topRow">
	<h3 style="display: inline-block; float: left; margin-top: 5px;">
		#getInfo.full_name#
	</h3>
	<div style="display: inline-block; float: left; margin-top: 5px; margin-left: 10px; padding-top: 7px;">
		<cfswitch expression="#getInfo.app_status#">
			<cfdefaultcase>
				<i class="far fa-toggle-off" data-toggle="tooltip" data-html="true" title="In-Progress<br><span style='font-size: .8em;'>(Started on #htmlEditFormat(dateFormat(getInfo.start_dt, "mm/dd/yy"))#)</span>" style="color: ##777; font-weight: normal;" aria-hidden="true"></i>
			</cfdefaultcase>
			<cfcase value="In-Progress">
				<i class="far fa-toggle-off" data-toggle="tooltip" data-html="true" title="In-Progress<br><span style='font-size: .8em;'>(Started on #htmlEditFormat(dateFormat(getInfo.start_dt, "mm/dd/yy"))#)</span>" style="color: ##777; font-weight: normal;" aria-hidden="true"></i>
			</cfcase>
			<cfcase value="Completed">
				<i class="far fa-toggle-on" data-toggle="tooltip" data-html="true" title="Completed<br><span style='font-size: .8em;'>(Completed on #htmlEditFormat(dateFormat(getInfo.complete_dt, "mm/dd/yy"))#)</span>" style="color: ##777; font-weight: normal;" aria-hidden="true"></i>
			</cfcase>
			<cfcase value="Declined">
				<i class="far fa-user-times" data-toggle="tooltip" title="Declined" aria-hidden="true" style="color: ##777; font-weight: normal;"></i>
			</cfcase>
			<cfcase value="Waitlisted">
				<i class="far fa-list-ol" data-toggle="tooltip" title="Waitlist" aria-hidden="true" style="color: ##777; font-weight: normal;"></i>
				<span style="font-weight: normal; font-size: .8em; color: ##999;">(###wlPos#)</span>
			</cfcase>
			<cfcase value="Held">
				<i class="far fa-pause-circle" aria-hidden="true" style="color: ##777; font-weight: normal;" data-toggle="tooltip" title="#getInfo.cluster#"></i>
			</cfcase>
			<cfcase value="Accepted">
				<i class="far fa-check" aria-hidden="true" style="color: ##777; font-weight: normal;" data-toggle="tooltip" title="#getInfo.cluster#"></i>
			</cfcase>
		</cfswitch>
		<cfif (getInfo.email_autosent_decline EQ 1) OR (getInfo.email_autosent_hold EQ 1) OR (getInfo.email_autosent_accept EQ 1)>

			<cfset emailsList = "">
			<cfif getInfo.email_autosent_decline EQ 1>
				<cfset emailsList = listAppend(emailsList, "Decline", "<br>")>
			</cfif>
			<cfif getinfo.email_autosent_hold EQ 1>
				<cfset emailsList = listAppend(emailsList, "Hold", "<br>")>
			</cfif>
			<cfif getInfo.email_autosent_accept EQ 1>
				<cfset emailsList = listAppend(emailsList, "Acceptance", "<br>")>
			</cfif>

			<span style="color: ##ccc; font-weight: normal; margin-left: 5px;">|</span>
			<span style="padding-left: 5px; padding-right: 5px; color: ##777; font-weight: normal;"><i class="far fa-envelope" aria-hidden="true" data-html="true" data-toggle="tooltip" title="Automatic Email(s) Sent:<br>#emailsList#"></i></span>
			
		</cfif>
		<cfif getInfo.confirmation_decision EQ "Confirmed">
			<span style="color: ##ccc; font-weight: normal; margin-right: 5px;">|</span><span style="padding-left: 5px; padding-right: 5px; color: ##777; font-weight: normal;"><i class="far fa-handshake" aria-hidden="true" data-toggle="tooltip" title="Confirmed"></i></span>
		<cfelseif getInfo.confirmation_decision EQ "Declined">
			<span style="color: ##ccc; font-weight: normal; margin-right: 5px;">|</span><span style="padding-left: 5px; padding-right: 5px; color: ##b00000; font-weight: normal;"><i class="far fa-handshake" aria-hidden="true" data-toggle="tooltip" title="Declined"></i></span>
		</cfif>
	</div>
	

	<!--- Action Buttons --->
	<div id="adminActionDiv">
		<cfif getInfo.app_status EQ "In-Progress">
			<button type="button" data-adminaction="lock" class="adminActionButton"><i class="far fa-check-square" aria-hidden="true"></i> Mark Complete &amp; Lock</button>

			<cfif getInfo.app_type EQ "Returning">
				<button type="button" data-adminaction="switchToIncoming" class="adminActionButton"><i class="far fa-random" aria-hidden="true"></i> Switch to Incoming</button>
			<cfelse>
				<button type="button" data-adminaction="switchToReturning" class="adminActionButton"><i class="far fa-random" aria-hidden="true"></i> Switch to Returning</button>
			</cfif>
		<cfelse>
			<button type="button" data-adminaction="unlock" class="adminActionButton"><i class="far fa-unlock" aria-hidden="true"></i> Unlock &amp; Reopen</button>
		</cfif>
		<button type="button" data-adminaction="updateODSInfo" class="adminActionButton"><i class="far fa-cloud-download" aria-hidden="true"></i> Sync ODS Data</button>
	</div>
	</div>

	<div style="clear: both;""></div>


	<!--- Applicant's Basic Info --->
	<table class="quickInfo" style="margin: 20px auto 20px auto;">
		<thead>
			<tr>
				<th colspan="4">Quick Info</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>Email</th>
				<td style="min-width: 170px;"><a href="mailto:#htmlEditFormat(getInfo.email)#" style="text-decoration: underline;">#htmlEditFormat(getInfo.email)#</td>

				<th>Age</th>
				<td style="min-width: 170px;">#htmlEditFormat(getInfo.pi_age)#</td>
			</tr>
			<tr>
				<th>Phone</th>
				<td>
					<cfif getInfo.phone NEQ "">
						(#htmlEditFormat(left(getInfo.phone, 3))#) #htmlEditFormat(mid(getInfo.phone, 4, 3))#-#htmlEditFormat(right(getInfo.phone, 4))#
					</cfif>
				</td>
				
				<th>Gender</th>
				<td>#htmlEditFormat(getInfo.pi_gender)#</td>
			</tr>
			<tr>
				
				<th>CSU ID</th>
				<td>#htmlEditFormat(getInfo.csuid)#</td>

				<th>Ethnicity</th>
				<td>#htmlEditFormat(getInfo.pi_ethnicity)#</td>

			</tr>
			<tr>
				<th>ODS Major</th>
				<td>
					<cfif NOT validMajor>
						<span style="color: ##b00000;">#htmlEditFormat(getInfo.ods_major)#</span>
					<cfelseif NOT majorsMatch>
						<span style="color: ##c18d00;">#htmlEditFormat(getInfo.ods_major)#</span>
					<cfelse>
						#htmlEditFormat(getInfo.ods_major)#
					</cfif>
				</td>

				<th>Attributes</th>
				<td><cfif getInfo.ods_first_gen EQ "Y">First Gen<cfif getInfo.ods_asset EQ "Y">,</cfif></cfif><cfif getInfo.ods_asset EQ "Y">ASSET</cfif></td>
			</tr>
			<tr>
				<th>Said Major</th>
				<td>
					<cfif getInfo.app_type EQ "Returning">
						N/A
					<cfelseif NOT majorsMatch>
						<span style="color: ##c18d00;">#htmlEditFormat(getInfo.said_major)#</span>
					<cfelse>
						#htmlEditFormat(getInfo.said_major)#
					</cfif>
				</td>

				<th>ADM Index</th>
				<td>#htmlEditFormat(getInfo.ods_adm_index)#</td>
			</tr>
			<tr>
				<th>Year</th>
				<td>#htmlEditFormat(getInfo.student_year)#</td>

				<th>Home State</th>
				<td>
					<cfif getInfo.pi_home_state EQ "">
						[#htmlEditFormat(getInfo.pi_home_nation)#]
					<cfelse>
						#htmlEditFormat(getInfo.pi_home_state)#
					</cfif>
				</td>
			</tr>
			<tr>
				<th>Completed Date</th>
				<td><cfif getInfo.complete_dt NEQ "">#dateFormat(getInfo.complete_dt, "mm/dd/yyyy")#<span style="color: ##777;"> @ #timeFormat(getInfo.complete_dt, "h:mm tt")#</span></cfif></td>

				<th>Decision Date</th>
				<td><cfif getInfo.latest_decision_dt NEQ "">#dateFormat(getInfo.latest_decision_dt, "mm/dd/yyyy")#<span style="color: ##777;"> @ #timeFormat(getInfo.latest_decision_dt, "h:mm tt")#</span></cfif></td>
			</tr>
			<!--- If applicant has been accepted but hasn't deciced, give admin access to the confirm URL in case it's needed --->
			<cfif (getInfo.app_status EQ "Accepted") AND (getInfo.confirmation_decision EQ 0)>
			<tr>
				<th colspan="1"><i class="far fa-handshake" aria-hidden="true" style="padding-right: 5px;"></i>Link</th>
				<td colspan="4"><a href="#application.urlpath_https#confirm.cfm?cid=#getInfo.confirmation_code#" style="font-size: .7em;">#application.urlpath_https#confirm.cfm?cid=#getInfo.confirmation_code#</a></td>
			</tr>
			</cfif>
		</tbody>
	</table>
	

	<!--- Get Application --->
	<div id="adminView">
	<cfif getInfo.app_type EQ "Incoming">
		<h4 class="appHeader">Incoming Application</h4>
		<cfinclude template="#application.urlpath#apps/incoming.cfm">
	<cfelseif getInfo.app_type EQ "Returning">
		<h4 class="appHeader">Returning Application</h4>
		<cfinclude template="#application.urlpath#apps/returning.cfm">
	</cfif>
	<cfif getInfo.confirmation_decision EQ "Declined">
		<h4 class="appHeader" style="margin-top: 40px; margin-bottom: 10px;">Reason for Declining <span style="font-size: .7em; font-weight: normal;">(#dateFormat(getInfo.confirmation_decision_dt, "mm/dd/yyyy")#)</span></h4>
		<textarea class="form-control" style="border: 1px solid ##888; padding: 2px 5px 2px 5px; font-size: 1em; min-height: 70px; min-width: 957px; max-width: 957px;">#getInfo.confirmation_decline_why#</textarea>
	</cfif>
	</div>


	<div id="notesArea">

		<!--- Admin notes --->
		<h4><label for="admin_notes">Administration Notes:</label></h4>
		<div class="noteBox">
			<textarea name="admin_notes" id="admin_notes" class="notes" placeholder="Administration notes...">#htmlEditFormat(getInfo.admin_notes)#</textarea>
			<div id="save_admin_notes"><i class="far fa-save" aria-hidden="true"></i> Save Notes</div>
		</div>

	</div>

	
	<div id="adminBottomArea">
		
		<!--- Decision Options --->
		<cfif getInfo.app_status NEQ "In-Progress"><!--- Don't allow modification unless application is at least complete --->
			<div id="decisionsDiv">

				<!--- Include Mail Option --->
				<div class="decisionDiv" id="decisionDivDecline">
					<!--- Main Button Div --->
					<div class="decisionButton<cfif getInfo.app_status EQ "Declined"> currentButton noSubmit<cfelse> hasExtra</cfif>" id="dbDecline" <cfif getInfo.app_status NEQ "Declined">data-toggle="declineExtra"</cfif>>
						<i class="far fa-user-times fa-lg" aria-hidden="true" style="padding-right: 5px;"></i> Decline
					</div>

					<!--- Extra Options Div --->
					<div id="autoEmailDecline" class="decisionExtra deFirst declineExtra decisionEmail">
						<label><input type="checkbox" name="autoSend" id="autoSendDecline" value="1" checked> Auto-send email</label>
					</div>
					<div class="decisionExtra declineExtra decisionGo">
						<button type="button" class="goButton iAmButton<cfif getInfo.app_status EQ "Declined"> noSubmit</cfif>" id="goButtonDecline" data-optionvalue="Declined" data-emailopt="autoSendDecline">
							<i class="far fa-user-times" aria-hidden="true"></i> Decline
						</button>
					</div>
				</div>

				<div class="decisionDiv">
					<div class="decisionButton iAmButton<cfif getInfo.app_status EQ "Waitlisted"> currentButton noSubmit</cfif>" id="dbWaitlist" data-optionvalue="Waitlisted">
						<i class="far fa-list-ol fa-lg" aria-hidden="true" style="padding-right: 5px;"></i> Waitlist
					</div>
				</div>

				<!--- Include Cluster Options --->
				<div class="decisionDiv" id="decisionDivHold">
					<!--- Main Button Div --->
					<div class="decisionButton hasExtra<cfif getInfo.app_status EQ "Held"> currentButton</cfif>" id="dbHold" data-toggle="holdExtra">
						<i class="far fa-pause-circle fa-lg" aria-hidden="true" style="padding-right: 5px;"></i> Hold
					</div>

					<!--- Extra Options Div --->
					<div class="decisionExtra deFirst holdExtra decisionCluster">
						<select name="cluster" id="holdcluster"<cfif getInfo.app_status EQ "Held"> class="currentSelect"</cfif> data-gobutton="goButtonHold">
							<cfloop query="getClusters">
								<option value="#short_name#"<cfif getInfo.cluster EQ #short_name#>class="currentOption" selected</cfif>>#short_name#</option>
							</cfloop>
						</select>
					</div>
					<div id="autoEmailHold" class="decisionExtra deFirst holdExtra decisionEmail">
						<label><input type="checkbox" name="autoSend" id="autoSendHold" value="1" checked> Auto-send email</label>
					</div>
					<div class="decisionExtra holdExtra decisionGo">
						<button type="button" class="goButton iAmButton<cfif getInfo.app_status EQ "Held"> noSubmit</cfif>" id="goButtonHold" data-optionvalue="Held" data-select="holdcluster" data-emailopt="autoSendHold">
							<i class="far fa-arrow-circle-right" aria-hidden="true"></i> <cfif getInfo.app_status EQ "Held">Move<cfelse>Hold</cfif> into Cluster
						</button>
					</div>
				</div>

				<!--- Include Cluster Options --->
				<div class="decisionDiv" id="decisionDivAccept">
					<!--- Main Button Div --->
					<div class="decisionButton hasExtra<cfif getInfo.app_status EQ "Accepted"> currentButton</cfif>" id="dbAccept" data-toggle="acceptExtra">
						<i class="far fa-check fa-lg" aria-hidden="true" style="padding-right: 5px;"></i> Accept
					</div>

					<!--- Extra Options Div --->
					<div class="decisionExtra deFirst acceptExtra decisionCluster">
						<select name="cluster" id="acceptcluster"<cfif getInfo.app_status EQ "Accepted"> class="currentSelect"</cfif> data-gobutton="goButtonAccept">
							<cfloop query="getClusters">
								<option value="#short_name#"<cfif getInfo.cluster EQ #short_name#>class="currentOption" selected</cfif>>#short_name#</option>
							</cfloop>
						</select>
					</div>
					<div id="autoEmailAccept" class="decisionExtra deFirst acceptExtra decisionEmail">
						<label><input type="checkbox" name="autoSend" id="autoSendAccept" value="1" checked> Auto-send email</label>
					</div>
					<div class="decisionExtra acceptExtra decisionGo">
						<button type="button" class="goButton iAmButton<cfif getInfo.app_status EQ "Accepted"> noSubmit</cfif>" id="goButtonAccept" data-optionvalue="Accepted" data-select="acceptcluster" data-emailopt="autoSendAccept">
							<i class="far fa-arrow-circle-right" aria-hidden="true"></i> <cfif getInfo.app_status EQ "Accepted">Move<cfelse>Accept</cfif> into Cluster
						</button>
					</div>
				</div>

			</div>
		</cfif>
	</div>

	</cfif>

</div>

<div style="clear: both;"></div>

<!--- For super admins only --->
<cfif (session.permission_level GTE 9) AND (url.csuid NEQ 0)>
	<cfquery name="getLog">
		SELECT *
		FROM access_log
		WHERE
			(ename = <cfqueryparam value="#getInfo.ename#" cfsqltype="cf_sql_varchar">) OR
			(impacted_user = <cfqueryparam value="#getInfo.ename#" cfsqltype="cf_sql_varchar">)
		ORDER BY
			access_dt DESC,
			id DESC
	</cfquery>

	<h4 id="logToggle" style="display: block; margin: 0 auto; margin-top: 50px; padding-top: 10px; cursor: pointer; text-align: center; border-top: 1px solid ##ddd;">Log:</h4>
	
	<div id="logDiv">
	<table id="access_log_table" class="tableStyles tablesorter tsArrows tsArrowsBothFade unifyInputHeights" style="width: 98%; padding-right: 10px;">
		<thead>
			<tr>
				<th>Action</th>
				<th class="filter-select">Action By</th>
				<th class="filter-select">User Type</th>
				<th>Datetime</th>
				<th class="filter-select">IP Address</th>
			</tr>
		</thead>
		<tbody>
			<cfloop query="getLog">
				<tr>
					<td>#action#</td>
					<td>#ename#</td>
					<td>#session_user_level#</td>
					<td>#dateFormat(access_dt, "mm/dd/yyyy")# #timeFormat(access_dt, "h:mm tt")#</td>
					<td>#ip_address#</td>
				</tr>
			</cfloop>
		</tbody>
	</table>
	</div>
</cfif>

</cfoutput>


<script>
$(document).ready(function(){
// Highlight current menu item
<cfif getInfo.app_status EQ "Declined" OR getInfo.confirmation_decision EQ "Declined">
	<cfset currentTab = "Declined">
<cfelse>
	<cfif getInfo.cluster NEQ 0>
		<cfif getInfo.cluster EQ "Science Outreach">
			<cfset currentTab = "SOS">
		<cfelse>
			<cfset currentTab = getInfo.cluster>
		</cfif>
	<cfelse>
		<cfset currentTab = getInfo.app_status>
	</cfif>
</cfif>
$('[data-menuID=<cfoutput>#currentTab#</cfoutput>]').addClass('current');

/* ------------ Disable inputs -------------  */
$('#adminView :input').attr('disabled', true);

// Tooltip
$("body").tooltip({ selector: '[data-toggle=tooltip]' });


/* -- Only Show DIVs if Checkbox Checked ---  */
$('#interests input:checkbox').each(function(index){
	// Get corresponding div
	var initextradiv = $(this).data('extradiv');

	// If checked
	if( $(this).is(':checked') ){
		$('#' + initextradiv).show();
	} else{
	// If unchecked
		$('#' + initextradiv).hide();
	}
});

/* -- Auto Height Textareas to fit Content -- */
$("textarea").each(function(){$(this).height( $(this)[0].scrollHeight )});


/* ------- Admin Actions (Top Right) -------- */
$('.adminActionButton').click(function(){
	// Store action
	var aAction = $(this).data('adminaction');

	// Boolean used to proceed
	var goOn = true;

	// Going ahead and coding for if I decide to erase irrelevant data in the future (currently this message is a lie) ...
	if( (aAction == 'switchToIncoming') || (aAction == 'switchToReturning') ){
		if( !confirm('When you switch application types, data that is no longer relevant to the other application type will be lost.') ){goOn = false;}
	}

	if(goOn){
		$.ajax({
			type: 'post',
			url: 'mods/adminActions.cfc?method=' + aAction,
			data: {'ename' : '<cfoutput>#getInfo.ename#</cfoutput>'},
			datatype: 'json',
			success: function(){window.location.reload(true);},
			error: function(){alert('An error has occurred. (Code 478)');}
		});
	}
});


/* ---- Save Admin Notes (Bottom of Page) ---- */
$('#save_admin_notes').click(function(){
	// Declare object to be returned
	var noteObj = {};
	noteObj.ename = '<cfoutput>#getInfo.ename#</cfoutput>';
	noteObj.admin_notes = $('#admin_notes').val();

	$.ajax({
		type: 'post',
		url: 'mods/updateNotes.cfc?method=adminNotes',
		data: noteObj,
		datatype: 'json',
		success: function(){alert('Notes saved.');window.location.reload(true);},
		error: function(){alert('An error has occurred. (Code 497)');}
	});
});


/* ---- Admin Decisions (Bottom of Page) ---- */
// Visual highlight of current button
var buttonColor = $('.currentButton').css('background-color');
$('.currentButton').parent('div').css('border','1px solid ' + buttonColor);

// Assign each button ready to "open" on their next click (to show their "extra" divs)
$('.decisionButton').addClass('openNextClick');

// On button click - expand if needed
$('.decisionButton').click(function(){

	// On click of any decision, go ahead and hide all extra divs (in case they were currently displayed)
	$('.decisionExtra').hide(200);

	// Get the button/div's extra div id
	var extraClass = $(this).data('toggle');

	// If this button is meant to be shown (based off of below logic), show it
	if( $(this).hasClass('openNextClick')){
		$( '.' + extraClass ).show(200); // Show
	}

	// Setting the logic...
	if( $(this).hasClass('openNextClick') ){ // If this button WAS set to open next click...
		$(this).removeClass('openNextClick'); // Remove the open next click
		$(this).addClass('closeNextClick'); // Add the close next click
	} else{ // If the button WAS set to close next click...
		$(this).removeClass('closeNextClick'); // Remove that it should be closed next time
		$(this).addClass('openNextClick'); // Add that it should be opened next click
	}

	// Reset all other buttons
	$('.decisionButton').not(this).removeClass('closeNextClick');
	$('.decisionButton').not(this).addClass('openNextClick');
	
});

// Monitor if goButtion should be allowed to submit (is the "select" different than it was originally)
// And manipulate the "auto send email" optoins accordingly
$('.currentSelect').change(function(){
	// Selected option is same as before
	if( this.value == $('.currentOption').html() ){
		$('#' + $(this).data('gobutton')).addClass('noSubmit');
	} else{
		$('#' + $(this).data('gobutton')).removeClass('noSubmit');
	}
});

// On button click - actions
$('.iAmButton').click(function(){
	// Only submit if allowed (aka not the previously selected option)
	if( !$(this).hasClass('noSubmit') ){
		// Declare object to be sent
		var dataObj = {};

		// Add essential data to object
		dataObj.app_status = $(this).data('optionvalue');
		dataObj.csuid = '<cfoutput>#getInfo.csuid#</cfoutput>';
		dataObj.ename = '<cfoutput>#getInfo.ename#</cfoutput>';
		var sendCheck = 0;
		if( $('#' + $(this).data('emailopt')).is(':checked') ){
			sendCheck = 1;
		}
		dataObj.autosent = sendCheck;

		// If the buttion clicked was the "extra"
		if( $(this).hasClass('goButton') ){
			dataObj.cluster = $( '#' + $(this).data('select') ).val();
		}

		// AJAX to update record
		$.ajax({
			type: 'post',
			url: 'mods/updateAppStatus.cfc?method=update',
			data: dataObj,
			datatype: 'json',
			success: function(){alert('Application successfully ' + dataObj.app_status);window.location.reload(true);},
			error: function(){alert('An error has occurred. (Code 579)');}
		})
	}
});

//Table sorter stuff
var mTable = $('#access_log_table');

mTable.tablesorter({
	widgets: ['zebra', 'filter', 'saveSort'],
	widgetOptions: {
		filter_columnFilters: true,
		filter_saveFilters: true,
		filter_reset: 'button.resetFilter',
		filter_functions: {

		}
	}
});

$('#logDiv').hide();
$('#logToggle').click(function(){
	$('#logDiv').slideToggle('slow');
});

});

</script>

<cfinclude template="#application.urlpath#mods/tech_issues.cfm">
