<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="lock" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">

		<!--- Lock (Mark Complete) Application --->
		<cfquery name="lockApplication">
			UPDATE application_form
			SET
				app_status = 'Completed',
				complete_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#',
				cluster = 0
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Application locked and marked complete" impacted_user="#arguments.ename#">

	</cffunction>

	<cffunction name="unlock" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">

		<!--- Unlock Application --->
		<cfquery name="unlockApplication">
			UPDATE application_form
			SET
				app_status = 'In-Progress',
				complete_dt = NULL,
				cluster = 0,
				latest_decision_by = NULL,
				latest_decision_dt = NULL,
				confirmation_code = NULL,
				confirmation_decision = 0,
				confirmation_decision_dt = NULL,
				confirmation_decline_why = NULL,
				email_autosent_hold = 0,
				email_autosent_accept = 0
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Application unlocked and marked in-progress" impacted_user="#arguments.ename#">

	</cffunction>



	<!---
		MAYBE wipe fields that belong to the other type in the future?
	--->
	<cffunction name="switchToIncoming" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">

		<!--- Switch to Incoming --->
		<cfquery name="switchToIncoming">
			UPDATE application_form
			SET app_type = 'Incoming'
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar" maxlength="30">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Application switched to Incoming app type" impacted_user="#arguments.ename#">

	</cffunction>



	<cffunction name="switchToReturning" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">

		<!--- Switch to Incoming --->
		<cfquery name="switchToIncoming">
			UPDATE application_form
			SET app_type = 'Returning'
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar" maxlength="30">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Application switched to Returning app type" impacted_user="#arguments.ename#">

	</cffunction>



	<cffunction name="updateODSInfo" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">
		<cfargument name="outputLog" type="string" required="false" default="yes">

		<!--- Query ODS ---> 
		
		<!--- removed for security - just a function that queries the CSU database and updates the information with any current info --->
		
		<!--- end of removed ODS section --->
        
        
		<!--- Log action --->
		<cfif arguments.outputLog EQ "yes">
			<cfmodule template="#application.urlpath_log#" action="Admin Action: Synced applicants data with ODS" impacted_user="#arguments.ename#">
		</cfif>

	</cffunction>

	<cffunction name="updateAllODSInfo" access="remote" output="false">

		<!--- Arguments --->

		<!--- Get all ename's --->
		<cfquery name="getEnames">
			SELECT ename
			FROM application_form
		</cfquery>

		<!--- Loop through ename's and call update function --->
		<cfloop query="getEnames">
			<cfinvoke method="updateODSInfo" ename="#getEnames.ename#" outputLog="no">
		</cfloop>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Synced system's ODS data" impacted_user="Everyone">

	</cffunction>

</cfcomponent>