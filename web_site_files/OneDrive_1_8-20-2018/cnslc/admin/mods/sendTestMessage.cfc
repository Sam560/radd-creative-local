<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="sendEmail" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="msg" type="string" required="true" default="">
		<cfargument name="msgsubject" type="string" required="true" default="">
		<cfargument name="msgtext" type="string" required="true" default="">

		<!--- This is a test request, use dummy values --->
		<cfset thisEmail = "#session.email#">
		<cfset thisODSMajor = "TestODSMajor">
		<cfset thisSaidMajor = "TestSaidMajor">
		<cfset thisCCL = application.urlpath_https & "confirm.cfm?cid=A1B2C3D4">
		<cfset thisCCL = "<a href='" & thisCCL & "'>" & thisCCL & "</a>">
		<cfset thisFirstName = "TestFName">
		<cfset thisLastName = "TestLName">

		<cfset activeMsg = "<p>" & arguments.msgtext & "</p>">
		<cfset activeMsg = reReplace(activeMsg, "\<\<first_name\>\>", thisFirstName, "ALL")><!--- First name --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<last_name\>\>", thisLastName, "ALL")><!--- Last name --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<ods_major\>\>", thisODSMajor, "ALL")><!--- ODS major --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<said_major\>\>", thisSaidMajor, "ALL")><!--- Said major --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<confirmation_link\>\>", thisCCL, "ALL")><!--- Confirm link --->
		<cfset activeMsg = reReplace(activeMsg, "\n", "</p><p>", "ALL")><!--- Linebreaks --->

		<!--- Start working on custom links (Loop 10x or so to handle up to 10 links?) --->
		<cfloop from="1" to ="10" index="i">
			<cfset cLink = REmatch("\<\<\(\S*\)\>\>", activeMsg)><!--- Get the shortcode --->
			<cfparam name="cLink[1]" default="">

			<!--- Only continue if there is a custom link to maninpulate (otherwise it will error) --->
			<cfif cLink[1] NEQ "">

				<cfset cLink = cLink[1]><!--- Extract from its automatically created array --->
				<cfset cLink = left(cLink, len(cLink)-3)><!--- Remove the last 3 chars [")>>"] --->
				<cfset cLink = right(cLink, len(cLink)-3)><!--- Remove the first 3 chars ["<<("] --->
				<cfset cLinkURL = trim(listGetAt(cLink, 1))><!--- Get the URL --->
				<cfset cLinkText = trim(listGetAt(cLink, 2))><!--- Get the link text --->
				<cfset cLinkFull = '<a href="' & cLinkURL & '">' & #cLinkText# & '</a>'><!--- Build full link --->

				<cfset activeMsg = reReplace(activeMsg, "\<\<\(\S*\)\>\>", cLinkFull, "one")><!--- Finally, swap out with the actual/live link --->

			</cfif>
		</cfloop>

		<cfmail
			from="#application.emailFrom#"
			to="#thisEmail#"
			replyto="#application.emailReplyTo#"
			subject="#arguments.msgsubject#"
			type="html"
		>
		#activeMsg#
		</cfmail>
	</cffunction>
</cfcomponent>