<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="query" access="remote" returnType="array" returnformat="JSON">

		<!--- Arguments --->
		<cfargument name="app_type" type="string" required="false" default="">
		<cfargument name="app_status" type="string" required="false" default="">
		<cfargument name="cluster" type="string" required="false" default="">
		<cfargument name="pi_gender" type="string" required="false" default="">
		<cfargument name="ods_major" type="string" required="false" default="">
		<!--- <cfargument name="said_major" type="string" required="false" default=""> --->
		<cfargument name="student_year" type="string" required="false" default="">
		<cfargument name="confirmation_decision" type="string" required="false" default="">
		<cfargument name="ods_first_gen" type="string" required="false" default="">
		<cfargument name="ods_admitted" type="string" required="false" default="">
		<cfargument name="ods_asset" type="string" required="false" default="">
		<cfargument name="eth_options" type="string" required="false" default="">
		<cfargument name="ref_all_ethnicities" type="string" required="false" default="">
		<cfargument name="ethnicities" type="string" required="false" default="">
		<cfargument name="int_options" type="string" required="false" default="">
		<cfargument name="int_pal" type="string" required="false" default="">
		<cfargument name="int_returners" type="string" required="false" default="">
		<cfargument name="int_sos" type="string" required="false" default="">
		<cfargument name="int_sustainability" type="string" required="false" default="">
		<cfargument name="int_wins" type="string" required="false" default="">
		<cfargument name="min_adm" type="string" required="false" default="">
		<cfargument name="max_adm" type="string" required="false" default="">
		<cfargument name="pi_home_nation" type="string" required="false" default="">
		<cfargument name="pi_home_state" type="string" required="false" default="">

		<cfif eth_options EQ "only">
			<cfset excludeEths = "">
			<cfset remaingEths = listToArray(ref_all_ethnicities)>
			<cfset selectedEths = listToArray(ethnicities)>
			<cfset remaingEths.removeAll(selectedEths)>
		</cfif> 

		<cfset intsChecked = false>
		<cfif (arguments.int_pal NEQ "") OR (arguments.int_returners NEQ "") OR (arguments.int_sos NEQ "") OR (arguments.int_sustainability NEQ "") OR (arguments.int_wins NEQ "")>
			<cfset intsChecked = true>
		</cfif>

		<!--- Select filter --->
		<cfquery name="filteredResults">
			SELECT
				csuid, 
				CONCAT(last_name, ', ', first_name) AS full_name,
				app_type,
				app_status,
				cluster,
				pi_gender,
				ods_major,
				said_major
			FROM application_form
			WHERE
				1 = 1
				<!--- 
				<cfloop array="#arguments#" index="i">
				 	<cfif i.name EQ 1>
				 		AND csuid = ''
				 	</cfif>
				 </cfloop>
				 --->
				<cfif arguments.app_type NEQ ""> AND app_type IN (<cfqueryparam value="#arguments.app_type#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.app_status NEQ ""> AND app_status IN (<cfqueryparam value="#arguments.app_status#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.cluster NEQ ""> AND cluster IN (<cfqueryparam value="#arguments.cluster#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.pi_gender NEQ ""> AND pi_gender IN (<cfqueryparam value="#arguments.pi_gender#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.ods_major NEQ ""> AND ods_major IN (<cfqueryparam value="#arguments.ods_major#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<!--- <cfif arguments.said_major NEQ ""> AND said_major IN (<cfqueryparam value="#arguments.said_major#" cfsqltype="cf_sql_varchar" list="yes">)</cfif> --->
				<cfif arguments.confirmation_decision NEQ ""> AND confirmation_decision IN (<cfqueryparam value="#arguments.confirmation_decision#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.student_year NEQ ""> AND student_year IN (<cfqueryparam value="#arguments.student_year#" cfsqltype="cf_sql_varchar" list="yes">)</cfif>
				<cfif arguments.ods_first_gen NEQ ""> AND ods_first_gen = <cfqueryparam value="#arguments.ods_first_gen#" cfsqltype="cf_sql_varchar"></cfif>
				<cfif arguments.ods_admitted NEQ ""> AND ods_admitted = <cfqueryparam value="#arguments.ods_admitted#" cfsqltype="cf_sql_varchar"></cfif>
				<cfif arguments.ods_asset NEQ ""> AND ods_asset = <cfqueryparam value="#arguments.ods_asset#" cfsqltype="cf_sql_varchar"></cfif>
				<cfif arguments.ethnicities NEQ "">
					<cfif arguments.eth_options EQ "all">
						<cfloop list="#arguments.ethnicities#" index="i">
							AND pi_ethnicity LIKE <cfqueryparam value="%#i#%" cfsqltype="cf_sql_varchar">
						</cfloop>
					<cfelseif arguments.eth_options EQ "any">
						<cfset firstTime = true>
						AND
						(
						<cfloop list="#arguments.ethnicities#" index="i">
							<cfif NOT firstTime>
							OR 
							</cfif>
							pi_ethnicity LIKE <cfqueryparam value="%#i#%" cfsqltype="cf_sql_varchar">
							<cfset firstTime = false>
						</cfloop>
						)
					<cfelseif arguments.eth_options EQ "only">
						<cfloop array="#remaingEths#" index="i">
							AND pi_ethnicity NOT LIKE <cfqueryparam value="%#i#%" cfsqltype="cf_sql_varchar">
						</cfloop>
						<cfloop list="#arguments.ethnicities#" index="i">
							AND pi_ethnicity LIKE <cfqueryparam value="%#i#%" cfsqltype="cf_sql_varchar">
						</cfloop>
					</cfif>
				</cfif>
				<cfif (arguments.int_options EQ "all") AND (intsChecked)>
					<cfif arguments.int_pal EQ "1"> AND int_pal = 1</cfif>
					<cfif arguments.int_returners EQ "1"> AND int_returners = 1</cfif>
					<cfif arguments.int_sos EQ "1"> AND int_sos = 1</cfif>
					<cfif arguments.int_sustainability EQ "1"> AND int_sustainability = 1</cfif>
					<cfif arguments.int_wins EQ "1"> AND int_wins = 1</cfif>
				<cfelseif (arguments.int_options EQ "any") AND (intsChecked)>
					<cfset needOr = false>
					AND (
						<cfif arguments.int_pal EQ "1">int_pal = 1<cfset needOr = true></cfif>
						<cfif arguments.int_returners EQ "1"><cfif needOr> OR</cfif> int_returners = 1<cfset needOr = true></cfif>
						<cfif arguments.int_sos EQ "1"><cfif needOr> OR</cfif>int_sos = 1<cfset needOr = true></cfif>
						<cfif arguments.int_sustainability EQ "1"><cfif needOr> OR</cfif> int_sustainability = 1<cfset needOr = true></cfif>
						<cfif arguments.int_wins EQ "1"><cfif needOr> OR</cfif> int_wins = 1<cfset needOr = true></cfif>
					)
				<cfelseif (arguments.int_options EQ "only") AND (intsChecked)>
					AND int_pal <cfif arguments.int_pal EQ 1> = 1<cfelse> <> 1</cfif>
					AND int_returners <cfif arguments.int_returners EQ 1> = 1<cfelse> <> 1</cfif>
					AND int_sos <cfif arguments.int_sos EQ 1> = 1<cfelse> <> 1</cfif>
					AND int_sustainability <cfif arguments.int_sustainability EQ 1> = 1<cfelse> <> 1</cfif>
					AND int_wins <cfif arguments.int_wins EQ 1> = 1<cfelse> <> 1</cfif>
				</cfif>
				<cfif arguments.min_adm NEQ "">
					AND ods_adm_index >= <cfqueryparam value="#arguments.min_adm#" cfsqltype="cf_sql_numeric">
				</cfif>
				<cfif arguments.max_adm NEQ "">
					AND ods_adm_index <= <cfqueryparam value="#arguments.max_adm#" cfsqltype="cf_sql_numeric">
				</cfif>
				<cfif arguments.pi_home_nation NEQ "">
					AND pi_home_nation IN (<cfqueryparam value="#arguments.pi_home_nation#" cfsqltype="cf_sql_varchar" list="yes">)
				</cfif>
				<cfif arguments.pi_home_state NEQ "">
					AND pi_home_state IN (<cfqueryparam value="#arguments.pi_home_state#" cfsqltype="cf_sql_varchar" list="yes">)
				</cfif>

		</cfquery>

		<!--- Log action --->
		<!--- <cfmodule template="#application.urlpath_log#" action="Admin Action: Message default updated (#arguments.msg#)"> --->

		<cfset results = []>
		<cfloop query="filteredResults">
			<cfset tmpResult = {
				csuid = #filteredResults.csuid#,
				full_name = #filteredResults.full_name#,
				app_type = #filteredResults.app_type#,
				app_status = #filteredResults.app_status#,
				cluster = #filteredResults.cluster#,
				pi_gender = #filteredResults.pi_gender#,
				ods_major = #filteredResults.ods_major#,
				said_major = #filteredResults.said_major#
			}>
			<cfset arrayAppend(results, tmpResult)>
		</cfloop>
		<cfreturn results>

	</cffunction>

</cfcomponent>