<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="update" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="csuid" type="string" required="true" default="">
		<cfargument name="ename" type="string" required="true" default="">
		<cfargument name="app_status" type="string" required="true" default="">
		<cfargument name="autosent" type="numeric" required="false" default="0">
		<cfargument name="cluster" type="string" required="false" default="">

		<cftransaction>

		<!--- To prevent any malformed data --->
		<cfset validStatusArr = ["Declined", "Waitlisted", "Held", "Accepted"]>
		<cfif arrayContains(validStatusArr, arguments.app_status)>

						<!--- For Query Below
						#mid(now(),17,2)#<!--- Hour --->
						#mid(now(),23,2)#<!--- MS --->
						#mid(now(),14,2)#<!--- Date --->
						#mid(now(),20,2)#<!--- Mins --->
						--->

			<!--- Update Application's Status --->
			<cfquery name="updateQuery">
				UPDATE application_form
				SET
					app_status = <cfqueryparam value="#arguments.app_status#" cfsqltype="cf_sql_varchar" maxlength="20">,
					latest_decision_by = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar" maxlength="30">,
					latest_decision_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#',
					confirmation_decision = 0,
					confirmation_decision_dt = NULL,
					<cfif arguments.cluster NEQ "">
					cluster = <cfqueryparam value="#arguments.cluster#" cfsqltype="cf_sql_varchar" maxlength="20">,
					<cfelse>
					cluster = 0,
					</cfif>
					<!--- If accepted, generate code --->
					<cfif arguments.app_status EQ "Accepted">
						<cfset tmpcode = 
							randRange(1,9) & randRange(1,9) & mid(now(),17,2) & randRange(1,9) & right(arguments.csuid,2) & 
							mid(now(),23,2) & randRange(1,9) & randRange(1,9) & right(arguments.ename,1) & mid(now(),14,2) & 
							randRange(1,9) & left(arguments.ename,2) & left(arguments.csuid,1) & mid(now(),20,2) & 
							randRange(1,9) & randRange(1,9) & randRange(1,9) & randRange(1,9)
						>
					confirmation_code = '#tmpcode#'
					<!--- If anything else, reset all confirmation related values --->
					<cfelse>
					confirmation_code = NULL 
					</cfif>
				WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
			</cfquery>


			<!--- Log action --->
			<cfset tempCluster = "">
			<cfif arguments.cluster NEQ "">
				<cfset tempCluster = " (" & #arguments.cluster# & ")">
			</cfif>
			<cfmodule template="#application.urlpath_log#" action="Admin Decision: Application was #arguments.app_status##tempCluster#" impacted_user="#arguments.ename#">


			<!--- Send out Email --->
			<cfif arguments.autosent EQ 1>
				<cfif arguments.app_status EQ "Accepted">

					<!--- Setup for message --->
					<cfset thisMsg = "">
					<cfswitch expression="#arguments.cluster#">
						<cfcase value="General">
							<cfset thisMsg = "email_general">
						</cfcase>
						<cfcase value="Science Outreach">
							<cfset thisMsg = "email_outreach">
						</cfcase>
						<cfcase value="Returners">
							<cfset thisMsg = "email_returners">
						</cfcase>
						<cfcase value="Sustainability">
							<cfset thisMsg = "email_sustainability">
						</cfcase>
						<cfcase value="WINS">
							<cfset thisMsg = "email_wins">
						</cfcase>
					</cfswitch>

					<!--- Only if message setup was successful (which it should be but you never know) --->
					<cfif thisMsg NEQ "">

						<cfobject component="sendMessages" name="sendMsgObj">
						<cfinvoke component="#sendMsgObj#" method="sendEmail">
							<cfinvokeargument name="ename" value="#arguments.ename#">
							<cfinvokeargument name="msg" value="#thisMsg#">
						</cfinvoke>

						<!--- Record --->
						<cfquery name="updateSent">
							UPDATE application_form
							SET email_autosent_accept = 1
							WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
						</cfquery>

						<!--- Log action --->
						<cfmodule template="#application.urlpath_log#" action="Admin Decision: Email autosent (#arguments.app_status# - #arguments.cluster#)" impacted_user="#arguments.ename#">

					</cfif>

				</cfif>
				<cfif arguments.app_status EQ "Held">

					<!--- Currently only for SOS --->
					<cfif arguments.cluster EQ "Science Outreach">
						<!--- Do first (interest) email --->
						<cfobject component="sendMessages" name="sendMsgObj">
						<cfinvoke component="#sendMsgObj#" method="sendEmail">
							<cfinvokeargument name="ename" value="#arguments.ename#">
							<cfinvokeargument name="msg" value="email_outreach_interest">
						</cfinvoke>

						<!--- Record --->
						<cfquery name="updateSent">
							UPDATE application_form
							SET email_autosent_hold = 1
							WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
						</cfquery>

						<!--- Log action --->
						<cfmodule template="#application.urlpath_log#" action="Admin Decision: Email autosent (#arguments.app_status# - #arguments.cluster#)" impacted_user="#arguments.ename#">
					</cfif>

				</cfif>
				<cfif arguments.app_status EQ "Declined">

					<cfobject component="sendMessages" name="sendMsgObj">
					<cfinvoke component="#sendMsgObj#" method="sendEmail">
						<cfinvokeargument name="ename" value="#arguments.ename#">
						<cfinvokeargument name="msg" value="email_decline">
					</cfinvoke>

					<!--- Record --->
					<cfquery name="updateSent">
						UPDATE application_form
						SET email_autosent_decline = 1
						WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
					</cfquery>

					<!--- Log action --->
					<cfmodule template="#application.urlpath_log#" action="Admin Decision: Email autosent (#arguments.app_status#)" impacted_user="#arguments.ename#">

				</cfif>
			</cfif>


		<!--- Provided app_status not valid --->
		<cfelse>
			<!--- Log action --->
			<cfmodule template="#application.urlpath_log#" action="Admin Decision: Invalid app_status provided to update query (#arguments.app_status#)" impacted_user="#arguments.ename#">

			<!--- Kick user out --->
			<cflocation url="#application.urlpath#?status=invalidData" addtoken="false">
		</cfif>

		</cftransaction>

	</cffunction>


</cfcomponent>