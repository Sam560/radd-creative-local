<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="sendEmail" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">
		<cfargument name="msg" type="string" required="true" default="">

		<!--- Get applicant data --->
		<cfquery name="getAppData">
			SELECT first_name, last_name, email, ods_major, said_major, confirmation_code
			FROM application_form
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Get appropriate message data --->
		<cfquery name="getMsg">
			SELECT *
			FROM ref_messages
			WHERE msg = <cfqueryparam value="#arguments.msg#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Prep confirmation link --->
		<cfset ccl = application.urlpath_https & "confirm.cfm?cid=" & #getAppData.confirmation_code#>
		<cfset ccl = "<a href='" & ccl & "'>" & ccl & "</a>">

		<!--- Start REGEXing --->
		<cfset activeMsg = "<p>" & getMsg.msg_text & "</p>">
		<cfset activeMsg = reReplace(activeMsg, "\<\<first_name\>\>", getAppData.first_name, "ALL")><!--- First name --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<last_name\>\>", getAppData.last_name, "ALL")><!--- Last name --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<ods_major\>\>", getAppData.ods_major, "ALL")><!--- ODS major --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<said_major\>\>", getAppData.said_major, "ALL")><!--- Said major --->
		<cfset activeMsg = reReplace(activeMsg, "\<\<confirmation_link\>\>", ccl, "ALL")><!--- Confirm link --->
		<cfset activeMsg = reReplace(activeMsg, "\n", "</p><p>", "ALL")><!--- Linebreaks --->

		<!--- Start working on custom links (Loop 10x or so to handle up to 10 links?) --->
		<cfloop from="1" to ="10" index="i">
			<cfset cLink = REmatch("\<\<\(\S*\)\>\>", activeMsg)><!--- Get the shortcode --->
			<cfparam name="cLink[1]" default="">

			<!--- Only continue if there is a custom link to maninpulate (otherwise it will error) --->
			<cfif cLink[1] NEQ "">

				<cfset cLink = cLink[1]><!--- Extract from its automatically created array --->
				<cfset cLink = left(cLink, len(cLink)-3)><!--- Remove the last 3 chars [")>>"] --->
				<cfset cLink = right(cLink, len(cLink)-3)><!--- Remove the first 3 chars ["<<("] --->
				<cfset cLinkURL = trim(listGetAt(cLink, 1))><!--- Get the URL --->
				<cfset cLinkText = trim(listGetAt(cLink, 2))><!--- Get the link text --->
				<cfset cLinkFull = '<a href="' & cLinkURL & '">' & #cLinkText# & '</a>'><!--- Build full link --->

				<cfset activeMsg = reReplace(activeMsg, "\<\<\(\S*\)\>\>", cLinkFull, "one")><!--- Finally, swap out with the actual/live link --->

			</cfif>
		</cfloop>

		<!--- Mail --->
		<cfmail
			from="#application.emailFrom#"
			to="#getAppData.email#"
			replyto="#application.emailReplyTo#"
			subject="#getMsg.msg_subject#"
			type="html"
		>
		#activeMsg#
		</cfmail>
	</cffunction>
</cfcomponent>