<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="adminNotes" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="ename" type="string" required="true" default="">
		<cfargument name="admin_notes" type="string" default="">

		<!--- Update Note Fields --->
		<cfquery name="saveAdminNotes">
			UPDATE application_form
			SET
				admin_notes = <cfqueryparam value="#arguments.admin_notes#" cfsqltype="cf_sql_longvarchar">,
				latest_admin_notes_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#',
				latest_admin_notes_by = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar" maxlength="30">
			WHERE ename = <cfqueryparam value="#arguments.ename#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Notes: Updated notes on application." impacted_user="#arguments.ename#">

	</cffunction>



	<cffunction name="interviewNotes" access="remote" output="false">

	</cffunction>

</cfcomponent>