<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="loadMore" access="remote" returntype="array" returnformat="JSON">

		<!--- Arguments --->
		<cfargument name="minID" type="numeric" required="true" default="0">
		<cfargument name="fAction" type="string" required="false" default="">
		<cfargument name="fEname" type="string" required="false" default="">
		<cfargument name="fUL" type="string" required="false" default="">
		<cfargument name="fIP" type="string" required="false" default="">
		<cfargument name="fIU" type="string" required="false" default="">
		<cfargument name="fAD" type="string" required="false" default="">

		<!--- Select filtered --->
		<cfquery name="moreResults">
			SELECT AL.*, AF.csuid as tmpcsuid
			FROM
				access_log as AL
				LEFT JOIN
				application_form as AF
				ON
				AL.ename = AF.ename
			WHERE
				1 = 1
				<cfif minID NEQ 0>
				AND AL.id < <cfqueryparam value="#arguments.minID#" cfsqltype="cf_sql_numeric">
				</cfif>
				<cfif fAction NEQ "">
				AND AL.action = <cfqueryparam value="#arguments.fAction#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif fEname NEQ "">
				AND AL.ename LIKE <cfqueryparam value="%#arguments.fEname#%" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif fUL NEQ "">
				AND AL.session_user_level = <cfqueryparam value="#arguments.fUL#" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif fIP NEQ "">
				AND AL.ip_address LIKE <cfqueryparam value="%#arguments.fIP#%" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif fIU NEQ "">
				AND AL.impacted_user LIKE <cfqueryparam value="%#arguments.fIU#%" cfsqltype="cf_sql_varchar">
				</cfif>
				<cfif fAD NEQ "">
				AND AL.access_dt LIKE <cfqueryparam value="%#arguments.fAD#%" cfsqltype="cf_sql_varchar">
				</cfif>
			ORDER BY id DESC
			LIMIT 10
		</cfquery>

		<cfset results = []>

		<cfloop query="moreResults">
			<cfset tmpResult = {
				lastID = #moreResults.id#,
				fAction = #moreResults.action#,
				fEname = #moreResults.ename#,
				fCSUID = #moreResults.tmpcsuid#,
				fUL = #moreResults.session_user_level#,
				fIP = #moreResults.ip_address#,
				fIU = #moreResults.impacted_user#,
				fAD = #dateFormat(moreResults.access_dt, "mm/dd/yyyy")#,
				fAT = #timeFormat(moreResults.access_dt, "h:mm tt")#
			}>
			<cfset arrayAppend(results, tmpResult)>
		</cfloop>

		<cfreturn results>

	</cffunction>


	<cffunction name="search" access="remote" returntype="array" returnformat="JSON">

		<!--- Arguments --->
		<cfargument name="filter" type="string" required="false" default="">

		<!--- Query results --->
		<cfquery name="searchResults">
			SELECT *
			FROM access_log
			WHERE
				action LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				ename LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				impacted_user LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				ip_address LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR


				csuid LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				first_name LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				last_name LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar">
		</cfquery>

	</cffunction>

</cfcomponent>