<cfparam name="attributes.current" default="">

<cfoutput>
<div id="leftSide">
	<!--- Put this as a module within admin/mods/ --->
	<div id="leftMenu">
		<ul class="menuSection">
			<li class="headerLI">Pending Applications</li>
			<li><a href="#application.urlpath_admin#?filter=In-Progress" data-menuID="In-Progress">In-Progress <i class="far fa-toggle-off" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#?filter=Completed" data-menuID="Completed">Completed <i class="far fa-toggle-on" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#?filter=Waitlisted" data-menuID="Waitlisted">Waitlisted <i class="far fa-list-ol" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#?filter=Held" data-menuID="Held">Held <i class="far fa-pause-circle" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#?filter=Declined" data-menuID="Declined">Declined <i class="far fa-user-times"></i></a></li>
		</ul>
		<ul class="menuSection">
			<li class="headerLI">Roster by Clusters</li>
			<li><a href="#application.urlpath_admin#?filter=Accepted" data-menuID="Accepted">All Clusters</a></li>
			<li><a href="#application.urlpath_admin#?filtertype=cluster&filter=General" data-menuID="General">General</a></li>
			<li><a href="#application.urlpath_admin#?filtertype=cluster&filter=Returners" data-menuID="Returners">Returners</a></li>
			<li><a href="#application.urlpath_admin#?filtertype=cluster&filter=SOS" data-menuID="SOS">Science Outreach</a></li>
			<li><a href="#application.urlpath_admin#?filtertype=cluster&filter=Sustainability" data-menuID="Sustainability">Sustainability</a></li>
			<li><a href="#application.urlpath_admin#?filtertype=cluster&filter=WINS" data-menuID="WINS">WINS</a></li>
		</ul>
		<ul class="menuSection">
			<li class="headerLI">Tools</li>
			<li><a href="#application.urlpath_admin#tools/finder.cfm"<cfif attributes.current EQ "finder"> class="current"</cfif>>Application Finder <i class="far fa-search" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#tools/messages.cfm"<cfif attributes.current EQ "messages"> class="current"</cfif>>Message Defaults <i class="far fa-envelope" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#tools/data.cfm"<cfif attributes.current EQ "data"> class="current"</cfif>>Data Utilities <i class="far fa-hdd" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#tools/query.cfm"<cfif attributes.current EQ "query"> class="current"</cfif>>Query Builder <i class="far fa-bolt" aria-hidden="true"></i></a></li>
			<li><a href="#application.urlpath_admin#tools/stats.cfm"<cfif attributes.current EQ "stats"> class="current"</cfif>>Statistics <i class="far fa-chart-bar" aria-hidden="true"></i></a></li>
			<cfif session.permission_level GTE 9><li><a href="#application.urlpath_admin#tools/actions.cfm"<cfif attributes.current EQ "actions"> class="current"</cfif>>Action Log <i class="far fa-book" aria-hidden="true"></i></a></li></cfif>
		</ul>
	</div>
</div>
</cfoutput>