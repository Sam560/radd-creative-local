<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="dlShort" access="remote" output="false">

		<!--- Arguments --->

		<!--- Get Short Data --->
		<cfquery name="getShort">
			SELECT csuid, ename, last_name, first_name, email, app_type, app_status, cluster, confirmation_decision, ods_major, college, student_year, pi_ethnicity, pi_gender, ods_asset
			FROM application_form
		</cfquery>

		<!--- Create a new spreadsheet --->
		<cfset objSpreadsheet = SpreadsheetNew()>

		<!--- Create spreadsheet vars --->
		<cfset excelName = "CNSLC_Data_Short_" & #dateFormat(NOW(), "yyyy-mm-dd")# & ".xls">

		<!--- Create and format the header row --->
		<cfset spreadsheetAddRow( objSpreadsheet, arrayToList( getShort.getColumnNames() ) )>
		<cfset spreadsheetFormatRow( objSpreadsheet, {bold=TRUE, alignment="center"}, 1 )>

		<cfset spreadSheetAddRows( objSpreadsheet, getShort )>

		<cfheader name="Content-Disposition" value="attachment; filename=#excelName#"> 
		<cfcontent type="application/vnd.ms-excel" variable="#SpreadsheetReadBinary( objSpreadsheet )#">

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Data download (short)">

	</cffunction>



	<cffunction name="dlLong" access="remote" output="false">

		<!--- Arguments --->

		<!--- Get Short Data --->
		<cfquery name="getShort">
			SELECT *
			FROM application_form
		</cfquery>

		<!--- Create a new spreadsheet --->
		<cfset objSpreadsheet = SpreadsheetNew()>

		<!--- Create spreadsheet vars --->
		<cfset excelName = "CNSLC_Data_Full_" & #dateFormat(NOW(), "yyyy-mm-dd")# & ".xls">

		<!--- Create and format the header row --->
		<cfset spreadsheetAddRow( objSpreadsheet, arrayToList( getShort.getColumnNames() ) )>
		<cfset spreadsheetFormatRow( objSpreadsheet, {bold=TRUE, alignment="center"}, 1 )>

		<cfset spreadSheetAddRows( objSpreadsheet, getShort )>

		<cfheader name="Content-Disposition" value="attachment; filename=#excelName#"> 
		<cfcontent type="application/vnd.ms-excel" variable="#SpreadsheetReadBinary( objSpreadsheet )#">

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Data download (full)" impacted_user="#arguments.ename#">

	</cffunction>

</cfcomponent>