<cfoutput>
<h1 id="mainH1" style="float: left;">#application.h1_admin#</h1>
<a href="#application.urlpath#mods/logout.cfm" class="btn btn-outline-secondary btn-sm adminLogoutBtn"><i class="far fa-sign-out" aria-hidden="true"></i> Logout</a>
<div style="clear: both;"></div>
</cfoutput>

<script>
$(document).ready(function(){
	/* Notify Admin of Timeout */
	var myTimer = (<cfoutput>#application.sessTimeout#</cfoutput> - 120000);
	setTimeout(function(){
			alert('Session has timed out. Please sign back in.');
			window.location.replace('<cfoutput>#application.urlpath#</cfoutput>?status=noSession');
		},
		myTimer
	);
});
</script>