<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="filter" access="remote" returnType="array" returnformat="JSON">

		<!--- Arguments --->
		<cfargument name="filter" type="string" required="true" default="">

		<!--- Select filteredge --->
		<cfquery name="filteredResults">
			SELECT
				csuid, CONCAT(last_name, ', ', first_name) AS full_name,
				app_status,
				cluster,
				latest_decision_dt,
				confirmation_decision,
				confirmation_decision_dt
			FROM application_form
			WHERE
				csuid LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				first_name LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				last_name LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar"> OR
				ename LIKE <cfqueryparam value="%#arguments.filter#%" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Log action --->
		<!--- <cfmodule template="#application.urlpath_log#" action="Admin Action: Message default updated (#arguments.msg#)"> --->

		<cfset results = []>

		<cfloop query="filteredResults">
			<cfset tmpResult = {
				name = '#full_name#',
				csuid = '#csuid#'
			}>
			<cfset arrayAppend(results, tmpResult)>
		</cfloop>

		<cfreturn results>

	</cffunction>

</cfcomponent>