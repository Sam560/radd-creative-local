<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

	<cffunction name="saveMsg" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="msg" type="string" required="false" default="">
		<cfargument name="msgtext" type="string" required="false" default="">
		<cfargument name="msgsubject" type="string" required="false" default="">

		<!--- Update Message --->
		<cfquery name="saveMsgs">
			UPDATE ref_messages
			SET
				msg_subject = <cfqueryparam value="#arguments.msgsubject#" cfsqltype="cf_sql_longvarchar">,
				msg_text = <cfqueryparam value="#arguments.msgtext#" cfsqltype="cf_sql_longvarchar">
			WHERE msg = <cfqueryparam value="#arguments.msg#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<!--- Log action --->
		<cfmodule template="#application.urlpath_log#" action="Admin Action: Message default updated (#arguments.msg#)">

	</cffunction>

</cfcomponent>