<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfoutput>

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="data">

<!-- Modal -->
<div class="modal fade" id="syncingModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="syncingModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="syncingModalLabel">Syncing...</h3>
			</div>
			<div class="modal-body">
				<div class="progress">
					<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="successModalLabel">Success!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<i class="far fa-check-circle fa-2x" aria-hidden="true" style="color: ##5B9D78; padding: 0 10px 0 10px; vertical-align: middle"></i><span style="font-size: 1.1em;">ODS sync completed successfully.</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="rightSide">
	<h2>Data Utilities</h2>
	<h3 style="margin-top: 0;">Sync System's ODS Data</h3>
	<p style="padding-bottom: 0;">Sync <span style="font-weight: bold;">all</span> applicants' ODS data.</p>

	<div style="margin: 10px 0 0 10px;">
		<button role="button" id="syncOds" class="dataOptBtns btn btn-sm btn-primary">
			<i class="fas fa-sync"></i>
			<span style="margin-left: 5px; color: ##fff;">Sync ODS Data</span>
		</button>
	</div>

	<h3 style="margin-top: 40px;">Download Data</h3>
	<p style="padding-bottom: 0;">Use these tools to download system data in Excel spreadsheet format.</p>

	<div style="margin: 10px 0 0 10px;">
		<a href="../mods/downloads.cfc?method=dlShort" role="button" id="dlShort" class="dataOptBtns btn btn-sm btn-primary" style="color: ##fff;"><i class="far fa-download" aria-hidden="true"></i> Brief Data</a>
		<a href="../mods/downloads.cfc?method=dlLong" role="button" id="dlFull" class="dataOptBtns btn btn-sm btn-primary" style="color: ##fff; margin-left: 20px;"><i class="far fa-download" aria-hidden="true"></i> Full Data</a>
	</div>

</div>

</cfoutput>

<div style="clear: both;"></div>

<script>
$(function(){

$('#syncOds').click(function(){
	// Open modal
	$('#syncingModal').modal('show');

	// AJAX call
	$.ajax({
		type: 'post',
		url: '../mods/adminActions.cfc?method=updateAllODSInfo'
	}).done(function(response){
		// Hide modal
		$('#syncingModal').modal('hide');
		// Show success modal
		$('#successModal').modal('show');
	}).fail(function(response){
		// Hide modal
		$('#syncingModal').modal('hide');
		// Alert user of error
		alert('An error has occurred. (Code 89)');
	});
});

});
</script>

<cfinclude template="#application.urlpath#mods/tech_issues.cfm">