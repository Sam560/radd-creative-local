<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfoutput>

<cfquery name="getActionTypes">
	SELECT action, count(action) AS actionFreq
	FROM access_log
	GROUP BY action
	ORDER BY action
</cfquery>
<cfquery name="getAllRecent">
	SELECT access_log.*, application_form.csuid as csu_id
	FROM
		access_log LEFT JOIN application_form ON access_log.ename = application_form.ename
	ORDER BY id DESC
	LIMIT 10
</cfquery>

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="actions">

<div id="rightSide">
	<h2 style="margin-bottom: 10px;">Action Log</h2>

	<!--- All actions --->
	<table id="mainTable" class="tablesorter tsArrows tsArrowsBothFade unifyInputHeights">
		<thead style="cursor: default;">
			<tr class="avoid-sort">
				<th>Action</th>
				<th>ename</th>
				<th>Level</th>
				<th>IP</th>
				<th>Impacted User</th>
				<th>Access Date</th>
			</tr>
		</thead>
		<tfoot>
			<tr id="loader" class="tablesorter-childRow">
				<td colspan="6" class="loadMore">
					<i class="far fa-chevron-circle-down" style="margin-right: 10px;" aria-hidden="true"></i> Load More
				</td>
			</tr>
		</tfoot>
		<tbody id="filterTBody">
			<tr class="filter-row avoid-sort">
				<td>
					<select id="action" name="action">
						<option value=""></option>
						<cfloop query="getActionTypes">
							<option value="#action#">#action#</option>
						</cfloop>
					</select>
				</td>
				<td>
					<input type="text" id="ename" name="ename">
				</td>
				<td>
					<select id="session_user_level" name="session_user_level">
						<option value=""></option>
						<option value="admin">admin</option>
						<option value="applicant">applicant</option>
					</select>
				</td>
				<td>
					<input type="text" id="ip_address" name="ip_address">
				</td>
				<td>
					<input type="text" id="impacted_user" name="impacted_user">
				</td>
				<td>
					<input type="text" id="access_dt" name="access_dt">
				</td>
			</tr>
		</tbody>
		<tbody id="mainTBody"></tbody>
	</table>


	<!--- Search actions --->
	<h3 style="margin-top: 30px;">Search All</h3>
	<input type="search" id="searchActions" name="searchActions" class="form-control form-control-sm" placeholder="Name, ename, IP address, action, etc." style="margin-left: 10px; width: 400px;">

</div>

</cfoutput>

<div style="clear: both;"></div>
<cfinclude template="#application.urlpath#mods/tech_issues.cfm">

<script>
$(function(){
	// Table sorter
	var mTable = $('#mainTable');

	mTable.tablesorter({
		cssInfoBlock: 'avoid-sort',
		widgets: ['zebra', 'saveSort']
	});

	// Tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// Function to clear table
	function clrTable(tableid){
		$(tableid).empty();
	}

	// Enable/disable "load more"
	function disableMore(){
		$('.loadMore').css({'opacity':'0.3','cursor':'not-allowed'});
	}
	function enableMore(){
		$('.loadMore').css({'opacity':'1','cursor':'pointer'});
	}

	// Var to keep track of the ID to "continue" results from
	var continueFrom = 0;

	// Build filter object to send with AJAX
	var jsonObj = {};
	function buildJSON(){
		jsonObj = {};
		jsonObj.minID = continueFrom;
		jsonObj.fAction = $('#action').val();
		jsonObj.fEname = $('#ename').val();
		jsonObj.fUL = $('#session_user_level').val();
		jsonObj.fIP = $('#ip_address').val();
		jsonObj.fIU = $('#impacted_user').val();
		jsonObj.fAD = $('#access_dt').val();
	}

	// Function to do ajax
	function doAJAX(){
		// Build object to send
		buildJSON();

		// Send object
		$.ajax({
			type: 'post',
			url: '../mods/loadActions.cfc?method=loadMore',
			data: jsonObj,
			datatype: 'json'
		}).done(function(response){

			// Parse JSON
			var results = JSON.parse(response);

			// Loop through results
			$.each(results, function(k, v){
				// Build row
				var tr = jQuery('<tr />');
				tr.attr('data-rowid', results[k].LASTID);

				// Build columns
				var action = jQuery('<td />');
				action.text(results[k].FACTION);
				action.appendTo(tr);

				var ename = jQuery('<td />');
				ename.html('<a href="/students/cnslc/admin/viewApp.cfm?csuid=' + results[k].FCSUID + '" style="text-decoration: underline;">' + results[k].FENAME) + '</a>';
				ename.appendTo(tr);

				var ul = jQuery('<td />');
				ul.text(results[k].FUL);
				ul.appendTo(tr);

				var ip = jQuery('<td />');
				ip.text(results[k].FIP);
				ip.css({'text-align':'right','padding-right':'10px'});
				ip.appendTo(tr);

				var iu = jQuery('<td />');
				iu.text(results[k].FIU);
				iu.appendTo(tr);

				var ad = jQuery('<td />');
				var adString = results[k].FAD + '<br><span style="color: #444; font-size: 0.9em;">' + results[k].FAT + '</span>';
				ad.css({'text-align':'right','padding-right':'10px'});
				ad.html(adString);
				ad.appendTo(tr);

				// Append row
				tr.hide();
				tr.appendTo('#mainTBody').show('fast');
			});

			// If response not empty
			if(results){
				// Update the ID to continue AJAX queries from
				continueFrom = results[results.length-1].LASTID;

				if(results.length < 10){
					disableMore();
				}
			} else{
				disableMore();
			}

			// Refresh Tablesorter's zebra
			$('#mainTable').trigger('update');

			// If response was less than the set 10, we know we are done with records... check use case of if it is exactly
		}).fail(function(response){
			alert('Error.');
		});
	}

	// Load table initially
	buildJSON();
	doAJAX();

	// Reset table for new Ajax
	function readyTable(){
		clrTable('#mainTBody'); // Clear current table results
		continueFrom = 0; // Reset continue from ID
		enableMore();
		doAJAX();
	}

	// Events to trigger ajax
	$('.filter-row select').change(function(){
		readyTable();
	});
	var timeout;
	$('#mainTable').on('keyup', 'input', function(){
		window.clearTimeout(timeout);
		timeout = window.setTimeout(function(){
			readyTable();
		},500);
	});

	// Load more
	$('#loader').click(function(){
		buildJSON();
		doAJAX();
	});
});
</script>