<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfoutput>

<cfquery name="getGender">
	SELECT pi_gender, cluster
	FROM application_form
	WHERE app_status = 'Accepted'
	ORDER BY pi_gender ASC
</cfquery>
	<cfquery name="getGenderAll" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		GROUP BY pi_gender
	</cfquery>
	<cfquery name="getGenderGen" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		WHERE cluster = 'General'
		GROUP BY pi_gender
	</cfquery>
	<cfquery name="getGenderRet" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		WHERE cluster = 'Returners'
		GROUP BY pi_gender
	</cfquery>
	<cfquery name="getGenderSOS" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		WHERE cluster = 'Science Outreach'
		GROUP BY pi_gender
	</cfquery>
	<cfquery name="getGenderSus" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		WHERE cluster = 'Sustainability'
		GROUP BY pi_gender
	</cfquery>
	<cfquery name="getGenderWIN" dbtype="query">
		SELECT pi_gender, count(pi_gender) as genderCnt
		FROM getGender
		WHERE cluster = 'WINS'
		GROUP BY pi_gender
	</cfquery>

<cfquery name="getState">
	SELECT pi_home_state, count(pi_home_state) as stateCnt
	FROM application_form
	WHERE app_status = 'Accepted'
	GROUP BY pi_home_state
	ORDER BY pi_home_state
</cfquery>

<cfquery name="getAppStarts">
	SELECT date(start_dt) as uniqDate, count(*) as dateFreq
	FROM application_form
	GROUP BY uniqDate
	ORDER BY uniqDate
</cfquery>






<cfquery name="getFirstGen">
	SELECT ods_first_gen
	FROM application_form
</cfquery>

<cfquery name="getAsset">
	SELECT ods_asset
	FROM application_form
</cfquery>

<cfquery name="getEthnicity">
	SELECT pi_ethnicity
	FROM application_form
</cfquery>

<cfquery name="getNation">
	SELECT pi_home_nation
	FROM application_form
</cfquery>

<cfquery name="getAge">
	SELECT pi_age
	FROM application_form
</cfquery>

<cfquery name="getYear">
	SELECT student_year
	FROM application_form
</cfquery>

<cfquery name="getODSMajor">
	SELECT ods_major
	FROM application_form
</cfquery>

<!--- <cfquery name="getHalls">
	SELECT res_hall
	FROM application_for
mhig<h/cfquery> --->

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="stats">
<style>
.highcharts-credits{
	display: none;
}
</style>


<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/maps/modules/map.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/us/us-all.js"></script>


<div id="rightSide" style="width: 950px;">
	<div class="adminWarning">
		<p><i class="far fa-exclamation-circle fa-lg" aria-hidden="true" style="padding-right: 5px;"></i>This page is under development. Do not use. If you have questions, please contact CNS IT.</p>
	</div>
	<h2>Statistics</h2>
	<h3>Accepted Applicants</h3>

	<!---
		Priorities:
			1) Bar chart for each cluster with gender and majors
			2) Outreach for all clusters
			3) Ethnicity for all clusters
			4) Ethnicity gender all cluster
	--->

	<div class="row">
		<div id="allGender" class="col-10"></div>
		<div class="col-2">
			<ul id="genderOptions" class="list-group statListGroup">
				<button type="button" class="list-group-item list-group-item-action active" data-sid="genderAll">All Accepted</button>
				<button type="button" class="list-group-item list-group-item-action" data-sid="genderGen">General</button>
				<button type="button" class="list-group-item list-group-item-action" data-sid="genderRet">Returners</button>
				<button type="button" class="list-group-item list-group-item-action" data-sid="genderSOS">Science Outreach</button>
				<button type="button" class="list-group-item list-group-item-action" data-sid="genderSus">Sustainability</button>
				<button type="button" class="list-group-item list-group-item-action" data-sid="genderWIN">WINs</button>
			</ul>
		</div>
	</div>

	<div class="row" style="margin-top: 40px;">
		<div id="allState" class="col-6"></div>
		<div id="stateMap" class="col-6"></div>
	</div>

	<div class="row" style="margin-top: 40px;">
		<h3>Application Rates</h3>
		<div id="newAppRates" class="col-11"></div>
	</div>

	
	<!--- <cfquery name="testLC" datasource="csulc">
		SELECT *
		FROM Communities
	</cfquery>
	<cfdump var="#testLC#"> --->
</div>

</cfoutput>

<div style="clear: both;"></div>


<script>
$(function () { 
	// Gender
	<cfoutput>
	var genderXAxis = [<cfloop query="getGenderAll">'#pi_gender#',</cfloop> 'D', 'R'];
	var genderAll = [<cfloop query="getGenderAll">{name: '#pi_gender#',y: #genderCnt#},</cfloop>];
	var genderGen = [<cfloop query="getGenderGen">{name: '#pi_gender#',y: #genderCnt#},</cfloop> {name: 'D',y: 12}, {name: 'R',y: 7}];
	var genderRet = [<cfloop query="getGenderRet">{name: '#pi_gender#',y: #genderCnt#},</cfloop>];
	var genderSOS = [<cfloop query="getGenderSOS">{name: '#pi_gender#',y: #genderCnt#},</cfloop> {name: 'B',y: 10}];
	var genderSus = [<cfloop query="getGenderSus">{name: '#pi_gender#',y: #genderCnt#},</cfloop>];
	var genderWIN = [<cfloop query="getGenderWIN">{name: '#pi_gender#',y: #genderCnt#},</cfloop> {name: 'C',y: 8}];
	</cfoutput>

    var genderBarChart = Highcharts.chart('allGender', {
        chart: { type: 'bar' },
        title: { text: 'Gender' },
        xAxis: { 
        	categories: genderXAxis,
        	title: {
        		text: 'Gender'
        	}
        },
        yAxis: { title: { text: '# of Applicants' } },
        series: [
        	{
        		showInLegend: false,
        		name: 'Count',
        		data: genderAll
        	}
        ]
    });
    console.log( 'Initial genderAll:' );
    console.log( genderAll );
    console.log( '\nInitial genderGen: ' );
    console.log( genderGen );
    $('#genderOptions button').click(function(){
    	$('#genderOptions button').removeClass('active'); // Remove prev actives
    	$(this).addClass('active'); // Add active to this one
    	genderBarChart.series[0].setData( eval( $(this).data('sid') ) );
    	console.log( '\nCurrent ' + $(this).data('sid') + ': ' );
    	console.log( eval( $(this).data('sid') ) );
    });





    // State
    var myChart = Highcharts.chart('allState', {
        chart: {
        	renderTo: 'container',
            type: 'pie'
        },
        title: {
            text: 'Home States'
        },
        xAxis: {
            categories: ['States']
        },
        yAxis: {
            title: {
                text: 'States'
            }
        },
        series: [{
        	name: 'States',
        	colorByPoint: true,
        	data:[
	        <cfoutput query="getState">
	        	{
		        	name: '#pi_home_state#',
		        	y: #stateCnt#
		        },
	        </cfoutput>
        	]
        }]
    });


    // US Map
    var statedata = [
    	<cfoutput query="getState">
    	{
    		name: '#pi_home_state#',
    		value: #stateCnt#
    	},
    	</cfoutput>
    ];
    // Create the chart
	Highcharts.mapChart('stateMap', {
	    chart: {
	        map: 'countries/us/us-all'
	    },
	    title: {
	        text: 'Home States'
	    },
	    mapNavigation: {
	        enabled: true,
	        buttonOptions: {
	            verticalAlign: 'bottom'
	        }
	    },
	    colorAxis: {
	        min: 0
	    },
	    series: [{
	        data: statedata,
	        joinBy: 'name',
	        name: 'Random data',
	        states: {
	            hover: {
	                color: '##70c2ec'
	            }
	        },
	        dataLabels: {
	            enabled: true,
	            format: '{point.name}'
	        }
	    },
	    {
            name: 'Separators',
            type: 'mapline',
            data: Highcharts.geojson(Highcharts.maps['countries/us/us-all'], 'mapline'),
            color: 'silver',
            showInLegend: false,
            enableMouseTracking: false
        }]
	});







	// Application rates
	var appDates = [<cfoutput query="getAppStarts">['#dateFormat(uniqDate, "mmm d, yy")#', #dateFreq#],</cfoutput>];
    Highcharts.chart('newAppRates', {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: 'New Applicants per Day'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
            type: 'date'
        },
        yAxis: {
            title: {
                text: 'Number of new applicants'
            },
            min: 0
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },

        series: [{
            type: 'area',
            name: 'New Applications per Day',
            data: appDates
        }]
    });
});
</script>

<cfinclude template="#application.urlpath#mods/tech_issues.cfm">