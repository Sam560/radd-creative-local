<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfquery name="getMsgs">
	SELECT *
	FROM ref_messages
</cfquery>

<cfoutput>

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="messages">

<div id="rightSide" style="width: 990px;">
	<h2>Message Defaults</h2>

	<!--- Shortcode Cheat Sheet --->
	<table class="quickInfo" style="margin: 20px auto 20px auto;">
		<thead>
			<tr>
				<th colspan="4">Email Shortcode Legend <span style="font-size: .8em;">(Not for Subject Fields)</span></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>First Name</th>
				<td>&lt;&lt;first_name&gt;&gt;</td>
				<th style="padding-left: 10px;">Last Name</th>
				<td>&lt;&lt;last_name&gt;&gt;</td>
			</tr>
			<tr>
				<th>ODS Major</th>
				<td>&lt;&lt;ods_major&gt;&gt;</td>
				<th>Said Major</th>
				<td>&lt;&lt;said_major&gt;&gt;</td>
			</tr>
			<tr>
				<th colspan="1" style="padding-left: 10px;">Confirmation Link</th>
				<td colspan="3">&lt;&lt;confirmation_link&gt;&gt;</td>
			</tr>
			<tr>
				<th colspan="1" style="vertical-align: top;">Custom Links</th>
				<td colspan="3">&lt;&lt;(pageUrlGoesHere,linkTextGoesHere)&gt;&gt;<br><span style="font-size: .8em;">(Be sure to include the "http" or "https")</td>
			</tr>
		</tbody>
	</table>
	

	<form id="msgDefaults" name="msgDefaults">

		<cfset count = 1>
		<cfloop query="getMsgs">
			<h3 style="font-size: 1em;">#getMsgs.msg_name#</h3>

			<input type="text" name="msg_subject" id="#getMsgs.msg#_msg_subject" value="#getMsgs.msg_subject#" style="margin: 0 0 5px 10px; width: 850px;" placeholder="Subject">

			<textarea name="msg_text" id="#getMsgs.msg#_msg_text" class="adminMessages">#getMsgs.msg_text#</textarea>

			<div class="msgOptionsDiv">

				<button type="button" class="btn btn-primary btn-sm sendTest" data-testmsg="#getMsgs.msg#" data-testmsgsubject="#getMsgs.msg#_msg_subject" data-testmsgtext="#getMsgs.msg#_msg_text">
					<i class="far fa-envelope" aria-hidden="true" style="margin-right: 5px;"></i> Send Test Email to Yourself
				</button>

				<button type="button" class="btn btn-success btn-sm saveMsg" data-testmsg="#getMsgs.msg#" data-testmsgsubject="#getMsgs.msg#_msg_subject" data-testmsgtext="#getMsgs.msg#_msg_text">
					<i class="far fa-save" aria-hidden="true" style="margin-right: 5px;"></i> Save Message
				</button>

			</div>

			<cfif count LT getMsgs.recordCount>
				<div style="width: 90%; border-bottom: 1px solid ##ddd; margin: 50px auto 40px auto;"></div>
			</cfif>

			<cfset count++>
		</cfloop>

	</form>

</div>

</cfoutput>

<div style="clear: both;"></div>
<cfinclude template="#application.urlpath#mods/tech_issues.cfm">

<script>
$(document).ready(function(){

/* -- Auto Height Textareas to fit Content -- */
$("textarea").each(function(){$(this).height( $(this)[0].scrollHeight )});

$('.saveMsg').click(function(){
	var emailObj = {};

	emailObj.msg = $(this).data('testmsg');
	emailObj.msgtext = $('#' + $(this).data('testmsgtext')).val();
	emailObj.msgsubject = $('#' + $(this).data('testmsgsubject')).val();

	$.ajax({
		type: 'post',
		url: '../mods/saveMessage.cfc?method=saveMsg',
		data: emailObj,
		datatype: 'json',
		success: function(){alert('Message saved.');},
		error: function(){alert('An error has occurred. (Code 105)');}
	});
});

$('.sendTest').click(function(){
	var emailObj = {};

	emailObj.msg = $(this).data('testmsg');
	emailObj.msgtext = $('#' + $(this).data('testmsgtext')).val();
	emailObj.msgsubject = $('#' + $(this).data('testmsgsubject')).val();

	$.ajax({
		type: 'post',
		url: '../mods/sendTestMessage.cfc?method=sendEmail',
		data: emailObj,
		datatype: 'json',
		success: function(){alert('Test email sent.');},
		error: function(){alert('An error has occurred. (Code 122)');}
	});
});

});
</script>