<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfoutput>

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="finder">

<div id="rightSide">
	<h2>Application Finder</h2>
	
	<div style="margin-left: 10px;">

		<p style="margin: 10px 0 0 0; padding: 0; font-size: 1em;">Quickly find application by name, CSUID, or ename.</p>
		<br>
		<input id="appFinder" type="search" placeholder="Search by name, CSUID, etc." class="form-control form-control-sm" style="margin-bottom: 10px;">

		<h3 style="padding: 20px 0 0 0;">Results</h3>
		<ul id="resultList" class="fa-ul" style="margin: 0 0 0 20px; padding: 10px 0 0 0;">
		</ul>

	</div>
</div>

</cfoutput>

<div style="clear: both;"></div>
<cfinclude template="#application.urlpath#mods/tech_issues.cfm">

<script>
$(document).ready(function(){

var timeout;
$('body').on('keyup', '#appFinder', function(){
	window.clearTimeout(timeout);
	filter = $(this).val();

	if(filter){
		timeout = window.setTimeout(function(){
			$.ajax({
				type: 'post',
				url: '../mods/appFinder.cfc?method=filter',
				data: {'filter' : filter},
				datatype: 'json'
			}).done(function(response){
				// Clear previous
				$('#resultList').empty();

				// Parse JSON
				var results = JSON.parse(response);

				// Loop through results
				$.each(results, function(k, v){
					// Build Link
					var a = jQuery('<a />');
					var url = "../viewApp.cfm?csuid=" + results[k].CSUID;
					a.attr('href', url);
					a.text(results[k].NAME);

					// Build li
					var li = jQuery('<li />');
					li.html(a);

					// Append
					li.appendTo('#resultList');
				});
			}).fail(function(response){
				alert('An error has occurred. (Code 67)');
			});
		},500);
	} else{
		$('#resultList').empty();
	}
});

});

</script>