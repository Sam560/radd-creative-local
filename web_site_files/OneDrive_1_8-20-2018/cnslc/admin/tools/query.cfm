<cfmodule template="#application.urlpath_security#" reqAdmin="#application.enforceAdmin#">

<cfoutput>

<!--- Queries to get certain options --->
<cfquery name="getAppTypes">
	SELECT app_type
	FROM application_form
	GROUP BY app_type
</cfquery>
<cfquery name="getAppStatuses">
	SELECT app_status
	FROM application_form
	GROUP BY app_status
</cfquery>
<cfquery name="getClusters">
	SELECT short_name
	FROM ref_clusters
	ORDER BY short_name
</cfquery>
<cfquery name="getGenders">
	SELECT pi_gender
	FROM application_form
	GROUP BY pi_gender
</cfquery>
<cfquery name="getODSMajors">
	SELECT ods_major
	FROM application_form
	GROUP BY ods_major
	ORDER BY ods_major
</cfquery>
	<cfset ourMajorsArr = ["Biochemistry", "Biological Science", "Chemistry", "Computer Science", "Mathematics", "Natural Sciences", "Neuroscience", "Physics", "Psychology", "Statistics", "Zoology"]>
<cfquery name="getSaidMajors">
	SELECT said_major
	FROM application_form
	WHERE said_major <> ''
	GROUP BY said_major
	ORDER BY said_major
</cfquery>
<cfquery name="getYears">
	SELECT student_year
	FROM application_form
	WHERE student_year <> ''
	GROUP BY student_year
	ORDER BY
		case
			when student_year = 'Freshman' then 1
			when student_year = 'Sophomore' then 2
			when student_year = 'Junior' then 3
			when student_year = 'Senior' then 4
			else 5
		end
</cfquery>
<cfquery name="getConfirmations">
	SELECT confirmation_decision
	FROM application_form
	WHERE confirmation_decision <> '0'
	GROUP BY confirmation_decision
	ORDER BY confirmation_decision
</cfquery>
<cfquery name="getEthnicities">
	SELECT pi_ethnicity
	FROM application_form
	GROUP BY pi_ethnicity
</cfquery>
	<cfset ethVL = valueList(getEthnicities.pi_ethnicity)>
	<cfset unique_ethVL = "">
	<cfloop list="#ethVL#" index="i">
		<cfif NOT listFind(unique_ethVL, i)>
			<cfset unique_ethVL = listAppend(unique_ethVL, i)>
		</cfif>
	</cfloop>
<cfquery name="getNations">
	SELECT pi_home_nation
	FROM application_form
	GROUP BY pi_home_nation
	ORDER BY pi_home_nation
</cfquery>
<cfquery name="getStates">
	SELECT pi_home_state
	FROM application_form
	GROUP BY pi_home_state
	ORDER BY pi_home_state
</cfquery>

<cfinclude template="../mods/_pageHeader.cfm">
<cfmodule template="#application.urlpath_admin#mods/menu.cfm" current="query">

<div id="rightSide">

<h2>Query Builder</h2>

<h3 style="float: left;">Filters</h3>
<button id="clearAll" type="button" style="float: right; font-size: .8em;"><i class="far fa-eraser" aria-hidden="true"></i> Clear All</button>

<div style="clear: both;"></div>

<div class="queryBuilder" style="width: 805px; margin: 0 auto;">
	<form id="filters" name="filters" action="">
	<div class="row">
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Application Status</legend>
				<cfloop query="getAppStatuses">
					<label class="checkbox-inline"><input type="checkbox" name="app_status" value="#app_status#">#app_status#</label>
				</cfloop>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Accepted Cluster</legend>
				<cfloop query="getClusters">
					<label class="checkbox-inline"><input type="checkbox" name="cluster" value="#short_name#">#short_name#</label>
				</cfloop>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Interests</legend>
				<label class="checkbox-inline"><input type="checkbox" name="int_pal" value="1">PALs</label>
				<label class="checkbox-inline"><input type="checkbox" name="int_returners" value="1">Returners</label>
				<label class="checkbox-inline"><input type="checkbox" name="int_sos" value="1">Science Outreach</label>
				<label class="checkbox-inline"><input type="checkbox" name="int_sustainability" value="1">Sustainability</label>
				<label class="checkbox-inline"><input type="checkbox" name="int_wins" value="1">WINs</label>
				<label>Match records that have:
					<select class="" name="int_options">
						<option value="any">At least 1 of these</option>
						<option value="all">At least all of these</option>
						<option value="only">Only these</option>
					</select>
				</label>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Confirmation Dec.'s</legend>
				<cfloop query="getConfirmations">
					<label class="checkbox-inline"><input type="checkbox" name="confirmation_decision" value="#confirmation_decision#">#confirmation_decision#</label>
				</cfloop>
			</fieldset>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Application Type</legend>
				<cfloop query="getAppTypes">
					<cfif app_type NEQ "">
						<label class="checkbox-inline"><input type="checkbox" name="app_type" value="#app_type#" id="c#app_type#">#app_type#</label>
					</cfif>
				</cfloop>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Year</legend>
				<cfloop query="getYears">
					<label class="checkbox-inline"><input type="checkbox" name="student_year" value="#student_year#">#student_year#</label>
				</cfloop>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Gender</legend>
				<cfloop query="getGenders">
					<label class="checkbox-inline"><input type="checkbox" name="pi_gender" value="#pi_gender#">#pi_gender#</label>
				</cfloop>
			</fieldset>
		</div>
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Attributess</legend>
				<label class="checkbox-inline"><input type="checkbox" name="ods_admitted" value="Y">Admitted</label>
				<label class="checkbox-inline"><input type="checkbox" name="ods_first_gen" value="Y">First Gen</label>
				<label class="checkbox-inline"><input type="checkbox" name="ods_asset" value="Y">ASSET</label>
				<label class="input-inline"><input class="admIndex_input" type="text" name="min_adm" value="">Min. ADM</label>
				<label class="input-inline"><input class="admIndex_input" type="text" name="max_adm" value="">Max. ADM</label>
			</fieldset>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<fieldset class="vChecklist">
				<legend>Ethnicities</legend>
				<cfloop list="#listSort(unique_ethVL, "text")#" index="i">
					<label class="checkbox-inline"><input type="checkbox" name="ethnicities" value="#i#">#i#</label>
				</cfloop>
				<input type="hidden" name="ref_all_ethnicities" value="#unique_ethVL#">
				<label>Match:
					<select class="" name="eth_options">
						<option value="any">At least 1 of these</option>
						<option value="all">At least all of these</option>
						<option value="only">Only these</option>
					</select>
				</label>
			</fieldset>
		</div>
		<div class="col">
			<fieldset>
				<legend>Residence</legend>
				<label>Nation:<br>
				<select id="pi_home_nation" name="pi_home_nation" class="" multiple style="width: 100%;">
					<cfloop query="getNations">
						<option value="#pi_home_nation#">#pi_home_nation#</option>
					</cfloop>
				</select>
				</label>
				<label>State:<br>
				<select id="pi_home_state" name="pi_home_state" class="" multiple style="width: 100%; min-height: 100px;">
					<cfloop query="getStates">
						<option value="#pi_home_state#">#pi_home_state#</option>
					</cfloop>
				</select>
				</label>
			</fieldset>
		</div>
		<div class="col-6">
			<fieldset>
				<legend>ODS Major <button type="button" class="clearMulti"><i class="far fa-times" aria-hidden="true"></i> Clear</button></legend>
				<select id="ods_major" name="ods_major" class="multiSelect" multiple style="font-size: .9em; min-height: 220px;">
					<cfloop query="getODSMajors">
						<cfif arrayFind(ourMajorsArr, ods_major)>
							<option value="#ods_major#">#ods_major#</option>
						</cfif>
					</cfloop>
					<cfloop query="getODSMajors">
						<cfif NOT arrayFind(ourMajorsArr, ods_major)>
							<option value="#ods_major#">#ods_major#</option>
						</cfif>
					</cfloop>
				</select>
			</fieldset>
		</div>
	</div>
	<div class="row">
		
		<div class="col-6">
			<!---
			<fieldset class="vChecklist">
				<legend>Said Major <button type="button" class="clearMulti"><i class="far fa-times" aria-hidden="true" style="padding-right: 5px;"></i>Clear</button></legend>
				<select id="said_major" name="said_major" class="multiSelect" multiple>
					<cfloop query="getSaidMajors">
						<option value="#said_major#">#said_major#</option>
					</cfloop>
				</select>
			</fieldset>
			--->
		</div>
	</div>
	</form>

<div style="clear: both;"></div>

<button id="runQuery" type="button" class="btn btn-primary" style="display: block; margin: 20px auto;">
	<div id="restState" style="display: inline-block;">
		<i class="far fa-bolt" aria-hidden="true"></i>
	</div>
	<div id="loadingState" style="display: none;">
		<i class="far fa-spinner fa-spin fa-fw"></i>
		<span class="sr-only">Loading...</span>
	</div>
	<span id="rQuery" style="padding-left: 10px;">Run Query</span>
</button>

</div>
<hr style="margin-left: 10px; padding: 0;">
<h3 style="float: left;" id="jumpToResults">Results</h3>
<div style="float: right; margin-bottom: 0; padding-bottom: 0;" class="rowCountDiv">Displaying <span class="liveRowCount">0</span> of <span class="intialRowCount">0</span></div>
<div style="clear: both;"></div>
<table id="mainTable" class="tablesorter tsArrows tsArrowsBothFade unifyInputHeights">
	<thead style="cursor: pointer;">
		<tr>
			<th>Name</th>
			<th class="filter-select">ODS Major</th>
			<th class="filter-select">App Type</th>
			<th class="filter-select">Cluster</th>
			<th class="filter-select">App Status</th>
		</tr>
	</thead>
	<tbody id="mainTBody"></tbody>
</table>
</div>

</cfoutput>

<div style="clear: both;"></div>
<cfinclude template="#application.urlpath#mods/tech_issues.cfm">

<script>
$(document).ready(function(){

// Tooltip
$('[data-toggle="tooltip"]').tooltip();

// Clear all inputs
$('#clearAll').click(function(){
	$('#filters')[0].reset();
});

// Clear multiselect
$('.clearMulti').click(function(){
	var thisSelect = '#' + $(this).parent().next('select').attr('id');
	$(thisSelect + ' option:selected').prop('selected', false);
});

// Clear table
function clearTable(){
	$('#mainTBody').empty();
};

// Fn to scroll to anchor
function scrollToAnchor(aid){
	var aTag = $('[id="' + aid + '"]');
	$('html,body').animate({scrollTop: aTag.offset().top}, 'slow');
}

// Fn to modify loadingState
function loadingState(state){
	if(state == "on"){
		$('#restState').hide();
		$('#loadingState').css('display','inline-block').show();
		$('#rQuery').css('color','#c7e4ff');
	} else{
		$('#restState').show();
		$('#loadingState').hide();
		$('#rQuery').css('color','#fff');
	}
};

// Fn to set counters
function setLiveCnt(count){
	$('.liveRowCount').text(count);
};
function setTotalCnt(count){
	$('.intialRowCount').text(count);
};

// Tablesorter
var mTable = $('#mainTable');
mTable.tablesorter({
	widgets: ['zebra', 'filter', 'saveSort'],
	widgetOptions: {
		filter_columnFilters: true,
		filter_saveFilters: false,
		filter_reset: 'button.resetFilter',
		filter_functions: {
		}
	}
});
mTable.bind('filterInit filterEnd pagerComplete', function (event, data) {
    $('.liveRowCount').html( data.filteredRows );
});

// Run query
$('#runQuery').click(function(){
	// Indicate loading
	loadingState('on');

	// Clear previous results
	clearTable();

	// AJAX to get records
	$.ajax({
		type: 'post',
		url: '../mods/query.cfc?method=query',
		data: $('#filters').serialize(),
		datatype: 'json'
	}).done(function(response){
		// Parse JSON
		var results = JSON.parse(response);

		var tmpCount = 0; // Hopefully replace with better solution... reponse.length wasn't being friendly
		if(response.length > 2){
			// Loop through results
			$.each(results, function(k, v){
				// Build row
				var tr = jQuery('<tr />');

				// Build columns
				var name = jQuery('<td />');
				name.html('<a href="/students/cnslc/admin/viewApp.cfm?csuid=' + results[k].CSUID + '" style="text-decoration: underline;">' + results[k].FULL_NAME + '</a>');
				name.appendTo(tr);

				var odsmajor = jQuery('<td />');
				odsmajor.text(results[k].ODS_MAJOR);
				odsmajor.appendTo(tr);

				var apptype = jQuery('<td />');
				apptype.text(results[k].APP_TYPE);
				apptype.appendTo(tr);

				var cluster = jQuery('<td />');
				cluster.text(results[k].CLUSTER);
				cluster.appendTo(tr);

				var appstatus = jQuery('<td />');
				appstatus.text(results[k].APP_STATUS);
				appstatus.appendTo(tr);

				// Append row
				tr.hide();
				tr.appendTo('#mainTBody').show('fast');

				// Increment count
				tmpCount++;
			});

			// Refresh Tablesorter's zebra
			$('#mainTable').trigger('update');

			// Update counts
			setLiveCnt(tmpCount);
			setTotalCnt(tmpCount);

			// Jump to results
			scrollToAnchor('jumpToResults');

		} else{
			var noResults = '<tr><td colspan="5" style="text-align: center;">[No results found matching that query]</td></tr>';
			$('#mainTBody').append(noResults);
			setLiveCnt(0);
			setTotalCnt(0);
		}

		// Turn off loading
		loadingState('off');

	}).fail(function(){
		alert('An error has occurred. (Code 429)');

		// Turn off loading
		loadingState('off');
	});
});

});
</script>