<!--- Security --->
<cfmodule template="#application.urlpath_security#" reqApplicant="true">

<!--- 
	OKAY, LONG REMINDER FOR YOURSELF, STEVE.
	A user lands on "select.cfm" if it was their first time logging in entirely OR if
	they left off there without ever making a selection on their application type before
	getting timed out (I could see this actually happening so it's best to account for it).

	I did not like the idea of having to create a new page just to check if the user provided
	a legit app type (maliciously, they could have modified the form (well, mock form)). The
	variable "session.app_type" is only set if they were a returned user logging in (who already
	went through this process so we know they're "safe") or if their app type submitted was one
	of the two legit options. Therefore, it is safe to rely upon "sesson.app_type". If it is
	not defined, that means they provided a dubious value and we should send them back to select
	their app type again.

	Ideally I'll add a boolean to "submit_appType.cfc" (which is called from "select.cfm") that
	will return true/false depending on the provided value. However, that would only be a
	"front-end" check as it'd be JS/AJAX (in case you can spoof AJAX responses?) so this must
	remain, too.
--->
<cfif NOT structKeyExists(session, "app_type")>
	<cflocation url="#application.urlpath_select#?error=invalidType" addtoken="false">
</cfif>

<!--- Main Query --->
<cfquery name="getInfo">
	SELECT *
	FROM application_form
	WHERE ename = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar">
</cfquery>


<cfoutput>

<!--- Modals --->
<div id="submitModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="submitModalTitle" aria-hidden="true">
	<div class="modal-dialog">
		<!--- Modal content --->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="submitModalTitle">Confirm Submission</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>After you submit your application, you will no longer be able to edit it. You will, however, be able to sign back in to view your answers and check the status of your application.</p>
				<p>Only continue if you are sure you are finished with your application.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="submitApp">Submit</button>
			</div>
		</div>
	</div>
</div>


<!--- Top Headers --->
<a href="#application.urlpath#mods/logout.cfm" class="btn btn-info btn-sm logoutBtn"><i class="far fa-sign-out" aria-hidden="true"></i> Logout</a>
<div style="clear: both;"></div>


<!--- Page Titles --->
<cfif session.app_type EQ "Incoming">
	<h2>Incoming Student Application</h2>
<cfelseif session.app_type EQ "Returning">
	<h2>Returning Student Application</h2>
</cfif>

<!--- Intro Blurb --->
<p style="font-size: 1em;">We are using this application to learn about <span style="font-style: italic;">YOU</span> so it is extremely important that you (not your parent, friend or sibling) fill these out. There is no right or wrong answer.</p>

<p style="font-size: 1em;">Please know Colorado State University responds to any reported disclosures related to sexual misconduct and/or interpersonal violence, and also encourages full and open expression by applicants. If your personal essay submissions contain information related to past or present sexual misconduct, you may receive an outreach from CSU's Office of Support and Safety Assessment to discuss reporting options and available support resources.</p>

<h3 style="margin: 50px 0 10px 0;">Application Progress</h3>
<!--- Notify completed applicant why they cannot edit --->
<cfif session.app_status EQ "In-Progress">
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-primary btn-circle"><i class="far fa-play" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Acceptance</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Confirmation</p>
	        </div> 
	    </div>
	</div>
<cfelseif session.app_status EQ "Completed">
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-primary btn-circle"><i class="far fa-play" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Acceptance</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Confirmation</p>
	        </div> 
	    </div>
	</div>
<cfelseif session.app_status EQ "Waitlisted">
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Reviewed</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-warning btn-circle"><i class="far fa-list-ol" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Acceptance</p><p style="margin: 0; padding: 0;">Your application is on the waitlist.</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Confirmation</p>
	        </div> 
	    </div>
	</div>
<cfelseif session.app_status EQ "Held">
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-warning btn-circle"><i class="far fa-pause" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Acceptance</p><p style="margin: 0; padding: 0;">Your application is on hold.</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-default btn-circle" disabled="disabled"></button>
	            <p>Confirmation</p>
	        </div> 
	    </div>
	</div>
<cfelseif (session.app_status EQ "Accepted") AND (getInfo.confirmation_decision EQ 0)>
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Acceptance</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-primary btn-circle"><i class="far fa-play" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Confirmation</p><p style="margin: 0; padding: 0;">Please confirm <a href="confirm.cfm?cid=#getInfo.confirmation_code#" style="text-decoration: underline;">here</a>.</p>
	        </div> 
	    </div>
	</div>
	<cfelseif (session.app_status EQ "Accepted") AND (getInfo.confirmation_decision EQ "Confirmed")>
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Acceptance</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Confirmed</p>
	        </div> 
	    </div>
	</div>
<cfelseif (session.app_status EQ "Accepted") AND (getInfo.confirmation_decision EQ "Declined")>
	<div class="stepwizard" style="margin: 20px 0 60px 0;">
	    <div class="stepwizard-row">
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Started</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Application Completed</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Under Review</p>
	        </div> 
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-success btn-circle"><i class="far fa-check" aria-hidden="true"></i></button>
	            <p>Acceptance</p>
	        </div>
	        <div class="stepwizard-step">
	            <button type="button" class="btn btn-danger btn-circle"><i class="far fa-times" aria-hidden="true"></i></button>
	            <p style="font-weight: bold;">Confirmation</p><p style="margin: 0; padding:0">Declined by Applicant</p>
	        </div> 
	    </div>
	</div>
</cfif>

<!--- Warning Div --->
<cfif session.app_status EQ "In-Progress">
	<div class="alert alert-warning" role="alert" style="text-align: center; margin: 20px 0 20px 0;">
		<i class="far fa-exclamation-triangle fa-lg" aria-hidden="true" style="margin-right: 5px;"></i> You must save your application every <em style="font-weight: bold;">30</em> minutes, otherwise all changes may be lost.
	</div>
<cfelse>
	<div class="alert alert-info" role="alert" style="text-align: center; margin: 20px 0 20px 0;">
		<i class="far fa-info-circle fa-lg" aria-hidden="true" style="margin-right: 5px;"></i> As you have already submitted your application, you are not able to edit your responses.
	</div>
</cfif>
<div class="alert alert-danger missingFields" role="alert" style="text-align: center; margin: 20px 0 20px 0; display: none;">
	<i class="far fa-exclamation-circle fa-lg" aria-hidden="true" style="margin-right: 5px;"></i> Please fill out the highlighted fields correctly.
</div>

<h3 style="margin-top: 50px;">Questions</h3>

<!--- Include Application Forms --->
<cfif session.app_type EQ "Incoming">
	<cfinclude template="apps/incoming.cfm">
<cfelseif session.app_type EQ "Returning">
	<cfinclude template="apps/returning.cfm">
</cfif>

<div class="alert alert-danger missingFields" role="alert" style="text-align: center; margin: 20px 0 20px 0; display: none;">
	<i class="far fa-exclamation-circle fa-lg" aria-hidden="true"></i> Please fill out the highlighted fields correctly.
</div>

<!--- Display options only if the applicant has not already completed their application --->
<cfif session.app_status EQ "In-Progress">
<div id="actionButtonDiv">
	<button id="saveDraft" type="button" class="btn btn-secondary" data-toggle="modal" data-target="##saveModal"><i class="far fa-save fa-lg" aria-hidden="true" style="margin-right: 5px;"></i> Save as Draft</button>
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="##submitModal"><i class="far fa-paper-plane fa-lg" aria-hidden="true" style="margin-right: 5px;"></i> Save and Submit</button>
</div>
</cfif>

</cfoutput>

<cfinclude template="#application.urlpath#mods/questions.cfm">


<!--- Scripts for submits and form interaction --->
<script>
$(document).ready(function(){
	<!--- If user has already submitted, make read only -- there will be a check server-side, too --->
	<cfif session.app_status NEQ "In-Progress">
	/* Application has already been submited, disable inputs */
	$('#appForm :input').prop('disabled', true);
	</cfif>

	/* On initial page load, show selected inputs */
	$('#interests input:checkbox').each(function(index){
		// Get corresponding div
		var initextradiv = $(this).data('extradiv');

		// If checked
		if( $(this).is(':checked') ){
			$('#' + initextradiv).show();
		} else{
		// If unchecked
			$('#' + initextradiv).hide();
		}
	});

	/* Auto Height Textareas to fit Content */
	$("textarea").each(function(){$(this).height( $(this)[0].scrollHeight )});


	<!---
	ALL OF BELOW IS ONLY NEEDED FOR "IN-PROGRESS" APPLICATIONS.

	All other application "statuses" do not need to have a modifiable form or, more
	importantly, a submittable form. This is *especially* true for the auto-save/draft
	feature. When this is left running in a non "In-Progress" application (say they
	signed in to check their application status), if the user lets the session get
	close to timing out, it would "submit" their application but, as the form is
	disabled for them, it would submit blank data and erase their application data.
	BAD. VERY BAD.
	--->

	<cfif session.app_status EQ "In-Progress">
	/* On checkbox change, hide/show appropriately */
	$('#interests input:checkbox').click(function(){
		// Get corresponding div 
		var extradiv = $(this).data('extradiv');

		// If checked
		if( $(this).is(':checked') ){
			$('#' + extradiv).show(200);
		} else{
		// If unchecked
			$('#' + extradiv).hide(200);
		}
	});

	/* Auto advance on phone numbers */
	$('.autoNext').keyup(function(){
		var $this = $(this);
		var characters = $this.val().length;
		var maxLength = $this.attr('maxLength');
		
		if( characters == maxLength){
			$this.next('input').focus();
		}
	});

	<!---
	Okay, if the user is about to get timed out by CF (currently set at 2 hours), it
	would be nice if we saved their data for them. However, having JS call the
	"submit_app.cfc" will renew their session for another 2 hours. This is not viable
	as it would keep the user logged in forever.

	So what is the best course of action? Use JS to set a timer (slightly less than 2
	hours - which is okay since we tell them they only have 30 minutes) that will
	call "submit_app.cfc" but then 
	--->
	/* Session is about to timeout - Save draft for user */
	var myTimer = (<cfoutput>#application.sessTimeout#</cfoutput> - 120000);
	setTimeout(function(){ 
			$.ajax({
				type: 'post',
				url: 'mods/submit_app.cfc?method=submitApp',
				data: $('#appForm').serialize(),
				datatype: 'json',
				success: function(){window.location.replace('<cfoutput>#application.urlpath#</cfoutput>?status=noSession');},
				error: function(){alert('An error has occurred. (381)');}
			});
		}, 
		myTimer
	);


	/* Check phone function */
	function validatePhone(){
		var isValid = true;
		var filter = /^\d+$/;
		$('.phone').each(function(){
			if( !filter.test($(this).val()) || $(this).val().length != $(this).attr('maxLength') ){
				isValid = false;
			}
		});
		return isValid;
	};

	/* Check email function */
	function validateEmail(emailAddr){
		var a = document.getElementById(emailAddr).value;
		var filter = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(filter.test(a) && a.includes('@') && a.includes('.')){
			return true;
		} else{
			return false;
		}
	};


	/* Save as draft */
	$('#saveDraft').click(function(){
		$.ajax({
			type: 'post',
			url: 'mods/submit_app.cfc?method=submitApp',
			data: $('#appForm').serialize(),
			datatype: 'json',
			success: function(){window.location.reload(true);},
			error: function(){alert('An error has occurred. (420)');}
		});
	});


	/* Submit for review */
	$('#submitApp').click(function(){
		// Flag
		var allGood = true;
		// Check if phone values are empty
		var phoneUse = false;
		if( $('#phone1').val() || $('#phone2').val() || $('#phone3').val() ){
			phoneUse = true;
		}

		// Loop through all optional inputs
		$('#appForm input:visible, #appForm textarea:visible, #appForm select:visible').each(function(index, data){
			// Check all req'd inputes for empties
			if( this.value.length == 0 && !$(this).hasClass('opt') ){
				allGood = false;
				$(this).addClass('is-invalid');
			}
		});
		// Check phone, if needed
		if(phoneUse){
			if( !validatePhone('phone1') || !validatePhone('phone2') || !validatePhone('phone3') ){
				allGood = false;
				$('.phone').addClass('is-invalid');
			}
		}
		// Email
		if( !validateEmail('email') ){
			allGood = false;
			$('#email').addClass('is-invalid');
		}

		// Handle
		if(allGood){
			$.ajax({
				type: 'post',
				url: 'mods/submit_app.cfc?method=submitApp',
				data: $('#appForm').serialize() + '&app_status=Completed',
				datatype: 'json',
				success: function(){window.location.replace('completed.cfm');},
				error: function(){alert('An error has occurred. (464)');}
			});
		} else{
			$('#submitModal').modal('hide');
			$('.missingFields').show();
		}
	});

	/* If user tried to submit and discovered blank fields, remove the class that was added to notify them */
	$('input, textarea, select').change(function(){
		if( $(this).val() ){
			$(this).removeClass('is-invalid');
		}
		// If editing phone number
		if( $(this).hasClass('phone') ){
			$('.phone').removeClass('is-invalid');
		}
	});
	</cfif>

});
</script>


<!--- Log action --->
<cfmodule template="#application.urlpath_log#" action="Application: User visited their application">