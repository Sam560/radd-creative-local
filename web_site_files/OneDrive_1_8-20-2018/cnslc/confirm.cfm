<!--- Param var --->
<cfparam name="url.cid" default="">

<!--- Run query --->
<cfquery name="recordCheck">
	SELECT ename, confirmation_decision
	FROM application_form
	WHERE confirmation_code = <cfqueryparam value="#url.cid#" cfsqltype="cf_sql_varchar">
</cfquery>

<!--- Set flag vars --->
<cfset isValid = false>
<cfset isMultiple = false> <!--- Should never happen but if so don't allow user to proceed and log it --->

<!--- Assign flag vars --->
<cfif recordCheck.recordCount GT 1>
	<cfset isMultiple = true>
<cfelseif (recordCheck.recordCount EQ 1) AND (recordCheck.confirmation_decision EQ 0)>
	<cfset isValid = true>
</cfif>

<cfoutput>

<!--- Modals --->
<div id="declineModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="declineModalTitle" aria-hidden="true">
	<div class="modal-dialog">
		<!--- Modal content --->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="declineModalTitle">Are you sure?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to decline?</p>
				<div style="margin: 10px 0 0 10px; font-size: .8em;">
					<label for="confirmation_decline_why">If you have a moment, please provide some feedback as to why you are no longer interested in joining the CNSLC.</label>
					<textarea name="confirmation_decline_why" id="confirmation_decline_why" class="form-control form-control-sm" placeholder="Brief description..."></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					<i class="far fa-angle-double-left fa-lg" aria-hidden="true"></i> Go Back
				</button>
				<button type="button" class="btn btn-danger btn-conf" id="submitApp" data-decision="Declined">
					<i class="far fa-times-circle fa-lg" aria-hidden="true"></i> Decline
				</button>
			</div>
		</div>
	</div>
</div>
<!--- Modals --->
<div id="acceptModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="acceptModalTitle" aria-hidden="true">
	<div class="modal-dialog">
		<!--- Modal content --->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="acceptModalTitle">Congratulations!</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Thanks for joining the CNSLC! If you have any questions, please <a href="mailto:#application.helpEmail#" style="text-decoration: underline;">contact us</a>.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submitApp" data-dismiss="modal">Done</button>
			</div>
		</div>
	</div>
</div>


<h2>Confirmation</h2>

<!--- If invalid CID is provided --->
<cfif NOT isValid>
	<p style="margin-top: 10px;"><span style="font-weight: bold;">Error:</span> Not a valid confirmation code.</p>


<!--- If there are somehow duplicate CIDs --->	
<cfelseif isMultiple>
	<p style="margin-top: 10px;">An error has occurred. Please <a href="mailto:#application.admin_email#">contact us</a>.


<!--- If everything checks out, proceed --->
<cfelse>
	<p style="margin-top: 10px;">Congratulations on your acceptance to the CNSLC! In order to complete the process, please confirm or decline your invitation.</p>

	<div style="margin: 10px 0 0 10px;">

		<button type="button" class="btn btn-success btn-conf" data-toggle="modal" data-target="##acceptModal" data-decision="Confirmed" style="margin-right: 10px;">
			<i class="far fa-check-circle fa-lg" aria-hidden="true"></i> Confirm
		</button>
		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="##declineModal" style="margin-left: 10px;">
			<i class="far fa-times-circle fa-lg" aria-hidden="true"></i> Decline
		</button>

	</div>
</cfif>

</cfoutput>

<div style="margin-top: 200px;">
<cfinclude template="#application.urlpath#mods/questions.cfm">
</div>

<cfif (isValid) AND (NOT isMultiple)>
<script>
$(document).ready(function(){

$('.btn-conf').click(function(){
	confObj = {};
	confObj.cid = '<cfoutput>#url.cid#</cfoutput>';
	confObj.choice = $(this).data('decision');

	if( $(this).data('decision') == "Declined" ){
		confObj.why = $('#confirmation_decline_why').val();
	}

	$.ajax({
		type: 'post',
		url: 'mods/submitConfirm.cfc?method=confirm',
		data: confObj,
		datatype: 'json'
	}).done(function(){
		window.location.replace('<cfoutput>#application.urlpath#</cfoutput>?status=confirmationMade');
	}).fail(function(){
		alert('An error has occurred. (Code 130)');
	});
});

});
</script>
</cfif>