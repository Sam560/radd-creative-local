<cfif CGI.SERVER_PORT EQ 80>
	<cflocation addtoken="no" url="https://apps.natsci.colostate.edu/students/cnslc/">
</cfif>

<cfparam name="URL.status" default="">

    <!--- Not in session --->
    <!---
    <h1>CNSLC application deadline has passed</h1>
    <cfoutput>
    <p>We are no longer accepting applications for the #APPLICATION.beginYear#-#APPLICATION.endYear# academic year. Please check back here in October #APPLICATION.beginYear# for the #APPLICATION.endYear#/#APPLICATION.nextYear# application.</p>
    </cfoutput>
    --->


    <!--- Is in session --->
    <cfoutput>

    <cfset userShouldBeAbleToAccessThisPage = false>
    
    <!--- Check URL Status to display appropriate message --->
    <cfif (URL.status NEQ "") AND (URL.status NEQ "confirmationMade")>
        <div class="errorDiv">
        <cfif URL.status EQ "noSession">
            <cfset structClear(session)>
            <p style="color: ##333;"><i class="far fa-sign-out fa-lg" aria-hidden="true" style="padding-right: 5px;"></i><span style="font-weight: bold;">Your session expired.</span> While we attempt to save a draft of your application for you, be sure to save your work every 30 minutes. Please sign in again.</p>
        <cfelseif URL.status EQ "noAuth">
            <cfset structClear(session)>
            <p style="color: ##333;"><i class="far fa-minus-circle fa-lg" aria-hidden="true" style="padding-right: 5px;"></i><span style="font-weight: bold;">Failed authorization.</span> You do not have authorization to access this web application.</p>
        <cfelseif URL.status EQ "badCred">
            <cfset structClear(session)>
            <p style="color: ##333;"><i class="far fa-key fa-lg" aria-hidden="true" style="padding-right: 5px;"></i><span style="font-weight: bold;">Invalid credentials.</span> Please try again.</p>
        <cfelseif URL.status EQ "invalidData">
            <cfset structClear(session)>
            <p style="color: ##333;"><i class="far fa-terminal fa-lg" aria-hidden="true" style="padding-right: 5px;"></i></i><span style="font-weight: bold;">Invalid data was provided.</span> You have been signed out.</p>
            <p style="color: ##333;">If this is a reoccurring issue, please <a href="mailto:#application.admin_email#">contact us</a>.</p>
        </cfif>
        </div>
    </cfif>
    <cfif URL.status EQ "confirmationMade">
        <cfset structClear(session)>
        <div class="errorDiv" style="background: ##caffbe; border: 1px solid ##4ca537;">
            <p><i class="far fa-check-circle" aria-hidden="true"></i> Your confirmation choice has been recorded.</p>
        </div>
    </cfif>

    <a href="#application.urlpath_app#" class="btn btn-info btn-sm"><i class="far fa-arrow-right" aria-hidden="true" style="margin-right: 5px;"></i> Return to application</a>

    </cfoutput>

    <div style="clear: both; margin-bottom: 300px;"></div>

    <cfinclude template="#application.urlpath#mods/tech_issues.cfm">