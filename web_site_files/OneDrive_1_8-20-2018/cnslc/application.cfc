component {

    /*
    author: Steve Garten
    application: CNSLC application
    notes:
        4/12/18
            Started migrating application over to "apps" server
            Started converting application to use app.cfc
    */

    this.name = "cnslcapp";
    this.datasource = "cnslc_maria";
    this.sessionManagement = true;
    this.sessionTimeout = CreateTimeSpan(0, 1, 0, 0); // 1 hours

    function onApplicationStart() {

        // Basics
        application.h1 = "CNS Learning Community";
        application.h1_admin = "CNSLC Admin Dashboard";
        application.ptitle = "Learning Community";
        application.width_admin = "1200";
        application.sessTimeout = "3600000";
        application.contactEmail = "cns_web@colostate.edu";
        application.projectCSS = "/students/cnslc/styles/style.css";

        // Useful toggles
        application.enforceAdmin = true;

        // URLs
        application.urlpath = "/students/cnslc/";
        application.urlpath_https = "https://apps.natsci.colostate.edu" & application.urlpath;
        application.urlpath_security = "/students/cnslc/mods/security.cfm";
        application.urlpath_select = "/students/cnslc/select.cfm";
        application.urlpath_app = "/students/cnslc/main.cfm";
        application.urlpath_admin = "/students/cnslc/admin/";
        application.urlpath_log = "/students/cnslc/mods/log_action.cfm";
        application.urlpath_css = "/students/cnslc/styles/style.css";

        // Contacts
        application.helpContact = "Lindsey Young";
        application.helpEmail = "cns_lc@mail.colostate.edu";
        application.helpPhone = "970-491-3766";

        application.palContact = "Alexandra Keller";
        application.palEmail = "a.keller@colostate.edu";
        application.palPhone = "970-491-2369";

        application.emailFrom = "cns_lc@mail.colostate.edu";
        application.emailReplyTo = "cns_lc@mail.colostate.edu";

        application.admin_email = "cnsweb@colostate.edu";

        // Time-related variables
        application.currentTerm = "SP17";
        application.beginYear = "2017";
        application.endYear = "2018";
        application.nextYear = "2019";
        application.confDeadline = "May 15, 2017"; // Confirmation deadline (used in "ref_mod.cfm") | NO LONGER REFERENCED AS DEADLINE DOES NOT EXIST 
        application.earlyMove = "Wednesday, August 16, 2017"; // Early move-in date (used for incoming students only in "confirm.cfm")
        application.reOpenMonth = "October"; // Application re-open date (used in "index.cfm")

        // Utility functions
        //application.utils = createObject("component", "utils").init();

        // Error handling
        cferror(type="exception", template="./error.cfm");

        return true;

    }

    function onSessionStart() {

        // writeDump( getHTTPRequestData().headers );

        // Try Shib vars and set to session
        try {
            session.username = getHTTPRequestData().headers["colostateEduPersonEID"];
            session.csuid = getHTTPRequestData().headers["colostateEduPersonCSUID"];
            session.f_name = getHTTPRequestData().headers["eduPersonNickname"];
            session.l_name = getHTTPRequestData().headers["sn"];
            session.lf_name = session.l_name & ", " & session.f_name;
            session.fl_name = session.f_name & " " & session.l_name;
            session.email = getHTTPRequestData().headers["mail"];
        } catch (any e) {
            // Error 89 probably means the user tried to sign in with their secondary eID
            writeOutput('<h1>An error has occurred (Error 89). Please notify the system administrator.</h1>');
            abort;
        }

        // Other session vars
        session.valid = true;
        session.ip_address = "#CGI.REMOTE_ADDR#";
        session.client = "#CGI.HTTP_USER_AGENT#";

        // Query admin table
        adminCheck = queryExecute("
            SELECT ename, permission_level, email
            FROM susr_admin
            WHERE ename = :username",
            { username = {value = session.username, cfsqltype = 'varchar'} }
        );

        // If user is an admin
        if(adminCheck.recordcount == 1) {

            // Set session vars
            if(adminCheck.permission_level >= 7) {
                session.userLevel = "admin";
            } else if(adminCheck.permission_level >= 3) {
                session.userLevel = "reviewer";
            }
            session.permission_level = adminCheck.permission_level; // Save in session specific permission level if ever needed
            session.email = adminCheck.email;

            // Log action
            cfmodule(template="#application.urlpath_log#", action="Login: Admin");

            // Direct to admin page
            cflocation(url="#application.urlpath_admin#", addtoken="false");

        }

        // If user is applicant
        else {

            // Set session vars
            session.userLevel = "applicant";

            // Query to see if user is an existing user
            studentCheck = queryExecute("
                SELECT csuid, ename, last_name, first_name, app_type, app_status
                FROM application_form
                WHERE ename = :username",
                { username = {value = session.username, cfsqltype = 'varchar'} }
            );

            // If this is the applicant's first time signing in
            if(studentCheck.recordcount == 0) {

                // Set status
                session.app_status = "In-Progress";

                // ODS query section - 
                // removed for security - just queries the CSU database to bring in information to keep applicants from having to enter it
                
                
               
                // Log action
                cfmodule(template="#application.urlpath_log#", action="Login: First time user");

                // Redirect to next page (selecting "incoming" or "returning")
                cflocation(url="#application.urlpath_select#", addtoken="false");

            }

            // If user is a returning applicant
            else {

                // Set status
                session.app_status = studentCheck.app_status;

                // If user never selected app type
                if( (studentCheck.app_type NEQ "Incoming") AND (studentCheck.app_type NEQ "Returning") ) {

                    // Log action
                    cfmodule(template="#application.urlpath_log#", action="Login: Returning user but still needed to select app type.");

                    // Redirect to next page (selecting incoming or returning)
                    cflocation(url="#application.urlpath_select#", addtoken="false");

                }

                // User has already selected app type
                else {

                    // Store app_type as session var
                    session.app_type = studentCheck.app_type;

                    // Update "last_login"
                    updateLastLogin = queryExecute("
                        UPDATE application_form
                        SET last_login = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'
                        WHERE ename = :username",
                        { username = {value = session.username, cfsqltype = 'varchar'} }
                    );

                    // Log action
                    cfmodule(template="#application.urlpath_log#", action="Login: Returning user");

                    // Forward onto main application page
                    cflocation(url="#application.urlpath_app#", addtoken="false");
                    
                }

            }

        }

    }

    // the target page is passed in for reference, but you are not required to include it
    function onRequestStart( string targetPage ) {

        if(CGI.SERVER_PORT EQ 80) {
            cflocation(url="https://apps.natsci.colostate.edu/students/cnslc/", addtoken="no");
        }
        
        // Check session
        if(!structKeyExists(session, 'username')) {

            // Trigger session start
            onSessionStart();

        }

    }

    function onRequest( string targetPage ) {

        //applicationStop();
        //onApplicationStart();

        cfmodule(template="/base_mods/header.cfm", ptitle="#application.ptitle#", projectCSS="#application.projectCSS#", tooltips="yes", tablesorter="yes", cropperjs="yes");
        cfmodule(template="/base_mods/menu.cfm");

        // Vary page width depending on user
        if(session.userLevel == 'applicant') {
            writeOutput('<div id="mainBody">');
        } else {
            writeOutput('<div id="adminMainBody">');
        }

        include arguments.targetPage;

        writeOutput('</div>');

        include "/base_mods/footer.cfm";
        include "/base_mods/sub_footer.cfm";

    }

    function onRequestEnd() {}

    function onSessionEnd( struct SessionScope, struct ApplicationScope ) {}

    function onApplicationEnd( struct ApplicationScope ) {}

    // function onError( any Exception, string EventName ) {}

}   