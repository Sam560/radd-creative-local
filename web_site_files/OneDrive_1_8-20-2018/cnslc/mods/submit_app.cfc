<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#" reqApplicant="true">

	<!--- ONLY ALLOW EXECUTION OF THIS IF THE USER HAS NOT PREVIOUSLY SUMBITTED --->
	<cfif session.app_status EQ "In-Progress">

		<cffunction name="submitApp" access="remote" output="false">

			<!--- Arguments (both incoming and returning) --->
			<cfargument name="first_name" type="string" default="">
			<cfargument name="last_name" type="string" default="">
			<cfargument name="email" type="string" default="">
			<cfargument name="phone" type="string" default="">
			<cfargument name="int_wins" default="0">
			<cfargument name="app_status" default="In-Progress">

			<!--- Arguments (incoming) --->
			<cfargument name="said_major" type="string" default="">
			<cfargument name="why_major" type="string" default="">
			<cfargument name="shaped_you" type="string" default="">
			<cfargument name="meaningful_exp" type="string" default="">
			<cfargument name="something_unique" type="string" default="">
			<cfargument name="int_sos" default="0">
			<cfargument name="int_sos_extra" type="string" default="">
			<cfargument name="int_sustainability" default="0">
			<cfargument name="int_sustainability_extra" type="string" default="">
			<cfargument name="int_wins_extra" type="string" default="">

			<!--- Arguments (returning) --->
			<cfargument name="res_hall" type="string" default="">
			<cfargument name="why_cnslc" type="string" default="">
			<cfargument name="academic_exp" type="string" default="">
			<cfargument name="one_moment" type="string" default="">
			<cfargument name="ref_fName" type="string" default="">
			<cfargument name="ref_lName" type="string" default="">
			<cfargument name="ref_title" type="string" default="">
			<cfargument name="liked_programs" type="string" default="">
			<cfargument name="int_pal" default="0">
			<cfargument name="int_returners" default="0">


			<!--- If user modified "app_status" on form --->
			<cfif (arguments.app_status NEQ "In-Progress") AND (arguments.app_status NEQ "Completed")>
				<!--- Log modified form --->
				<cfmodule template="#application.urlpath_log#" action="App Submission: User modified form (#arguments.app_status#)">
				<!--- Kick user out if they provided a modified status for "status" --->
				<cflocation url="#application.urlpath_log#?status=invalidData" addtoken="false">
			</cfif>

			<!--- Cleanup phone number --->
			<cfset arguments.phone = replace(arguments.phone, ",", "", "all")>

			<!--- If user unchecked interest field, make sure their "extra" field is wiped --->
			<cfif arguments.int_sos EQ 0>
				<cfset arguments.int_sos_extra = "">
			</cfif>
			<cfif arguments.int_sustainability EQ 0>
				<cfset arguments.int_sustainability_extra = "">
			</cfif>
			<cfif arguments.int_wins EQ 0>
				<cfset arguments.int_wins_extra = "">
			</cfif>

			<!--- IF SUBMITTED FORM WAS FOR INCOMING APPLICANTS --->
			<cfif session.app_type EQ "Incoming">

				<!--- Update record --->
				<cfquery name="updateRecord">
					UPDATE application_form
					SET
						first_name = <cfqueryparam value="#trim(arguments.first_name)#" cfsqltype="cf_sql_varchar">,
						last_name = <cfqueryparam value="#trim(arguments.last_name)#" cfsqltype="cf_sql_varchar">,
						email = <cfqueryparam value="#trim(arguments.email)#" cfsqltype="cf_sql_varchar">,
						phone = <cfqueryparam value="#trim(arguments.phone)#" cfsqltype="cf_sql_varchar">,
						said_major = <cfqueryparam value="#arguments.said_major#" cfsqltype="cf_sql_varchar">,
						why_major = <cfqueryparam value="#arguments.why_major#" cfsqltype="cf_sql_longvarchar">,
						shaped_you = <cfqueryparam value="#arguments.shaped_you#" cfsqltype="cf_sql_longvarchar">,
						meaningful_exp = <cfqueryparam value="#arguments.meaningful_exp#" cfsqltype="cf_sql_longvarchar">,
						something_unique = <cfqueryparam value="#arguments.something_unique#" cfsqltype="cf_sql_longvarchar">,
						int_sos = <cfqueryparam value="#arguments.int_sos#" cfsqltype="cf_sql_integer">,
						int_sos_extra = <cfqueryparam value="#arguments.int_sos_extra#" cfsqltype="cf_sql_longvarchar">,
						int_sustainability = <cfqueryparam value="#arguments.int_sustainability#" cfsqltype="cf_sql_integer">,
						int_sustainability_extra = <cfqueryparam value="#arguments.int_sustainability_extra#" cfsqltype="cf_sql_longvarchar">,
						int_wins = <cfqueryparam value="#arguments.int_wins#" cfsqltype="cf_sql_integer">,
						int_wins_extra = <cfqueryparam value="#arguments.int_wins_extra#" cfsqltype="cf_sql_longvarchar">,
						app_status = <cfqueryparam value="#arguments.app_status#" cfsqltype="cf_sql_varchar">
						<cfif arguments.app_status EQ "Completed">, complete_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'</cfif>
					WHERE ename = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar">
				</cfquery>

				<!--- If completed/submitted --->
				<cfif arguments.app_status EQ "Completed">
					<!--- Update their session variable --->
					<cfset session.app_status = "Completed">
					<!--- Log action --->
					<cfmodule template="#application.urlpath_log#" action="App: Submitted as completed (Incoming)">
				<!--- If saved as draft --->
				<cfelse>
					<cfmodule template="#application.urlpath_log#" action="App: Saved as draft (Incoming)">
				</cfif>


			<!--- IF SUBMITTED FORM WAS FOR RETURNING APPLICANTS --->
			<cfelseif session.app_type EQ "Returning">

				<!--- Update record --->
				<cfquery name="updateRecord">
					UPDATE application_form
					SET
						first_name = <cfqueryparam value="#trim(arguments.first_name)#" cfsqltype="cf_sql_varchar">,
						last_name = <cfqueryparam value="#trim(arguments.last_name)#" cfsqltype="cf_sql_varchar">,
						email = <cfqueryparam value="#trim(arguments.email)#" cfsqltype="cf_sql_varchar">,
						phone = <cfqueryparam value="#trim(arguments.phone)#" cfsqltype="cf_sql_varchar">,
						res_hall = <cfqueryparam value="#arguments.res_hall#" cfsqltype="cf_sql_varchar">,
						why_cnslc = <cfqueryparam value="#arguments.why_cnslc#" cfsqltype="cf_sql_longvarchar">,
						academic_exp = <cfqueryparam value="#arguments.academic_exp#" cfsqltype="cf_sql_longvarchar">,
						one_moment = <cfqueryparam value="#arguments.one_moment#" cfsqltype="cf_sql_varchar">,
						ref_fName = <cfqueryparam value="#arguments.ref_fName#" cfsqltype="cf_sql_varchar">,
						ref_lName = <cfqueryparam value="#arguments.ref_lName#" cfsqltype="cf_sql_varchar">,
						ref_title = <cfqueryparam value="#arguments.ref_title#" cfsqltype="cf_sql_varchar">,
						liked_programs = <cfqueryparam value="#arguments.liked_programs#" cfsqltype="cf_sql_longvarchar">,
						int_pal = <cfqueryparam value="#arguments.int_pal#" cfsqltype="cf_sql_integer">,
						int_returners = <cfqueryparam value="#arguments.int_returners#" cfsqltype="cf_sql_integer">,
						int_wins = <cfqueryparam value="#arguments.int_wins#" cfsqltype="cf_sql_integer">,
						app_status = <cfqueryparam value="#arguments.app_status#" cfsqltype="cf_sql_varchar">
						<cfif arguments.app_status EQ "Completed">, complete_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'</cfif>
					WHERE ename = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar">
				</cfquery>

				<!--- If completed/submitted --->
				<cfif arguments.app_status EQ "Completed">
					<!--- Update their session variable --->
					<cfset session.app_status = "Completed">
					<!--- Log action --->
					<cfmodule template="#application.urlpath_log#" action="App: Submitted as completed (Returning)">
				<!--- If saved as draft --->
				<cfelse>
					<cfmodule template="#application.urlpath_log#" action="App: Saved as draft (Returning)">
				</cfif>


			<!--- IF SUBMITTED FORM WAS CORRUPT SOMEHOW --->
			<cfelse>

				<!--- Log action --->
				<cfmodule template="#application.urlpath_log#" action="App: Saved as draft (Failed)">

			</cfif>

		</cffunction>

	</cfif>

</cfcomponent>