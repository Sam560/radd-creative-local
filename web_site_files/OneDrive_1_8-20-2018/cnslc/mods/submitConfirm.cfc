<cfcomponent output="false">

	<cffunction name="confirm" access="remote" output="false">
		<!--- Arguments --->
		<cfargument name="cid" required="true" default="0" type="string">
		<cfargument name="choice" required="true" default="0" type="string">
		<cfargument name="why" required="false" default="" type="string">

		<!--- Look up record --->
		<cfquery name="checkApp">
			SELECT ename, confirmation_decision
			FROM application_form
			WHERE confirmation_code = <cfqueryparam value="#arguments.cid#" cfsqltype="cf_sql_varchar" maxlength="50">
		</cfquery>

		<!--- Only proceed if one record was found and they have not previously responded --->
		<cfif (checkApp.recordCount EQ 1) AND (checkApp.confirmation_decision EQ 0)>
			<!--- Mark as confirmed --->
			<cfif (arguments.choice EQ "Confirmed")>
				<cfquery name="markConfirmed">
					UPDATE application_form
					SET
						confirmation_decision = 'Confirmed',
						confirmation_decision_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'
					WHERE ename = <cfqueryparam value="#checkApp.ename#" cfsqltype="cf_sql_varchar">
				</cfquery>

			<!--- Mark as declined --->
			<cfelseif (arguments.choice EQ "Declined")>
				<cfquery name="markConfirmed">
					UPDATE application_form
					SET
						confirmation_decision = 'Declined',
						confirmation_decision_dt = '#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#',
						confirmation_decline_why = <cfqueryparam value="#arguments.why#" cfsqltype="cf_sql_longvarchar">
					WHERE ename = <cfqueryparam value="#checkApp.ename#" cfsqltype="cf_sql_varchar">
				</cfquery>
			</cfif>

			<!--- Log action --->
			<cfset current_url = CGI.SERVER_NAME & CGI.PATH_INFO>
			<cfquery name="insertActionRecord">
				INSERT INTO access_log
					(
						ename,
						session_user_level,
						ip_address,
						<!--- user_agent, --->
						page_url,
						action,
						<!--- impacted_user, --->
						access_dt
					)
				VALUES
					(
						<cfqueryparam value="#checkApp.ename#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="applicant" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="#CGI.REMOTE_ADDR#" cfsqltype="cf_sql_varchar">,
						<!---<cfqueryparam value="#session.user_agent#" cfsqltype="cf_sql_varchar ">,--->
						<!---<cfqueryparam value="#attributes.page_url#" cfsqltype="cf_sql_varchar">,--->
						<cfqueryparam value="#current_url#" cfsqltype="cf_sql_varchar">,
						<cfqueryparam value="Applicant Decision: #arguments.choice#" cfsqltype="cf_sql_varchar">,
						<!---<cfqueryparam value="#attributes.impacted_user#" cfsqltype="cf_sql_varchar">,--->
						<!---#dateTimeFormat(NOW(), "yyyy-mm-dd HH:nn:ss")#--->
						'#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'
					)
			</cfquery>
		</cfif>
	</cffunction>

</cfcomponent>