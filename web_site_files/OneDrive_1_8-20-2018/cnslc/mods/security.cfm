<cfparam name="session.valid" default="false">

<!--- Is their session valid (in this case, just exist)? --->
<cfif (session.valid EQ false)>
	<cflocation url="#application.urlpath#?status=noSession" addtoken="false">

<cfelse><!--- Session is valid --->

	<!---
		Random Note:
		Should we switch to use a parameter that is something similar to
		"lowest level" required? Advantages? Disadvantages?
	--->

	<!--- Declare params --->
	<cfparam name="attributes.reqApplicant" default="false">
	<cfparam name="attributes.reqAdmin" default="false">
	<cfparam name="attributes.reqReviewer" default="false">

	<!--- Requested page requires applicant but user is not applicant --->
	<cfif (attributes.reqApplicant EQ true) && (session.userLevel NEQ "applicant")>
		<cflocation url="#application.urlpath#?status=noAuth" addtoken="false">
	</cfif>

	<!--- Requested page requires admin but user is not admin --->
	<cfif (attributes.reqAdmin EQ true) && (session.userLevel NEQ "admin")>
		<cflocation url="#application.urlpath#?status=noAuth" addtoken="false">
	</cfif>

	<!--- Requested page requires reviewer but user is not reviewer --->
	<cfif (attributes.reqReviewer EQ true) && (session.userLevel NEQ "reviewer")>
		<cflocation url="#application.urlpath#?status=noAuth" addtoken="false">
	</cfif>

	<!--- Looks fine, do nothing --->
</cfif>