<cfcomponent output="false">
	<cfmodule template="#application.urlpath_security#">

	<cffunction name="submitAppType" access="remote" output="false">

		<!--- Arguments --->
		<cfargument name="selection" type="string" required="true" default="" />

		<!--- For Security/Validity: Only continue if one of the two options --->
		<cfif (arguments.selection EQ "Incoming") OR (arguments.selection EQ "Returning")>

			<!--- Update record with applicant's selection --->
			<cfquery name="updateSelection">
				UPDATE application_form
				SET app_type = <cfqueryparam value="#arguments.selection#" cfsqltype="cf_sql_varchar">
				WHERE ename = <cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar">
			</cfquery>

			<!--- Set session.app_type --->
			<cfset session.app_type = #arguments.selection#>

			<!--- Log success --->
			<cfoutput>
			<cfmodule template="#application.urlpath_log#" action="App Type Selection: Success (#session.app_type#)">
			</cfoutput>

		<!--- User modified form --->
		<cfelse>

			<!--- Log modified form --->
			<cfmodule template="#application.urlpath_log#" action="App Type Selection: Error (user modified form)">

			<!--- Kick user out --->
			<cflocation url="#application.urlpath#?status=invalidData" addtoken="false">

		</cfif>
	</cffunction>
</cfcomponent>