<!--- Declare params --->
<cfparam name="session.username" default="">
<cfparam name="session.ip_address" default="">
<cfparam name="session.user_agent" default="">
<!--- <cfparam name="attributes.page_url" default=""> --->
<cfparam name="attributes.action" default="">
<cfparam name="attributes.impacted_user" default="">


<!--- Include security --->
<cfmodule template="#application.urlpath_security#">

<!--- Get current url (hopefully the one that called this cfc) --->
<cfset current_url = CGI.SERVER_NAME & CGI.PATH_INFO>

<!--- Do work --->
<cfquery name="insertActionRecord">
	INSERT INTO access_log
		(
			ename,
			session_user_level,
			ip_address,
			user_agent,
			page_url,
			action,
			impacted_user,
			access_dt
		)
	VALUES
		(
			<cfqueryparam value="#session.username#" cfsqltype="cf_sql_varchar">,
			<cfqueryparam value="#session.userLevel#" cfsqltype="cf_sql_varchar">,
			<cfqueryparam value="#session.ip_address#" cfsqltype="cf_sql_varchar">,
			<cfqueryparam value="#session.user_agent#" cfsqltype="cf_sql_varchar ">,
			<!---<cfqueryparam value="#attributes.page_url#" cfsqltype="cf_sql_varchar">,--->
			<cfqueryparam value="#current_url#" cfsqltype="cf_sql_varchar">,
			<cfqueryparam value="#attributes.action#" cfsqltype="cf_sql_varchar">,
			<cfqueryparam value="#attributes.impacted_user#" cfsqltype="cf_sql_varchar">,
			<!---#dateTimeFormat(NOW(), "yyyy-mm-dd HH:nn:ss")#--->
			'#dateFormat(NOW(), "yyyy-mm-dd")# #timeFormat(NOW(), "HH:mm:ss")#'
		)
</cfquery>