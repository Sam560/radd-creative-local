<cfmodule template="#application.urlpath_security#" reqApplicant="true">

<!--- If user has been here (chosen app type before) --->
<cfif structKeyExists(session, "app_type")><!--- Only exists if they have been (due to behavior in ad_login.cfm) --->
	<!--- Log behavior as could be sign of bug or evil users --->
	<cfmodule template="#application.urlpath_log#" action="App Type Selection: Attempted to access the selection page after already having selected an option in the past.">

	<!--- Go ahead and forward user to where they should be (main application page) --->
	<cflocation url="main.cfm" addtoken="false">


<!--- This is user's first time loggin in, proceed --->
<cfelse>
	<cfparam name="URL.error" default="">

	<cfoutput>
	<h2>Select Application Type</h2>

	<p style="font-weight: bold;">Please read the description of each option below and select the appropriate application type for you.</p>

	<cfif URL.error EQ "invalidType">
		<div class="errorDiv">
			<p><span style="font-weight: bold;">Invalid application type provided.</span> Please try again.</p>
		</div>
	</cfif> 

	<div id="optionsContainer" style="margin-top: 20px; margin-left: 10px;">
		<div id="incomingContainer" data-selection="Incoming" class="optionContainer">
			<span class="optionBanner"><i class="far fa-level-down"></i> New CSU Student</span>
			<p>This will be your first semester attending CSU. Includes first-year and transferring students.</p>
		</div>
		<div id="returningContainer" data-selection="Returning" class="optionContainer" style="margin-left: 20px;">
			<span class="optionBanner"><i class="far fa-redo"></i> Returning CSU Student</span>
			<p>You have attended CSU as a student in any previous semester.</p>
		</div>
	</div>

	<cfinclude template="#application.urlpath#mods/questions.cfm">

	<script>
	$(document).ready(function(){
		$('.optionContainer').click(function(){
			$.ajax({
				type: 'post',
				url: '#application.urlpath#mods/submit_appType.cfc?method=submitAppType',
				data: {selection: $(this).data('selection')},
				datatype: 'json',
				success: function(){window.location.replace('main.cfm')},
				error: function(exception){alert('Error: An error occurred. Please sign out and try again.');}
			});
		});
	});
	</script>

	</cfoutput>

</cfif>