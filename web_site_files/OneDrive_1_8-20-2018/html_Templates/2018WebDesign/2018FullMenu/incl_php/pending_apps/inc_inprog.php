<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>In_Progress</title>

    <!--Copyed from index page  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../../css/mobile.css" media="only screen and (max-device-width: 480px), (max-width:480px)" />
<link rel="stylesheet" type="text/css" href="../../css/mid.css" media="only screen and (max-device-width: 768px) and (min-device-width:481px), (min-width:481px) and (max-width:768px)" />
<link rel="stylesheet" type="text/css" href="../../css/max.css" media="only screen and (min-device-width: 769px) and (min-width:769px)" /> 

</head>
<body>
     <!-- javascript files reference -->
<script src= "../../js/menu.js"></script>
<script src="../js/table_scroll.js"></script>
<script src="../../js/page_sort_button.js"></script>
<!-- <script>
    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById("Title1").onchange = myFunc 
    })
    function myFunc(e) {
        console.warn("function call",e)
    }
</script> -->

    
    <div id="greenstripe">

	<div id="logocontainer">

		<div style="height:20px;">
        </div>

		<a href="http://colostate.edu/">
        <span class="NotToShow">CSU</span>
			<div id="csulogo">
			</div>
		</a>
		<div id="collegelogo">
			<a href="http://www.natsci.colostate.edu/">

            <span class="rwd-line">COLLEGE OF </span>
            <span class="rwd-line">NATURAL SCIENCES</span>
			</a>
		</div>
		
    </div>
    <div id="greenstripe2">
    	<div id="deptcontainer">
        <div style="height:8px;">
        </div>
        	<div id="deptlogo">
            <a href="../../index.html">College of Natural Sciences Learning Community</a>
            </div>
        </div>
	</div>

</div>

<div id="cssmenu" style="max.css">
    <ul id="cssmenu">
        <li id="menu_button" onmouseenter="showMenu()" onmouseleave="hideMenu()"> Actions
            <ul id="drop-down">  
            </ul> 
        </li> 
    </ul>
</div>  
<!-- End of Top of screen-->
<header id="tophead"><h1>In-Progress Applications</h1></header>

 <?php
        // connection names oop style, might need to change to operational
        $servname = "localhost";
        $usrname = "root";
        $passwrd = "root4130";   
        $dbname = "csustud";

        //create connection class
        $conn =   mysqli_connect($servname,$usrname,$passwrd,$dbname);
          // // check connection
          if ($conn === FALSE) {
            printf("Connect failed: %s\n", mysqli_connect_error($conn));    // testing purposes. 
        } else {
            echo "Host info: ". mysqli_get_host_info($servname) . "";  // testing purposes
         
        }

    // sql statment 
        $sql = "SELECT *  FROM `access_log`";  
          // run query 
        $result = mysqli_query($conn, $sql); 
        if($result === FALSE) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit("Didnt work");   
        } else {
            echo "<p>Connection Is Working</p>\n";    // return  query result
        }
        mysqli_close($conn);    // close connection 
        
  
        // remove after testing
         echo "<p>4 stroke gang</p>\n" ; 
    ?>

<section class="tableSection"> 
<!--Table Finding Query Infromation-->
    <div class="tableContainer" rel="stylesheet" style="max.css">
        <div class="header" rel="stylesheet" style="max.css">
                <h2>In-Progress Report</h2>
        </div>
            <tbody>
                     <div class="form-control">
                        <label for="search"> <i class="icon-search"></i></label>
                        <input class="table-filter" type="search" data-table="advanced-web-table" placeholder="Search....">
                    </div>

                <table id="datatable"rel="stylesheet" style="max.css">
                    <!-- Header -->
                    <div class="row-headers" rel="stylesheet" style="max.css">
                       <th>ID</th>
                       <th>EName</th>
                       <th>CSU-ID</th>
                       <th>Session-User</th>
                       <th>IP-Address</th>
                       <th>Action</th>
                    </div>
                <?php while( $row = mysqli_fetch_array($result) )  { ?>
                        <tr>
                            <td><?php echo $row["id"]; ?></td>    <!-- Pulling data from Database csustud-->
                            <td><?php echo $row["ename"]; ?></td>
                            <td><?php echo $row["csuid"];?></td>
                            <td><?php echo $row["session_user_level"];?></td>
                            <td><?php echo $row["ip_address"];?></td>
                            <td><?php echo $row["action"];?></td>
                        </tr>
                            <?php } ?>


<!--- Table to display database info --->

                    
                </table>
            </tbody>
    </div>
</section>




<!--Buttons for sorting-->

<div class="buttonContainer" rel="stylesheet" style="max.css" >
     <ul>
        <label id="buttonTitle">Decline</label>
        <select id="Title1" name="Title 1">
        
            <option value="">Please Select...</option>
            <option value="choose1" href="#">Sub1</option>
            <option value="choose2" href="#">Sub2 </option>
            <option value="choose3" href="#">Sub3 </option>
        </select>
     </ul>
     <ul>
        <label id="buttonTitle">Wait-List</label>
        <select id="Title2" name="Title 2">
            <option value="">Please Select...</option>
            <option value="choose" href="#">Sub1</option>
            <option value="choose" href="#">Sub2 </option>
            <option value="choose" href="#">Sub3 </option>
        </select>
     </ul> 
     <ul>
         <label id="buttonTitle">Accept</label>
        <select id="Title3" name="Title 3">
            <option value="">Please Select...</option>
            <option value="choose" href="#">Sub1</option>
            <option value="choose" href="#">Sub2 </option>
            <option value="choose" href="#">Sub3 </option>
        </select>
     </ul>
     <ul>
         <label id="buttonTitle">Hold</label>
        <select id="Title3" name="Title 3">
            <option value="">Please Select...</option>
            <option value="choose" href="#">Sub1</option>
            <option value="choose" href="#">Sub2 </option>
            <option value="choose" href="#">Sub3 </option>
        </select>
     </ul>
     <button onclick= "javascript:ajax_post">Submit</button>
     <button onclick= "#">Reset</button>
</div>




<!--JS -->
<!-- <script>
    window.onscroll = function() {myFunction()};

    var cssmenu = document.getElementById("cssmenu");
    var sticky = cssmenu.offsetTop;

    function myFunction() {
    if (window.pageYOffset >= sticky) {
        cssmenu.classList.add("sticky")
    } else {
        cssmenu.classList.remove("sticky");
    }
    }
</script>
                        
                        
                        
    <script>
    function bars(x) {
        x.classList.toggle("change");
    }
    </script>  -->
</body>
</html>