// Data for menu  
var data = {
    "menu":[
     
      {"name": "Pending-Applications", "link":"#", "submenu": [
            {"name": "In-Progress", "link" :"incl_php/pending_apps/inc_inprog.php", "submenu": null},
            {"name": "Completed", "link":"#", "submenu": null},
            {"name": "Waitlisted", "link":"#", "submenu": null},
            {"name": "Held", "link":"#", "submenu": null},
            {"name": "Declined", "link":"#", "submenu": null}
        ]},
        {"name": "Roster by Clusters", "link":"#", "submenu": [
            {"name": "All Clusters", "link":"#", "submenu": null},
            {"name": "General", "link":"#", "submenu": null},
            {"name": "Returners", "link":"#", "submenu": null},
            {"name": "Science Outreach", "link":"#", "submenu": null},
            {"name": "Sustainability", "link":"#", "submenu": null},
            {"name": "Science Outreach", "link":"#", "submenu": null},
        ]},       
        {"name": "Tools", "link":"#", "submenu": [
            {"name": "Applications", "link":"#", "submenu": null},
            {"name": "Message Defaults", "link":"#", "submenu": null},
            {"name": "Data Utilites", "link":"#", "submenu": null},
            {"name": "Query Builder", "link":"#", "submenu": null},
            {"name": "Statistics", "link":"#", "submenu": null}
        ]},
    ]
};


// Parses the data and creates the menu
function parseMenu(element, menu) {
    for (var i=0; i<menu.length; i++) {
        var nestedli = document.createElement('li');
        nestedli.setAttribute('style', 'display:none;');
        nestedli.setAttribute('onmouseenter', "showNodes(this)");
        nestedli.setAttribute('onmouseleave', "hideNodes(this)");
  var link = document.createElement('a');
  link.setAttribute('href', menu[i].link);
  link.appendChild(document.createTextNode(menu[i].name));
        nestedli.appendChild(link);
        if (menu[i].submenu!=null) {
            var subul = document.createElement('ul');
            nestedli.appendChild(subul);
            parseMenu(subul, menu[i].submenu);
        }
        element.appendChild(nestedli);

    }
}
// Shows the drop down elements 
function showNodes(element){

    var menu = document.getElementById('drop-down');
    menu.style.display = "block";
    var lis = element.querySelectorAll("ul > li");
    for (var i=0;i<lis.length;i++) {
        lis[i].style.display = "block";
    }  
 }

 // Hides the drop down elements
 function hideNodes(element){

    var lis = element.querySelectorAll("ul > li");
    for (var i=0;i<lis.length;i++) {
        lis[i].style.display = "none";
    }  
 }

 // Shows the drop down
 function showMenu(){

    var menu = document.getElementById('drop-down');
    menu.style.display = "block";
    var lis = document.querySelectorAll("#drop-down > li");
    for (var i=0;i<lis.length;i++) {
        lis[i].style.display = "block";
    }  
 }

 // Hide the drop down
 function hideMenu(){
     var menu = document.getElementById('drop-down');
    menu.style.display = "none";
 }

 // When the window loads, create the menu from the data
window.onload=function(){
    var menu= document.getElementById('drop-down');
    parseMenu(menu, data.menu);
}; 