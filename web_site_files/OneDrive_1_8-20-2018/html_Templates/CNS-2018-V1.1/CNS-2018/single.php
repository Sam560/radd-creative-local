<?php get_header(); ?>

<div id="mainBody">

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="news-div">
				<div class="wpb_row vc_row news-list-row">
					<div class="wpb_column">
						<div class="wpb_row news-list-row">
							<div class="wpb_column vc_col-sm-2 news-featured-image">
								<?php if ( has_post_thumbnail() ) {
									the_post_thumbnail(array(128,128));
									} else { ?>
									<img src="<?php bloginfo('template_directory'); ?>/images/csu-ram-greengold-1024x1024.png"
									alt="<?php the_title(); ?>" />
									<?php } ?>
							</div>
							
							<div class="wpb_column vc_col-sm-10">

								<div class="news-list-title-div">
									<?php the_title('<h2>', '</h2>'); ?>
								</div>

								<div class="news-list-details-div">
									By <?php the_author() ?><br>
									<?php the_time('l, F jS, Y, g:i a') ?> MT<br>
									Posted in <?php the_category(', '); ?>
								</div>
							</div>
						</div>
						<div class="wpb_row news-list-row">
							<div class="wpb_column vc_col-sm-12">
								<div class="news-list-content-div">
									<p>
										<?php the_content(); ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>

</div>

<?php get_footer(); ?>