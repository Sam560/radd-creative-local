/*
 * Script by Ross Madden for trigger hamburger menu
 */
(function($) 
{
	
	$(".mk-responsive-nav li.menu-item-has-children > a").after(function() {
		return '<span class="mk-nav-arrow mk-nav-sub-closed"><svg  class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5acbd46cae359" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"/></svg></span>';
	});
	
	$("#hamburger-menu").click(function() 
	{
		if($(this).hasClass("is-active"))
		{
			$(this).removeClass("is-active");
		}
		else
		{
			$(this).addClass("is-active");
		}
		$( "#mobile-menu" ).toggle( 0, function() 
		{
			// Animation complete.
		});
	});
	
	$(".mk-nav-arrow").click(function() 
	{
		//alert("test");
		$(this).next(".sub-menu").toggle(0, function() 
		{
			// Animation complete.
		});
	});

	//search handler in menus
	$(".mk-search-trigger").click(function() 
	{
		$("#mk-nav-search-wrapper").toggle(0, function()
		{
			// Animation complete.
			$('#mk-ajax-search-input').focus();
		});
	});
	
	$("#main-search-submit").click(function()
	{
		$( "#mk-header-navside-searchform" ).submit();
	});
	
	$("#responsive-search-submit").click(function()
	{
		$( "#responsive-searchform" ).submit();
	});
	
})(jQuery);