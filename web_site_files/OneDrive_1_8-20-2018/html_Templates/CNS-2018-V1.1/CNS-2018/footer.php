<div style="clear: both;"></div>

<section id="mk-footer-unfold-spacer"></section>

<section id="mk-footer" class="" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
        <div class="footer-wrapper mk-grid">
        <div class="mk-padding-wrapper">
            		<div class="mk-col-1-4"><section id="text-3" class="widget widget_text mk-in-viewport"><div class="widgettitle">College of Natural Sciences</div>			<div class="textwidget"><ul style="color: #dddddd; padding-left: 0;">
<li>117 Statistics Building<br>
1801 Campus Delivery<br>Fort Collins, Colorado 80523-1801</li>
<li style="margin-top: 10px;">Telephone: 970-491-1300</li>
<li style="margin-top: 10px;&gt;Fax: 970-491-6639&lt;/li&gt;
&lt;li style=" margin-top:="" 10px;="">Email: <a href="mailto:cns_info@colostate.edu">cns_info@colostate.edu</a></li>
</ul></div>
		</section></div>
			<div class="mk-col-1-4"><section id="nav_menu-5" class="widget widget_nav_menu mk-in-viewport"><div class="widgettitle">Departments</div>
<div class="menu-departments-container"><ul id="menu-departments" class="menu noHoverUnderline">
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-722"><a href="http://www.bmb.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Biochemistry &amp; Molecular Biology</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-723"><a href="http://www.biology.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Biology</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-724"><a href="http://www.chem.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Chemistry</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-725"><a href="https://www.cs.colostate.edu/cstop/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Computer Science</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-726"><a href="http://www.math.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Mathematics</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-727"><a href="http://www.physics.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Physics</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-728"><a href="http://www.colostate.edu/Depts/Psychology/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Psychology</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-729"><a href="http://www.stat.colostate.edu/"><svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-59ce56e780de4" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 1792"><path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path></svg>Statistics</a></li>
</ul></div></section></div>
			<div class="mk-col-1-4"><section id="text-5" class="widget widget_text mk-in-viewport"><div class="widgettitle">Stay Connected</div>
                <div class="textwidget">
                    <a target="_self" href="https://advancing.colostate.edu/CNSUPDATE" aria-label="Subscribe to our college news" style="vertical-align: middle;">
                        <div class="mk-font-icons icon-align-none noHoverUnderline" id="" style="display: inline-block; vertical-align: bottom;"></div>
                        <div style="display: inline-block; vertical-align: top; margin-left: 0px;" class="noHoverUnderline">Subscribe for updates</div>
                    </a>
                </div>
		</section><section id="social-2" class="widget widget_social_networks mk-in-viewport"><div id="social-59ce56e781534" class="align-left"><a href="http://www.facebook.com/CollegeofNaturalSciences.CSU" rel="nofollow" class="builtin-icons light large facebook-hover" target="_blank" alt="Follow Us on facebook" title="Follow Us on facebook"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-facebook" data-cacheid="icon-59ce56e7816d5" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M444-6.4h-376c-37.555 0-68 30.445-68 68v376c0 37.555 30.445 68 68 68h376c37.555 0 68-30.445 68-68v-376c0-37.555-30.445-68-68-68zm-123.943 159.299h-49.041c-7.42 0-14.918 7.452-14.918 12.99v19.487h63.723c-2.081 28.41-6.407 64.679-6.407 64.679h-57.565v159.545h-63.929v-159.545h-32.756v-64.474h32.756v-33.53c0-8.098-1.706-62.336 70.46-62.336h57.678v63.183z"></path><title>Facebook link</title></svg></a><a href="https://www.flickr.com/photos/collegeofnaturalsciences-csu/sets/" rel="nofollow" class="builtin-icons light large flickr-hover" target="_blank" alt="Follow Us on flickr" title="Follow Us on flickr"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-flickr" data-cacheid="icon-59ce56e781865" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M444-6.4h-376c-37.555 0-68 30.445-68 68v376c0 37.555 30.445 68 68 68h376c37.555 0 68-30.445 68-68v-376c0-37.555-30.445-68-68-68zm-283.887 319.744c-35.204 0-63.743-28.539-63.743-63.744s28.539-63.744 63.743-63.744c35.206 0 63.745 28.539 63.745 63.744s-28.539 63.744-63.745 63.744zm191.772 0c-35.204 0-63.743-28.539-63.743-63.744s28.539-63.744 63.743-63.744c35.206 0 63.745 28.539 63.745 63.744s-28.539 63.744-63.745 63.744z"></path><title>Follow us on Flickr</title></svg></a><a href="http://www.linkedin.com/groups?home=&amp;gid=1913698&amp;trk=anet_ug_hm" rel="nofollow" class="builtin-icons light large linkedin-hover" target="_blank" alt="Follow Us on linkedin" title="Follow Us on linkedin"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-linkedin" data-cacheid="icon-59ce56e7819f6" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M444-6.4h-376c-37.555 0-68 30.445-68 68v376c0 37.555 30.445 68 68 68h376c37.555 0 68-30.445 68-68v-376c0-37.555-30.445-68-68-68zm-284.612 95.448c19.722 0 31.845 13.952 32.215 32.284 0 17.943-12.492 32.311-32.592 32.311h-.389c-19.308 0-31.842-14.368-31.842-32.311 0-18.332 12.897-32.284 32.609-32.284zm32.685 288.552h-64.073v-192h64.073v192zm223.927-.089h-63.77v-97.087c0-27.506-11.119-46.257-34.797-46.257-18.092 0-22.348 12.656-27.075 24.868-1.724 4.382-2.165 10.468-2.165 16.583v101.892h-64.193s.881-173.01 0-192.221h57.693v.31h6.469v19.407c9.562-12.087 25.015-24.527 52.495-24.527 43.069 0 75.344 29.25 75.344 92.077v104.954z"></path><title>Follow us on Linkedin</title></svg></a><a href="https://www.instagram.com/csunaturalsciences/" rel="nofollow" class="builtin-icons light large instagram-hover" target="_blank" alt="Follow Us on instagram" title="Follow Us on instagram"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-instagram" data-cacheid="icon-59ce56e781b8e" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M328 188.8h35.2c6.4 0 12.8-4.8 12.8-12.8v-35.2c0-6.4-4.8-12.8-12.8-12.8h-35.2c-6.4 0-12.8 4.8-12.8 12.8v35.2c1.6 8 6.4 12.8 12.8 12.8zm-72 121.6c33.6 0 60.8-27.2 60.8-60.8s-27.2-60.8-60.8-60.8-60.8 27.2-60.8 60.8 27.2 60.8 60.8 60.8zm99.2-60.8c0 54.4-44.8 99.2-99.2 99.2s-99.2-44.8-99.2-99.2c0-9.6 1.6-17.6 3.2-25.6h-24v132.8c0 6.4 4.8 12.8 12.8 12.8h217.6c6.4 0 12.8-4.8 12.8-12.8v-132.8h-24v25.6zm89.6-256h-377.6c-36.8 0-67.2 30.4-67.2 67.2v376c0 38.4 30.4 68.8 67.2 68.8h376c36.8 0 67.2-30.4 67.2-67.2v-377.6c1.6-36.8-28.8-67.2-65.6-67.2zm-28.8 379.2c0 19.2-17.6 36.8-36.8 36.8h-246.4c-11.2 0-36.8-17.6-36.8-36.8v-246.4c0-20.8 16-36.8 36.8-36.8h246.4c19.2 0 35.2 16 36.8 36.8v246.4z"></path><title>Follow Us via instagram</title></svg></a><a href="https://twitter.com/CSUNaturalSci" rel="nofollow" class="builtin-icons light large twitter-hover" target="_blank" alt="Follow Us on twitter" title="Follow Us on twitter"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-twitter" data-cacheid="icon-59ce56e781d35" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M444-6.4h-376c-37.555 0-68 30.445-68 68v376c0 37.555 30.445 68 68 68h376c37.555 0 68-30.445 68-68v-376c0-37.555-30.445-68-68-68zm-41.733 263.6c-31.342 122.024-241.534 173.781-338.231 47.108 37.04 33.978 101.537 36.954 142.439-3.669-23.987 3.373-41.436-19.233-11.968-31.465-26.501 2.808-41.236-10.76-47.279-22.26 6.213-6.255 13.068-9.157 26.322-9.998-29.017-6.581-39.719-20.227-43.011-36.776 8.059-1.844 18.135-3.436 23.637-2.724-25.411-12.772-34.247-31.98-32.848-46.422 45.402 16.202 74.336 29.216 98.534 41.7 8.619 4.41 18.237 12.38 29.084 22.471 13.825-35.095 30.888-71.268 60.12-89.215-.493 4.07-2.757 7.856-5.76 10.956 8.291-7.239 19.06-12.218 30.004-13.656-1.255 7.896-13.094 12.341-20.233 14.927 5.41-1.621 34.18-13.957 37.317-6.932 3.702 8-19.851 11.669-23.853 13.058-2.983.965-5.986 2.023-8.928 3.17 36.463-3.49 71.26 25.413 81.423 61.295.721 2.581 1.44 5.448 2.099 8.454 13.331 4.782 37.492-.222 45.279-4.825-5.626 12.792-20.253 22.21-41.833 23.93 10.399 4.154 29.994 6.427 43.51 4.222-8.558 8.83-22.363 16.836-45.823 16.648z"></path> <title>Follow Us on twitter</title></svg></a><a href="http://www.youtube.com/playlist?list=PLD46DC47FB9BE147F&amp;feature=plcp" rel="nofollow" class="builtin-icons light large youtube-hover" target="_blank" alt="Follow Us on youtube" title="Follow Us on youtube"><svg class="mk-svg-icon" data-name="mk-jupiter-icon-square-youtube" data-cacheid="icon-59ce56e781f38" style=" height:32px; width: 32px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M227.369 349.573c0 7.385.448 11.076-.017 12.377-1.446 3.965-7.964 8.156-10.513.43-.427-1.353-.049-5.44-.049-12.447l-.07-51.394h-17.734l.053 50.578c.022 7.752-.172 13.537.061 16.164.44 4.644.286 10.049 4.584 13.133 8.026 5.793 23.391-.861 27.24-9.123l-.04 10.547 14.319.019v-81.318h-17.835v51.035zm46.259-47.854l.062-31.592-17.809.035-.089 109.006 14.645-.219 1.335-6.785c18.715 17.166 30.485 5.404 30.458-15.174l-.035-42.49c-.017-16.183-12.129-25.887-28.567-12.781zm15.364 58.35c0 3.524-3.515 6.39-7.805 6.39s-7.797-2.867-7.797-6.39v-47.695c0-3.526 3.507-6.408 7.797-6.408 4.289 0 7.805 2.883 7.805 6.408v47.695zm155.008-366.469h-376c-37.555 0-68 30.445-68 68v376c0 37.555 30.445 68 68 68h376c37.555 0 68-30.445 68-68v-376c0-37.555-30.445-68-68-68zm-156.606 129.297h16.34v65.764c0 3.564 2.935 6.473 6.505 6.473 3.586 0 6.512-2.909 6.512-6.473v-65.764h15.649v84.5h-19.866l.334-6.997c-1.354 2.843-3.024 4.97-5.001 6.399-1.988 1.433-4.255 2.127-6.83 2.127-2.928 0-5.381-.681-7.297-2.026-1.933-1.366-3.366-3.178-4.29-5.419-.915-2.259-1.476-4.601-1.705-7.036-.219-2.457-.351-7.296-.351-14.556v-56.991zm-48.83.883c3.511-2.769 8.003-4.158 13.471-4.158 4.592 0 8.539.901 11.826 2.673 3.305 1.771 5.854 4.083 7.631 6.931 1.801 2.856 3.022 5.793 3.673 8.799.66 3.046.994 7.643.994 13.836v21.369c0 7.84-.317 13.606-.923 17.267-.599 3.67-1.908 7.072-3.912 10.272-1.988 3.155-4.544 5.52-7.647 7.029-3.137 1.515-6.733 2.258-10.786 2.258-4.531 0-8.341-.619-11.488-1.934-3.156-1.291-5.59-3.26-7.331-5.857-1.754-2.594-2.985-5.772-3.727-9.468-.756-3.701-1.113-9.261-1.113-16.666v-22.371c0-8.113.685-14.447 2.026-19.012 1.345-4.55 3.78-8.21 7.305-10.966zm-52.06-34.18l11.946 41.353 11.77-41.239h20.512l-22.16 55.523-.023 64.81h-18.736l-.031-64.788-23.566-55.659h20.287zm197.528 280.428c0 21.764-18.882 39.572-41.947 39.572h-172.476c-23.078 0-41.951-17.808-41.951-39.572v-90.733c0-21.755 18.873-39.573 41.951-39.573h172.476c23.065 0 41.947 17.819 41.947 39.573v90.733zm-131.334-174.005c4.343 0 7.876-3.912 7.876-8.698v-44.983c0-4.778-3.532-8.684-7.876-8.684-4.338 0-7.903 3.906-7.903 8.684v44.984c0 4.786 3.565 8.698 7.903 8.698zm-50.218 88.284v-14.152l-56.999-.098v13.924l17.791.053v95.84h17.835l-.013-95.567h21.386zm142.172 67.119l-.034 1.803v7.453c0 4-3.297 7.244-7.298 7.244h-2.619c-4.015 0-7.313-3.244-7.313-7.244v-19.61h30.617v-11.515c0-8.42-.229-16.832-.924-21.651-2.188-15.224-23.549-17.64-34.353-9.853-3.384 2.435-5.978 5.695-7.478 10.074-1.522 4.377-2.269 10.363-2.269 17.967v25.317c0 42.113 51.14 36.162 45.041-.053l-13.37.068zm-16.947-34.244c0-4.361 3.586-7.922 7.964-7.922h1.063c4.394 0 7.981 3.56 7.981 7.922l-.192 9.81h-16.887l.072-9.81z"></path><title>Follow Us via youtube</title></svg></a></div></section></div>
			<div class="mk-col-1-4"><section id="text-6" class="widget widget_text mk-in-viewport"><div class="widgettitle">Support the College</div>			<div class="textwidget"><div">
<a href="https://advancing.colostate.edu/GIVE" target="_self" onclick="" class="noHoverUnderline" style="background-color: rgb(151, 202, 61);
    border-color: rgb(239, 237, 237);
    border-width: 0px;
    border-style: none none none none;
    border-radius: 0px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 21px;
    padding-right: 21px;
    display: inline-block;
    color: #ffffff;
    font-size: 22px;
    font-weight: 600;
    font-family: Vitesse ssm a, vitesse ssm b;
    letter-spacing: 2px;">GIVE NOW</a>
</div>

<div style="margin-top: 40px; text-align: center;">
<a href="http://giving.colostate.edu/"><img src="<?php echo get_template_directory_uri(); ?>/images/state.png" style="width: 231px;" alt="State Your Purpose"></a></div></div>
		</section></div>
	            <div class="clearboth"></div>
        </div>
    </div>

<div id="sub-footer">
	<div class=" mk-grid">
		
    	<span class="mk-footer-copyright">
            <div class="flexbox-footer">
                <div class="flexbox-footer-left">
                    <div class="flexbox-footer-left-top">
                        <ul style="padding-left: 0;">
                            <li><a href="http://admissions.colostate.edu/">Apply to CSU</a></li>
                            <li><a href="http://www.colostate.edu/info-contact.aspx">Contact CSU</a></li>
                            <li><a href="http://www.colostate.edu/info-disclaimer.aspx">Disclaimer</a></li>
                            <li><a href="http://www.colostate.edu/info-equalop.aspx">Equal Opportunity</a></li>
                            <li><a href="http://www.colostate.edu/info-privacy.aspx">Privacy Statement</a></li>
                        </ul>
                    </div>
                    <div class="flexbox-footer-left-bottom">
                        &copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> Colorado State University - College of Natural Sciences, Fort Collins, CO 80523
                    </div>
                </div>
                <div class="flexbox-footer-right">
                    <a href="http://www.colostate.edu/"><img src="<?php echo get_template_directory_uri(); ?>/images/csu-logo-oneline.svg" alt="Colorado State University" style="width: 350px;"></a>
                </div>
            </div>
        </span>

    </div>
	<div class="clearboth"></div>
</div>

</section>

</div>

<?php wp_footer(); ?>

</body>
</html>