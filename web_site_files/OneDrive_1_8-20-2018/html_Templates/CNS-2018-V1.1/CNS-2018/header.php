<!DOCTYPE html>
<html <?php language_attributes();?>>

<?php wp_head(); ?>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="description" content="<?php if ( is_single() ) {
        echo get_the_excerpt();
    } else {
        bloginfo('name'); echo " - "; bloginfo('description');
    }
    ?>" />
	
	<title>
	<?php
		if ( is_single() ) {
			single_post_title(); echo ' | '; bloginfo( 'name' );
		} elseif ( is_home() || is_front_page() ) {
			bloginfo( 'name' ); 
			if( get_bloginfo( 'description' ) ) 
				echo ' | ' ; bloginfo( 'description' ); 
		} elseif ( is_page() ) {
			single_post_title( '' ); echo ' | '; bloginfo( 'name' );
		} elseif ( is_404() ) {
			_e( 'Not Found', 'Fifth Street Creative' ); echo ' | '; bloginfo( 'name' );
		} else {
			wp_title( '' ); echo ' | '; bloginfo( 'name' );		}
	?>
	</title>
	
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="format-detection" content="telephone=no">

<title>CNSIT | College of Natural Sciences</title>

<meta name="robots" content="noindex,follow">

<link rel="dns-prefetch" href="https://s.w.org/">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

<style id="theme-styles-inline-css" type="text/css">
body { background-color:#fff; } .mk-header { background-color:#fff;background-size:cover;-webkit-background-size:cover;-moz-background-size:cover; } .mk-header-bg { background-color:#1e4d2b;background-position:center center; } .mk-classic-nav-bg { background-color:#ffffff; } .master-holder-bg { background-color:#fff; } #mk-footer { background-color:#255f35;background-repeat:no-repeat; } #mk-boxed-layout { -webkit-box-shadow:0 0 0px rgba(0, 0, 0, 0); -moz-box-shadow:0 0 0px rgba(0, 0, 0, 0); box-shadow:0 0 0px rgba(0, 0, 0, 0); } .mk-news-tab .mk-tabs-tabs .is-active a, .mk-fancy-title.pattern-style span, .mk-fancy-title.pattern-style.color-gradient span:after, .page-bg-color { background-color:#fff; } .page-title { font-size:20px; color:#4d4d4d; text-transform:uppercase; font-weight:400; letter-spacing:2px; } .page-subtitle { font-size:14px; line-height:100%; color:#a3a3a3; font-size:14px; text-transform:none; } .mk-header { border-bottom:1px solid #ededed; } .header-style-1 .mk-header-padding-wrapper, .header-style-2 .mk-header-padding-wrapper, .header-style-3 .mk-header-padding-wrapper { padding-top:141px; }
#cnsHeaderLink:hover{
    text-decoration: none;
}
</style>

</head>

<body class="page-template-default page " itemscope="itemscope" itemtype="https://schema.org/WebPage">
	<div id="top-of-page"></div>
	<div id="mk-boxed-layout">
		<div id="mk-theme-container">		 
            <header data-height="90" data-sticky-height="55" data-responsive-height="90" data-transparent-skin="" data-header-style="2" data-sticky-style="fixed" data-sticky-offset="header" id="mk-header-1" class="mk-header header-style-2 header-align-left  toolbar-false menu-hover-5 sticky-style-fixed mk-background-stretch boxed-header " role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
                <div class="mk-header-holder">
                    <div class="mk-header-inner">
                        <div class="mk-header-bg "></div>
                            <div class="mk-grid header-grid">
                                <div class="add-header-height">
                                    <!-- CSU (SG) modification for logo holder -->
                                    <div id="responsiveHeaderContainer">
                                        <a href="http://colostate.edu/" id="csuHeaderLink">
                                            <img id="csuLargeLogo" src="<?php echo get_template_directory_uri(); ?>/images/signature-oneline.svg" alt="Colorado State University" width="350" height="45">
                                            <img id="csuMedLogo" src="<?php echo get_template_directory_uri(); ?>/images/signature-stacked.svg" alt="Colorado State University" width="172" height="45"> <!-- 1140px -->
                                            <img id="csuSmallLogo" src="<?php echo get_template_directory_uri(); ?>/images/signature-mobile.svg" alt="Colorado State University" width="113" height="45"> <!-- 800px -->
                                        </a>
                                        <div id="responsiveLogoSubsystem">
                                            <a href="http://www.natsci.colostate.edu/" id="cnsHeaderLink" title="">
                                                <h1 id="cnsHeaderText" class="larger-CSUtext">
                                                    <span style="display:inline-block;">COLLEGE OF</span> <span style="display:inline-block;">NATURAL SCIENCES</span>
                                                </h1>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="deptDIV" style="background-color:#286739; position: relative; z-index: 300; height: 50px;">
                                <div style="max-width: 1140px; margin: 0 auto; vertical-align: middle;">
                                       <h2><a href="<?php echo get_site_url(); ?>" style="color: rgba(255, 255, 255, 0.9); margin-left: 15px; vertical-align: middle; text-decoration: none;"><?php echo get_bloginfo('name');?></a></h2>
								</div>
                            </div>
                            <div class="clearboth"></div>
						</div>
					</div>
				
				<div class="mk-header-nav-container menu-hover-style-5" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" >
					<div class="mk-classic-nav-bg"></div>
					<div style="background-color:#ffffff; position: relative; z-index: 300; height: 40px; padding-top: 5px;" class="mk-classic-menu-wrapper">
						<nav style="max-width: 1140px; margin: 0 auto; vertical-align: middle;" class="mk-main-navigation js-main-nav">
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'main-navigation-ul') ); ?>
						</nav>
						<div class="main-nav-side-search">
							<a class="mk-search-trigger  mk-toggle-trigger" href="#"><i class="mk-svg-icon-wrapper"><svg  class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-5ad0c39c5275b" style=" height:16px; width: 14.857142857143px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1664 1792"><path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg></i><span style="display: none;">Open search</span></a>
							<div id="mk-nav-search-wrapper" class="mk-box-to-trigger">
								<form method="get" id="mk-header-navside-searchform" action="<?php echo get_site_url(); ?>">
									<input type="text" name="s" id="mk-ajax-search-input" autocomplete="off" /><label for="mk-ajax-search-input" style="display: none;">Search term</label>
									<input type="hidden" id="security" name="security" value="b9d551dbca" /><input type="hidden" name="_wp_http_referer" value="/" />			
									<i id="main-search-submit" class="nav-side-search-icon"><input type="submit" value="search" style="visibility: hidden;"/><svg class="mk-svg-icon" data-name="mk-moon-search-3" data-cacheid="icon-5ad0c39c52d45" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M496.131 435.698l-121.276-103.147c-12.537-11.283-25.945-16.463-36.776-15.963 28.628-33.534 45.921-77.039 45.921-124.588 0-106.039-85.961-192-192-192-106.038 0-192 85.961-192 192s85.961 192 192 192c47.549 0 91.054-17.293 124.588-45.922-.5 10.831 4.68 24.239 15.963 36.776l103.147 121.276c17.661 19.623 46.511 21.277 64.11 3.678s15.946-46.449-3.677-64.11zm-304.131-115.698c-70.692 0-128-57.308-128-128s57.308-128 128-128 128 57.308 128 128-57.307 128-128 128z"/></svg></i>
								</form>
								<ul id="mk-nav-search-result" class="ui-autocomplete"></ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="mk-header-right"></div>

				<div id="hamburger-menu" class="mk-nav-responsive-link">
					<div class="mk-css-icon-menu">
						<div class="mk-css-icon-menu-line-1"></div>
						<div class="mk-css-icon-menu-line-2"></div>
						<div class="mk-css-icon-menu-line-3"></div>
					</div>
				</div> 
				
				<div id="mobile-menu" class="mk-responsive-wrap">
					<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => false, 'menu_class' => 'mk-responsive-nav') ); ?>
					<form id="responsive-searchform" class="responsive-searchform" method="get" action="<?php echo get_site_url(); ?>">
						<input type="text" title="Search Terms" class="text-input" value="" name="s" id="s" placeholder="Search.." />
						<i id="responsive-search-submit"><input value="Submit search" type="submit" style="visibility:hidden;" /><svg class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-5ad0c39c65a87" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1664 1792"><path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg></i>
					</form>
				</div>
				</header>	
                        