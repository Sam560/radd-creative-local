<?php get_header(); ?>

<div id="mainBody">

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="news-div">
				<div class="wpb_row news-list-row">
					<div class="wpb_column">
						
						<?php the_title('<h2>', '</h2>'); ?>
						<?php the_modified_date('F jS, Y', "<p><strong>Updated</strong>: ", "</p>"); ?>

						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>

</div>

<?php get_footer(); ?>