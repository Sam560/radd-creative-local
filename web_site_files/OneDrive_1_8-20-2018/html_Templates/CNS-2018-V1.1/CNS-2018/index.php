<?php get_header(); ?>

<div style="margin-top: 10px;" id="mainBody">

<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

<?php query_posts('cat=-6280&paged=' . $paged); ?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="news-div">
				<div class="wpb_row vc_row news-list-row">
					<div class="wpb_column">
						<div class="wpb_row news-list-row">
							<div class="wpb_column vc_col-sm-2 news-featured-image">
								<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail(array(128,128));
								} else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/images/csu-ram-greengold-1024x1024.png"
								alt="<?php the_title(); ?>" />
								<?php } ?>
							</div>
						
							<div class="wpb_column vc_col-sm-10">
								<div class="news-list-title-div">
									<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								</div>

								<div class="news-list-details-div">
									By <?php the_author() ?><br>
									<?php the_time('l, F jS, Y, g:i a') ?> MT<br>
									Posted in <?php the_category(', '); ?>
								</div>
								<div class="news-list-content-div">
									<p>
										<?php the_excerpt(); ?>
									</p>
								</div>
							</div>
						</div>
							
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>

	<div class="navigation">
		<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
		<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
	</div>

<?php else : ?>
	<h2 class="center">Not Found</h2>
	<p class="center">Sorry, but you are looking for something that isn't here.</p>
	<?php //get_search_form(); ?>
<?php endif; ?>

</div>

<?php get_footer(); ?>