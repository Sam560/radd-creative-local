<?php

//Menu
add_theme_support('menus');
add_theme_support('post-thumbnails'); 

// REMOVE WP header info & Login errors
remove_action('wp_head', 'wp_generator');
add_filter('login_errors',create_function('$a', "return null;"));

//menus
function register_header_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_header_menu' );

function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="menu-item-link js-smooth-scroll"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'no-mega-menu';
  return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);

//Stylesheets
function add_cns2018_styles_with_the_lot()
{
	wp_register_style('light-style', get_template_directory_uri() . "/css/light_style.css", array(), '20180413', 'all');
	wp_register_style('full-style', get_template_directory_uri() . "/css/full-styles.css", array(), '20180413', 'all');
	wp_register_style('theme-options-prod-style', get_template_directory_uri() . "/css/theme-options-production.css", array(), '20180413', 'all');
	wp_register_style('normal-style', get_template_directory_uri() . "/css/style.css", array(), '20180413', 'all');
	wp_register_style('proxima-style', get_template_directory_uri() . "/css/proxima.css", array(), '20180413', 'all');	
	wp_register_style('fa-svg-with-js-style', get_template_directory_uri() . "/css/fa-svg-with-js.css", array(), '20180413', 'all');	
	wp_register_style('composer-style', get_template_directory_uri() . "/css/js_composer.min.css", array(), '20180413', 'all');	
	wp_register_style('bootstrap-style', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css", array(), '20180413', 'all');
	wp_register_style('main-style', get_template_directory_uri() . "/css/main-style.css", array(), '20180413', 'all');	
	
	wp_enqueue_style( 'light-style');
	wp_enqueue_style( 'full-style');
	wp_enqueue_style( 'theme-options-prod-style');
	wp_enqueue_style( 'normal-style');
	wp_enqueue_style( 'proxima-style');
	wp_enqueue_style( 'fa-svg-with-js-style');
	wp_enqueue_style( 'composer-style');
	wp_enqueue_style( 'bootstrap-style');
	wp_enqueue_style( 'main-style');
}
add_action('wp_enqueue_scripts', 'add_cns2018_styles_with_the_lot');

//Javascript
function add_cns2018_scripts_with_the_lot()
{
	wp_register_script('bootstrap-script', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js", array('jquery'), '20180413', true);
	wp_register_script('fontawesome-script', get_template_directory_uri() . "/js/fontawesome-all.min.js", array('jquery'), '20180413', true);
	wp_register_script('hamburger-script', get_template_directory_uri() . "/js/hamburger.js", array('jquery'), '20180413', true);
	
	wp_enqueue_script('bootstrap-script');
	wp_enqueue_script('fontawesome-script');
	wp_enqueue_script('hamburger-script');
}
add_action('wp_enqueue_scripts', 'add_cns2018_scripts_with_the_lot');

?>