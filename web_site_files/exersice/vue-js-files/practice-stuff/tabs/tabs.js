
Vue.component('tabs',{

    template:'<div>\n<div class="tabs">\n' +
    '  <ul>\n' +
    '    <li v-for="tab in tabs" :class="{ \'is-active\' : tab.isActive }">\n        <a href="#" @click="selectedTab(tab)">{{ tab.name }}</a>\n        <!--need to pass selected through next argument tab-->\n    </li>\n      ' +
    '\n' +
    '' +
    '' +
    '  </ul>\n' +
    '</div>\n    <div class="tabs-details">\n        <slot></slot>\n    </div>\n</div>',

//    need to be explicit about what data is needed for compenent
    data() {
      return { tabs: [] };
    },

created() {
        this.tabs = this.$children;
},

    methods: {
        selectedTab(selectTab) {
          this.tabs.forEach(tab => {
             tab.isActive = (tab.name == selectedTab.name);

          });
        },
    }
});

Vue.component('tab',{
    template:'<div><slot></slot></div>',

    props: {
        name: {required: true},
        selected: {default: false},
    },

    data() {
        isActive: false
    },

    mounted () {
        this.isActive = this.selected;
    }
});


new Vue ({
    el:'#root'
});