

//global vue componenet for task-list
Vue.component('message', {

    // need to implement properties in order to populate message tag
    props: ['title', 'body'],

    // need to fix syntax layout and make sure not to delete any of the commas so that things work correclty.
    template:' <article class="message is-primary">\n' +
        '        <div class="message-header">\n' +
        '            {{ title }}' +
        '            <button class="delete" aria-label="delete"></button>\n' +
        '        </div>\n' +
        '        <div class="message-body">\n' +
        '               {{ body }}   ' +
        '        </div>\n' +
        '    </article>\n'


});

new Vue({
    //make sure no space between el bind
    el:'#root'
});