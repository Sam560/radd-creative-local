

//global vue componenet for task-list
Vue.component('task-list', {
    template: '<li><slot></slot></li>'


//need to remember that you cant bind data directly to a compenent, you must create a function to contain that action.

});

new Vue({
    //make sure no space between el bind
    el:'#root'
});