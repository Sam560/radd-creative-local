

//global vue componenet for task-list
Vue.component('task-list', {
    template:
        '<div>' +

            '<task v-for="task in tasks">{{ task.task }}</task>' +
        '' +
        '</div>',

    data() {
        return{
            tasks: [
                {task: 'Go to the Store', completed: true},
                {task: 'Finish screencast', completed: false},
                {task: 'Make donation ', completed: true},
                {task: 'Clear inbox', completed: false},
                {task: 'Clean Room', completed: false},
                {task: 'CLean Room', completed: false},

            ]
        };

    }

//need to remember that you cant bind data directly to a compenent, you must create a function to contain that action.

});


//global vue componenet for task-list
Vue.component('task', {
    template: '<li><slot></slot></li>',


//need to remember that you cant bind data directly to a compenent, you must create a function to contain that action.

});

new Vue({
    //make sure no space between el bind
    el:'#root',

    computed: {
        incompleteTask()
        {
            return this.tasks.filter(task => !task.completed)
        }
    }


});