
Vue.component('modal', {

    template: ' <div class="modal is-active">\n         <div class="modal-background"></div>\n         <div class="modal-content">\n             <div class="box">\n            <p>\n                Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                Suspendisse varius enim in eros elementum tristique. \n                Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. \n                Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet.\n                Nunc ut sem vitae risus tristique posuere.\n                \n            </p>\n             </div>\n         </div>\n         <button class="modal-close" @click="$emit(\'close\')" ></button>\n    \n ' +
        '</div>'
});

new Vue({
   el:'#root',

    data: {
       showModal: false
    }

});