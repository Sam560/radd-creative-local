<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Calculate Paycheck</title>
</head>
<body>
    <?php
        // catch to display required feilds 
        function displayRequired($fieldName) {
            echo "The feild \"$fieldName\"is required.<br />\n";
        }
        // validates users inputs 
        function validateInput ($data, $fieldName) {
            global $errorCount; 
                if ( empty($data) ) {
                    displayRequired($fieldName);
                    ++$errorCount;
                    $retval = "";
                } else {        // Only clean up the input if it isn't
                                // empty
                    $retval = trim($data);
                    $retval = stripslashes($retval);
            }
            return($retval);
        }

        // declaring error count var
        $errorCount = 0; 

        // validates the input types 
        $firstName = validateInput($_POST['fName'], "First Name");
        $lastName = validateInput($_POST['lName'], "Last Name");
            if($errorCount>0) 
                echo "Please use the \"Back\"button to re-enter the data.<br />\n";
            else
                echo "Thank you for filling out the scholarship form," . $firstName . " " . $lastName . ".";

        // redisplay the form information 
        function redisplayform($firstName,$lastName) {
            ?>
            <h2 style = "text-align:center">Scholarship Form</h2>
            <form name = "scholarship" action = "process_Scholarship.php" method = "post">
                <p>First Name: <input type = "text" name = "fName" value = "<?php echo $firstName;?>"/></p>
                <p>Last Name: <input type = "text" name = "lName" value = "<?php echo $lastName;?>"/></p>
                <p><input type = "reset" value = "Clear Form" />&nbsp; 
                &nbsp;<input type = "submit" name = "Submit" value = "Send Form" /></p>
             </form>
             <?php
        }

        // redisplay the form to fix info
        if($errorCount>0) {
            echo " Please re-enter the information below.<br />\n";
            redisplayform($firstName, $lastName);
        }
        else {
            // send an email 
            // replace the "recipient@example.edu with your 
            // email address
            $To = "sschoenberg234@gmail.com";
            $Subject = "Scholarship Form Results"; 
            $Message = "Student Name:" . $firstName. " " . $lastName;
            $result = mail($To, $Subject, $Message);
                if($result)
                    $resultMsg = "Your message was successfully sent.";
                else
                    $resultMst = "There was a problem sending your message.";
        }

        
        
    ?>
    
</body>
</html>