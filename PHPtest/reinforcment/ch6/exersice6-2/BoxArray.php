<!DOCTYPE HTML PUBLIC"-/W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3c.org/1999/xhtml">

<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    <title>Box Array</title>
</head>
<body>
    <h1> The Box Array</h1>
    <?php
        // define the multiple arrays inside a single variable $boxArray
        $boxArray = array   (
            'Small Box' => array(12, 10, 2.5),
            'Medium Box' => array(30, 20, 4),
            'Large Box' => array(60, 40, 11.5)
            );
        // handels multiplying the values of the arrays together 
            foreach ($boxArray as $k => $v){
            echo $k.': '.$v[0].' x '.$v[1].' x '.$v[2].' = ' .$v[0]*$v[1]*$v[2].'<br>'; 
            }
                echo '<table border="10">';
                echo '<tr><th>Length</th><th>Width</th><th>Depth</th></tr>';
        
        
        foreach( $boxArray as $boxArray ){
            echo '<tr>';
        foreach( $boxArray as $key ){
            echo '<td>'.$key.'</td>';
        }
        echo '</tr>';
        }; 
    ?>
</body>
</html>