

// // Dependencies: jQuery, jQueryUI, birthday.js, styles.css
(function() {
    document.forms.register.noValidate = true; 

$('feedback').on('submit', function(e) {
    var elemnts = this.elements; 
    var valid  = {};
    var isValid; 
    var isFormValid; 

// PERFORM GENERIC CHECKS (calls functions outside the event handler)
    var i
    for (i=0,l=elements.length;i<l; i++) {
        isValid = validateRequired(elements[i]) && validateTypes(elements[i]);
        if(!valid) {
            showErrorMessage(elements[i]);
        }else{
            removeErrorMessage(elements[i]);
        }
        valid[elements[i].id] = isValid; 
    }
    //PERFORM CUSTOM VALIDATION
     // message (you could cache message input in variable here)
    if(!validateMessage()){
        showErrorMessage(document.getElementById('message'))
        vaild.message = false
    }else{
        removeErrorMessage(document.getElementById('message'));
    }
     // Call validatePassword(), and if not valid
    if (!validateName()) {         
         showErrorMessage(document.getElementById('fname')); 
         valid.password = false;          
     } else {                            
        removeErrorMessage(document.getElementById('fname'));
    }

    if (!validateName()) {         
        showErrorMessage(document.getElementById('lname')); 
        valid.password = false;          
    } else {                            
       removeErrorMessage(document.getElementById('lname'));
   }
    // emaillist-consent (you could cache emaillist-consent in variable here)
    if (!validateemailConsent()) {     
         showErrorMessage(document.getElementById('email-consent')); 
       valid.parentsConsent = false;      
     } else {                             
        removeErrorMessage(document.getElementById('email-consent'));
    }

    // Loop through valid object, if there are errors set isFormValid to false
     for (var field in valid) {          
        if (!valid[field]) {              
           isFormValid = false;            
            break;                          
         }                                 
         isFormValid = true;               
     }
     if(!isFormValid) {
         e.preventDevault();
     }
});
// CHECK IF THE FIELD IS REQUIRED AND IF SO DOES IT HAVE A VALUE
function validateRequired(el) {
    if (isRequired(el)) {                           
        var valid = !isEmpty(el);                     
        if (!valid) {                                 
        setErrorMessage(el,  'Field is required');  
        }
        return valid;                                 
    }
    return true;                                   
}

// CHECK IF THE ELEMENT IS REQUIRED
function isRequired(el) {
    return((typeof el.required === 'boolean') && el.required) || 
    (typeof el.required === 'string');
}
// CHECK IF THE ELEMENT IS EMPTY (or its value is the same as the placeholder text)
function isEmpty(el) {
         return !el.value || el.value === el.placeholder;   
}
    
function validateTypes(el) {
    if (!el.value) return true;                     
    // Otherwise get the value from .data()
 var type = $(el).data('type') || el.getAttribute('type');  
         if (typeof validateType[type] === 'function') { 
          return validateType[type](el);                
         } else {                                        
          return true;                                  
        }
       }

function validateTypes(el) {
        if (!el.value) return true; 
             var type = $(el).data('type') || el.getAttribute('type');  // OR get the type of input
        if (typeof validateType[type] === 'function') { // Is the type a method of validate object?
             return validateType[type](el);                // If yes, check if the value validates
        } else {                                        // If not
            return true;                                  // Return true because it cannot be tested
    }
}

// CHECK IF THE VALUE FITS WITH THE TYPE ATTRIBUTE
function validateParentsConsent() {
    var checkbox   = document.getElementById('email-container');
    var checkbox = document.getElementById('consent-container');
    var valid = true;                         
       if (checkbox.className.indexOf('hide') === -1) { 
         valid = checkbox.checked;         
         if (!valid) {                          
           setErrorMessage(checkbox, 'Are your sure you want to void the email listing');
         }
       }
       return valid;                             
     }

  // Check if the message is less than or equal to 100 characters
function validateMessage() {
    var messageCounter = document.getElementById('message-count');
    var valid = messageCounter.value.length <= 100;
        if (!valid) {
         setErrorMessage(messageCounter, 'Please make sure your Message does not exceed 100 characters');
        }
     return valid;
    }

   // -------------------------------------------------------------------------
   // -------------------------------------------------------------------------

function setErrorMessage(el, message) {
     $(el).data('errorMessage', message);                 // Store error message with element
   }

function getErrorMessage(el) {
     return $(el).data('errorMessage') || el.title;       // Get error message or title of element
   }

function showErrorMessage(el) {
     var $el = $(el);                                      //The element with the error
     var errorContainer = $el.siblings('.error.message'); // Any siblings holding an error message

     if (!errorContainer.length) {                         // If no errors exist with the element
        // Create a <span> element to hold the error and add it after the element with the error
        errorContainer = $('<span class="error message"></span>').insertAfter($el);
     }
     errorContainer.text(getErrorMessage(el));              //Add error message
   }

   function removeErrorMessage(el) {
     var errorContainer = $(el).siblings('.error.message');  //Get the sibling of this form control used to hold the error message
     errorContainer.remove();                               // Remove the element that contains the error message
   }



//   // -------------------------------------------------------------------------
//   // -------------------------------------------------------------------------

    //Checks whether data is valid, if not set error message
   // Returns true if valid, false if invalid
   var validateType = {
     email: function (el) {                                  //Create email() method
        //Rudimentary regular expression that checks for a single @ in the email
       var valid = /[^@]+@[^@]+/.test(el.value);             //Store result of test in valid
       if (!valid) {                                         //If the value of valid is not true
         setErrorMessage(el, 'Please enter a valid email');  //Set error message
       }
       return valid;                                        // Return the valid variable
     },
     number: function (el) {                                 //Create number() method
       var valid = /^\d+$/.test(el.value);                  // Store result of test in valid
       if (!valid) {
         setErrorMessage(el, 'Please enter a valid number');
       }
       return valid;
     },
     date: function (el) {                                   //Create date() method
                                                             //Store result of test in valid
       var valid = /^(\d{2}\/\d{2}\/\d{4})|(\d{4}-\d{2}-\d{2})$/.test(el.value);
       if (!valid) {                                         //If the value of valid is not true
         setErrorMessage(el, 'Please enter a valid date');   //Set error message
       }
       return valid;                                         //Return the valid variable
     }
   };

 }());  // end of error message removal 
