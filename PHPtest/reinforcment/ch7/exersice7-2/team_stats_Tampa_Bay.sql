-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 20, 2018 at 01:23 AM
-- Server version: 5.6.34-log
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baseball_stats`
--

-- --------------------------------------------------------

--
-- Table structure for table `teamstats`
--

CREATE TABLE `teamstats` (
  `Team` varchar(50) NOT NULL,
  `FirstYear` int(11) NOT NULL,
  `G` int(11) NOT NULL,
  `W` int(11) NOT NULL,
  `L` int(11) NOT NULL,
  `Pennants` int(11) NOT NULL,
  `WS` int(11) NOT NULL,
  `R` int(11) NOT NULL,
  `AB` int(11) NOT NULL,
  `H` int(11) NOT NULL,
  `HR` int(11) NOT NULL,
  `AVG` float NOT NULL,
  `RA` int(11) NOT NULL,
  `ERA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teamstats`
--

INSERT INTO `teamstats` (`team`, `HR`) VALUES
('Tampa Bay Rays', 1713),
('Arizona Diamondbacks', 1933),
('Florida Marlins', 2392),
('Colorado Rockies', 2944),
('Kansas City Royals', 4777),
('San Diego Padres', 4799),
('Washington Nationals', 4946),
('Seattle Mariners', 4967),
('Toronto Blue Jays', 5018),
('Houston Astros', 5533),
('Milwaukee Brewers', 5794),
('New York Mets', 5941),
('Los Angeles Angels of Anaheim', 6374),
('Texas Rangers', 7249),
('Minnesota Twins', 9232),
('Chicago White Sox', 9662),
('Pittsburgh Pirates', 9970),
('St. Louis Cardinals', 10504),
('Los Angeles Dodgers', 11154),
('Baltimore Orioles', 11335),
('Cleveland Indians', 11338),
('Oakland Athletics', 11408),
('Philadelphia Phillies', 11457),
('Cincinnati Reds', 11533),
('Boston Red Sox', 11671),
('Detroit Tigers', 12050),
('Atlanta Braves', 12203),
('Chicago Cubs', 12479),
('San Francisco Giants', 13262),
('New York Yankees', 13914);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
