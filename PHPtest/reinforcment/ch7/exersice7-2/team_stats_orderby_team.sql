-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 20, 2018 at 01:21 AM
-- Server version: 5.6.34-log
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baseball_stats`
--

-- --------------------------------------------------------

--
-- Table structure for table `teamstats`
--

CREATE TABLE `teamstats` (
  `Team` varchar(50) NOT NULL,
  `FirstYear` int(11) NOT NULL,
  `G` int(11) NOT NULL,
  `W` int(11) NOT NULL,
  `L` int(11) NOT NULL,
  `Pennants` int(11) NOT NULL,
  `WS` int(11) NOT NULL,
  `R` int(11) NOT NULL,
  `AB` int(11) NOT NULL,
  `H` int(11) NOT NULL,
  `HR` int(11) NOT NULL,
  `AVG` float NOT NULL,
  `RA` int(11) NOT NULL,
  `ERA` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teamstats`
--

INSERT INTO `teamstats` (`team`, `G`, `AB`) VALUES
('Arizona Diamondbacks', 1819, 62131),
('Atlanta Braves', 19764, 677310),
('Baltimore Orioles', 16861, 572146),
('Boston Red Sox', 16848, 575510),
('Chicago Cubs', 19796, 678492),
('Chicago White Sox', 16855, 570404),
('Cincinnati Reds', 19382, 661241),
('Cleveland Indians', 16863, 575356),
('Colorado Rockies', 2565, 88540),
('Detroit Tigers', 16885, 575699),
('Florida Marlins', 2561, 87282),
('Houston Astros', 7526, 255339),
('Kansas City Royals', 6380, 218508),
('Los Angeles Angels of Anaheim', 7684, 260842),
('Los Angeles Dodgers', 19185, 654177),
('Milwaukee Brewers', 6387, 217387),
('Minnesota Twins', 16869, 575390),
('New York Mets', 7518, 254460),
('New York Yankees', 16836, 574293),
('Oakland Athletics', 16818, 570335),
('Philadelphia Phillies', 19190, 657092),
('Pittsburgh Pirates', 19343, 663975),
('San Diego Padres', 6393, 216951),
('San Francisco Giants', 19263, 657818),
('Seattle Mariners', 5098, 174802),
('St. Louis Cardinals', 19387, 665624),
('Tampa Bay Rays', 1817, 62386),
('Texas Rangers', 7671, 262238),
('Toronto Blue Jays', 5101, 175373),
('Washington Nationals', 6385, 216094);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
